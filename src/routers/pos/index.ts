import express, {Request, Response} from 'express';

const router = express.Router();

router.use('/', (req:Request, res: Response)=> res.json({status: 'pos init OK'}))

export default router;