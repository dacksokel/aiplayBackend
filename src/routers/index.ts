import express, { Request, Response, NextFunction } from 'express';
import api from './api/index';
import webhook from './webhook/index';
import pos from './pos/index';

const router = express.Router();

// Use JSON parser for all non-webhook routes
router.use(
    (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): void => {
        console.log(req.originalUrl);
        if (req.originalUrl.includes('/webhook')) {
            next();
        } else {
            express.json()(req, res, next);
        }
    }
    );
    
    router.use('/api', api);
    
    // Endpoint for POS
    router.use('/pos', pos);
    
    // Endpoint for Webhooks
    router.use('/webhook', webhook);
// Invalid API or endpoint error handling middleware
router.use((req: Request, res: Response, next: NextFunction) => {
    res.status(404).json({ error: 'Invalid API or endpoint' });
});



export default router;
