import express from 'express';
import IaPremierLeagueController from '../../controllers/iaPremierLeagueController';

const router = express.Router({});

router.get('/', IaPremierLeagueController.fetchData.bind(IaPremierLeagueController));
router.get('/prediccion1', IaPremierLeagueController.prediccion.bind(IaPremierLeagueController))
router.get('/prediccion2', IaPremierLeagueController.prediccion2.bind(IaPremierLeagueController))
router.get('/prediccion3', IaPremierLeagueController.prediction3.bind(IaPremierLeagueController))
router.post('/standings', IaPremierLeagueController.saveStandigs.bind(IaPremierLeagueController));
router.post('/leader', IaPremierLeagueController.saveSeasonLeaders.bind(IaPremierLeagueController))
router.post('/overunder', IaPremierLeagueController.saveOverUnder.bind(IaPremierLeagueController))
router.post('/probabilitie', IaPremierLeagueController.saveProbabilities.bind(IaPremierLeagueController))



// router.get('/bestPlayer', IaPremierLeagueController.fetchLeader.bind(IaPremierLeagueController));

export default router;
