import express, { Request, Response } from 'express';
import SportRadarController from '../../../../controllers/SportRadarController';

const router = express.Router({});

router.get('/competitions', SportRadarController.getCompetitions.bind(SportRadarController));
router.get('/competitions/:id', SportRadarController.getCompetitionById.bind(SportRadarController));

router.get('/seasons', SportRadarController.getSeasons.bind(SportRadarController))
router.get('/seasons/:id', SportRadarController.getSeasonById.bind(SportRadarController))

// // router.get('/seasons/leaders/', SportRadarController.getLeaders.bind(SportRadarController))
// router.get('/seasons/leaders/:id', SportRadarController.getLeadersById.bind(SportRadarController))

// router.get('/seasons/standings/:id', SportRadarController.getStandingsBySeasonId.bind(SportRadarController))

router.get('/seasons/probabilities/:id', SportRadarController.getProbabilitiesBySeasonId.bind(SportRadarController))


export default router;