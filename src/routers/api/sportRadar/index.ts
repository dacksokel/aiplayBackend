import express, { Request, Response } from 'express';
import SoccerRouter from './soccer';
import TennisRouter from './tennis'
import BasketBallRouter from './basketball'
import BaseBallRouter from './baseball'
import NflRouter from './nfl'

const router = express.Router();

router.use('/soccer', SoccerRouter)
router.use('/tennis', TennisRouter)
router.use('/basketball', BasketBallRouter)
router.use('/nfl', NflRouter)
router.use('/baseball', BaseBallRouter)

export default router;
