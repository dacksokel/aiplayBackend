import express, { Request, Response } from 'express';
import passport from 'passport';

import setupPassport from '../../auth/passport';


import userRoutes from './userRoutes';
import authRoutes from './authRoutes';
import openaiRoutes from './openAiRoutes';
import promptRoutes from './promptRoutes'; // Add this line
import iaPremierLeagueRoutes from './iaPremierLeagueRoutes'; // Add this line
import SportRadarRoutes from './sportRadar'; // Add this line
import NewsApiRoutes from './newsApiRoutes'; // Add this line
import planRoutes from './planRoutes'
import paymentRoutes from './paymentRoutes'
import orderRoutes from './orderRoutes';

const router = express.Router();
setupPassport(passport)

const requireAuth = passport.authenticate('jwt', { session : false });

router.use('/v1/user',requireAuth,  userRoutes);
router.use('/v1/auth', authRoutes);
router.use('/v1/aiplay-openai',requireAuth,  openaiRoutes);

router.use('/v1/prompt',requireAuth,  promptRoutes); // Add this line
// router.use('/v1/ia_premier_league', iaPremierLeagueRoutes); // Add this line
router.use('/v1/ia_premier_league',requireAuth, iaPremierLeagueRoutes); // Add this line
router.use('/v1/sportradar',requireAuth, SportRadarRoutes); // Add this line
router.use('/v1/news', NewsApiRoutes); // Add this line
router.use('/v1/plans',requireAuth,  planRoutes)
// router.use('/v1/payments', paymentRoutes)
router.use('/v1/order', requireAuth, orderRoutes);

export default router;



