import express from 'express';
import PlanController from '../../controllers/PlanController';

const router = express.Router({});

router.post('/', PlanController.createPlan.bind(PlanController));
export default router;
