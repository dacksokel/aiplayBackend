import express from 'express';
import PaymentController from '../../controllers/PaymentController';

const router = express.Router({});

router.post('', PaymentController.createPayment.bind(PaymentController));

export default router;
