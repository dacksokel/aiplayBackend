import express, {Request, Response} from 'express';
import { upload } from '../../utils/multer';

import UserController from '../../controllers/UserController';

const router = express.Router({});

router.get('/all', UserController.getAllusers.bind(UserController));
router.get('/activate/:_id/:activationToken', UserController.activateUser.bind(UserController));
router.post('/forgot-password', UserController.forgotPassword.bind(UserController));
router.post('/resetpassword', UserController.resetPassword.bind(UserController))
router.put('/update/:id', UserController.updateUser.bind(UserController));
router.patch('/setpicture', upload.array("images", 5), UserController.setPicture.bind(UserController))

export default router;
