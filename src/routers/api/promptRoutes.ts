import express from 'express';
import PromptController from '../../controllers/PromptController';

const router = express.Router({});

router.get('/', PromptController.getAllPrompts.bind(PromptController));
router.get('/:id', PromptController.getPromptById.bind(PromptController));
router.post('/', PromptController.createPrompt.bind(PromptController));
router.put('/:id', PromptController.updatePrompt.bind(PromptController));
router.delete('/:id', PromptController.deletePrompt.bind(PromptController));

export default router;
