import express, { Request, Response } from 'express';

import AuthController from '../../controllers/AuthController';
// import allowRole from '../../middleware/allow-role';
// import Role from '../../enums/Role';
// const requireAuth = passport.authenticate('jwt', { session: false });

const router = express.Router({});

router.post('/login', AuthController.login.bind(AuthController));
router.post('/register', AuthController.register.bind(AuthController));
export default router;
