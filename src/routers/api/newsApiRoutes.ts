import express from 'express';
import NewsApiController from '../../controllers/newsApiController';

const router = express.Router({});

router.get('/', NewsApiController.getNews.bind(NewsApiController));


export default router;
