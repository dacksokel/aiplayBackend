import express, { Request, Response } from 'express';
import stripeRoutes from './stripe';

const router = express.Router();

router.use('/stripe', express.raw({ type: 'application/json' }), stripeRoutes);

export default router;