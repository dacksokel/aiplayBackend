import express from 'express';
import StripeController from '../../controllers/StripeController';

const router = express.Router({});
// router.get('/', (req, res) => res.json({ status: 'webhook stripe init OK' }));
router.post('/', StripeController.invoice.bind(StripeController));

export default router;
