import { Request, Response } from 'express';
import PlanService from '../service/planService';
import IPlan from '../interfaces/IPlan';

class PLanController {

    async createPlan(req: Request, res: Response) {
        try {
            const plan = (req.body as IPlan)
            const planResponse = await PlanService.createPlan(plan);
            return res.status(200).json(planResponse);
        } catch (error) {
            console.log(error)
            return res.status(500).json({ message: 'Failed to create plan' });
        }
    }

    async getPlans() { }

    async getPlanById() { }

    async updatePlan() { }

    async deletePlan() { }


}


export default new PLanController();