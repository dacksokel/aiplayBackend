import { Request, Response } from 'express';
import IPrompt from '../interfaces/IPrompt';
import PromptService from '../service/promptService';

class PromptController {
  private promptService: PromptService;

  constructor() {
    this.promptService = new PromptService();
  }

  public async getAllPrompts(req: Request, res: Response) {
    try {
      const prompts = await this.promptService.getAllPrompts();
      res.status(200).json(prompts);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  public async getPromptById(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const prompt = await this.promptService.getPromptById(id);
      if (prompt) {
        res.status(200).json(prompt);
      } else {
        res.status(404).json({ error: 'Prompt not found' });
      }
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  public async createPrompt(req: Request, res: Response) {
    try {
      const prompt  = (req.body as IPrompt);
      const createdPrompt = await this.promptService.createPrompt(prompt);
      res.status(201).json(createdPrompt);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  public async updatePrompt(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const prompt= (req.body as IPrompt);
      const updatedPrompt = await this.promptService.updatePrompt(id, prompt);
      if (updatedPrompt) {
        res.status(200).json(updatedPrompt);
      } else {
        res.status(404).json({ error: 'Prompt not found' });
      }
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  public async deletePrompt(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const deletedPrompt = await this.promptService.deletePrompt(id);
      if (deletedPrompt) {
        res.status(200).json(deletedPrompt);
      } else {
        res.status(404).json({ error: 'Prompt not found' });
      }
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
}

export default new PromptController();


