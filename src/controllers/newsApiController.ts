import { Request, Response } from "express";
import newsApiService from "../service/newsApiService";
import INews from '../interfaces/INews';
import NewsLanguage from '../enums/NewsLanguage';
import NewsQuestion from '../enums/NewsQuestionSport';


class NewsApiController {
    async getNews(req: Request, res: Response) {
        try {
            const query = req.query 
            const newsQuery: INews = {
                question: query.question as NewsQuestion,
                from: query.from ? (query.from as string) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000).toISOString().split('T')[0],
                to: query.to ? (query.to as string) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000).toISOString().split('T')[0],
                language: query.language as NewsLanguage,
                sortBy: query.sortBy ? (query.sortBy as string) : 'popularity'
            };  
            const news = await newsApiService.fetchNews(newsQuery);
        res.json(news);
        } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error retrieving news" });
        }
    }
}

export default new NewsApiController();