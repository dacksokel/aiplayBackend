import { Request, Response, NextFunction } from "express";
import AuthService from "../service/authService";
import IUser from "../interfaces/IUsers";
import mailService from "../service/mailService";
import TypeMAil from "../enums/typeMail";
import passport from "passport";
import { generateToken } from "../utils/jwt";

class AuthController {
  test(req: Request, res: Response) {
    res.json({ status: "test auth" });
  }

  async register(req: Request, res: Response) {
    const user: IUser = req.body;
    const response = await AuthService.register(user);
    res.json(response);
  }

  async login(req: Request, res: Response, next: NextFunction) {

    passport.authenticate("login", async (err: any, user: any, info: any) => {
      try {      
        if (err) {
          // const error = new Error(err.message);          
          // return next({ error });
          return res.json(info)
          
        }

        if (!user) {
          // const error = new Error("UnKnown user");
          // return next({ error });
          return res.json(info)
        }
    
        req.login(user, { session: false }, async (err) => {
          if (err) return next({ error: err });
          return res.json(user);
        });
      } catch (error) {
        return next({ error });
      }
    })(req, res, next);
  }

}
export default new AuthController();
