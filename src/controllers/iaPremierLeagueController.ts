import { Request, Response } from 'express';
import IaPremierLeagueService from '../service/iaPremierLeagueService';
// import langchainService from '../service/langchainService';
import config from '../../config/config';
import SoccerAiService from '../service/sportRadar/soccer/aiService'
import SoccerService from '../service/sportRadar/soccer/soccerService';
import TypeSport from '../enums/TypeSport';

import TennisServices from '../service/sportRadar/tennis/tennisServices';
import TennisAiService from '../service/sportRadar/tennis/TennisAiService';

import BaseBallService from '../service/sportRadar/baseball/baseballService';
import BaseBallAiService from '../service/sportRadar/baseball/baseballAiService';

import BasketBallAiService from '../service/sportRadar/basketball/basketBallAIService';
import BasketBallService from '../service/sportRadar/basketball/basketBallService';

import UserService from '../service/usersService';
import SourceChat from '../enums/SourceChat';

import IUser from "../interfaces/IUsers";

class iaPremierLeagueController { 
    async fetchData(req: Request, res: Response) {
        try {
            console.log('hola')
            const query = (req.query.team as string);    
            const user = req.user        
            // const data = await langchainService.getAnswer(query)
            // const data = await IaPremierLeagueService.getAllLeague('');
            // // const data = await IaPremierLeagueService.getStandingsByMessage(query);
            // res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async prediction3 (req: Request, res: Response){
        const question = (req.query.question as string)
        const user = (req.user as IUser);
        const competitionId = (req.query.competitionId as string)
        const sportType = (req.query.sportType as string)
        console.log(sportType)
        let competitionData= null;
        let response= null;
        
        //now check if the user have plan and user all
        

        if(sportType === TypeSport.Soccer){
            competitionData = await SoccerService.getCompetitionById(competitionId);
            const seasonData = await SoccerService.getSeasonsById(competitionData.season.id)
            response = await SoccerAiService.questionsAi(question, user, seasonData)
        }else if(sportType === TypeSport.Tennis){
            competitionData = await TennisServices.getCompetitionById(competitionId);
            const seasonData = await TennisServices.getSeasonsById(competitionData.id)
            response = await TennisAiService.questionsAi(question, user, seasonData)
        }else if(sportType === TypeSport.BaseBall){
            competitionData = await BaseBallService.getCompetitionById(competitionId);
            const seasonData = await BaseBallService.getSeasonsById(competitionData.season.id)
            response = await BaseBallAiService.questionsAi(question, user, seasonData)
        }else if(sportType === TypeSport.BasketBall){
            competitionData = await BasketBallService.getCompetitionById(competitionId);
            const seasonData = await BasketBallService.getSeasonsById(competitionData.season.id)
            response = await BasketBallAiService.questionsAi(question, user, seasonData)
        }else if(sportType === TypeSport.NFL){
            
        }else{
            res.json({
                error: 'Sport Type Error'
            })
        }

        //here put the user chat log
        if (response && 'content' in response && response.content !== null) {
            if(!user){
                res.status(400).json({ error: 'User not found' });
                return;
            }
            await UserService.saveChatLog(response.content, question ,user, sportType, SourceChat.Website); 
        }
        
        res.status(200).json(response)
    }

    async saveStandigs(req: Request, res: Response) { 
        try {
            await IaPremierLeagueService.fetchStandings();
            res.status(200).json({ message: 'Data fetched save' });
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async saveSeasonLeaders(req: Request, res: Response) {
        try {
            await IaPremierLeagueService.fetchSeasonLeaders();
            res.status(200).json({ message: 'Data fetched save' });
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async saveOverUnder(req: Request, res: Response) {
        try {
            await IaPremierLeagueService.fetchOverUnder();
            res.status(200).json({ message: 'Data fetched save' });
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async saveProbabilities(req: Request, res: Response) {
        try {
            await IaPremierLeagueService.fetchProbabilities();
            res.status(200).json({ message: 'Data fetched save' });
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async prediccion(req: Request, res: Response) { 
        try {
            const team1 = (req.query.team1 as string);
            const team2 = (req.query.team2 as string);
            const question = (req.query.question as string);
            const systemPrompt = (req.query.systemPrompt as string);
            const data = await IaPremierLeagueService.prediccion(team1, team2, question,systemPrompt);
            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async prediccion2(req: Request, res: Response) {
        try {
            const team1 = (req.query.team1 as string);
            const team2 = (req.query.team2 as string);
            const question = (req.query.question as string);
            const systemPrompt = (req.query.systemPrompt as string) || config.data.api.systemPromptDefault;
            const data = await IaPremierLeagueService.prediccion2(team1, team2, question, systemPrompt);
            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }
}

export default new iaPremierLeagueController();