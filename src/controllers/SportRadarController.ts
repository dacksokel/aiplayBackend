import { Request, Response } from 'express';
import SoccerService from '../service/sportRadar/soccer/soccerService';
import TennisService from '../service/sportRadar/tennis/tennisServices'
import BasketBallService from '../service/sportRadar/basketball/basketBallService';
import BaseballService from '../service/sportRadar/baseball/baseballService';
import NflService from '../service/sportRadar/nfl/nflService';
import ILimitAndPage from '../interfaces/ILimitAndPage';
import TypeSport from '../enums/TypeSport';
import { Types } from 'telegraf';


class SportRadarController {
  
    async getCompetitions(req: Request, res: Response) {
        try {
            const query  = req.query;
            const newQuery: ILimitAndPage = {
                limit: query.limit ? parseInt(query.limit.toString()) : 10,
                page: query.page ? parseInt(query.page.toString()) : 1
            }
            const typeSport = this.getBaseUrl(req);
            let data:any = null;
            // this be an Enum
            if(typeSport === TypeSport.Soccer){
                data = await SoccerService.getCompetitions();
            }else if(typeSport === TypeSport.Tennis){
                data = await TennisService.getCompetitions();
            }else if(typeSport === TypeSport.BasketBall){
                data = await BasketBallService.getCompetitions()
            }else if(typeSport === TypeSport.BaseBall){
                //code baseball
                data = await BaseballService.getCompetitions()
            }else if(typeSport === TypeSport.NFL){
                //code nfl
                data = await NflService.getCompetitions()
            }

            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }
    getBaseUrl(req: Request){
        const baseUrl:string[] = (req.baseUrl as string).split('/')
            return baseUrl[baseUrl.length - 1]
    }
    async getCompetitionById(req: Request, res:Response){
        const id = req.params.id;
        if(id){
            const typeSport = this.getBaseUrl(req);
            let responseData:any = null;
            if(typeSport === TypeSport.Soccer){
                responseData = await SoccerService.getCompetitionById(id)
            }else if(typeSport === TypeSport.Tennis){
                responseData = await TennisService.getCompetitionById(id);
            }else if(typeSport === TypeSport.BasketBall){
                responseData = await BasketBallService.getCompetitionById(id)
            }else if(typeSport === TypeSport.BaseBall){
                responseData = await BaseballService.getCompetitionById(id)
            }else if(typeSport === TypeSport.NFL){                
                responseData = await NflService.getCompetitionById(id)
            }
            res.status(200).json(responseData);
        }else{            
            res.status(500).json({
                status: 'competition not found'
            })
        }
    }

    async getSeasonById(req:Request, res: Response){
        const id:string =req.params.id;
        const typeSport = this.getBaseUrl(req);
        let response = null;

        if(typeSport === TypeSport.Soccer){
            response = await SoccerService.getSeasonsById(id);
        }else if(typeSport === TypeSport.Tennis){
            response = await TennisService.getSeasonsById(id);
        }else if(typeSport === TypeSport.BasketBall){
            response = await BasketBallService.getSeasonsById(id)
        }else if(typeSport === TypeSport.BaseBall){
            response = await BaseballService.getSeasonsById(id)
        }

        res.json(response)
    }
    async getSeasons(req:Request, res:Response){
        const query  = req.query;
        const newQuery: ILimitAndPage = {
            limit: query.limit ? parseInt(query.limit.toString()) : 10,
            page: query.page ? parseInt(query.page.toString()) : 1
        }
        const typeSport = this.getBaseUrl(req);
        let responseGetAllSeasonsAvalible = null
        if(typeSport === TypeSport.Soccer){
            responseGetAllSeasonsAvalible = await SoccerService.getAllSeasonAvalible()
        }else if(typeSport === TypeSport.Tennis){
            responseGetAllSeasonsAvalible = await TennisService.getAllSeasonAvalible();
        }else if(typeSport === TypeSport.BasketBall){
            responseGetAllSeasonsAvalible = await BasketBallService.getAllSeasonAvalible()
        }
        else if(typeSport === TypeSport.BaseBall){
            responseGetAllSeasonsAvalible = await BaseballService.getAllSeasonAvalible()
        }
        // else if(typeSport === TypeSport.NFL){                
        //     responseGetAllSeasonsAvalible = await NflService.getAllSeasonAvalible()
        // }

        res.json(responseGetAllSeasonsAvalible)
    }

    async getLeaders(req:Request, res: Response){
        const query  = req.query;
        const newQuery: ILimitAndPage = {
            limit: query.limit ? parseInt(query.limit.toString()) : 10,
            page: query.page ? parseInt(query.page.toString()) : 1
        }

        const response = await SoccerService.getAllLeaders()

        res.json(response)
    }

    async getLeadersById(req:Request, res:Response){
        const seasonId = req.params.id

        const response = await SoccerService.getLeadersById(seasonId)
        res.json(response)
    }

    async getStandingsBySeasonId(req:Request, res:Response){

        const seasonId = req.params.id;
        let response = null;
        const typeSport = this.getBaseUrl(req);
        if(typeSport === TypeSport.Soccer){
            response = await SoccerService.getStandingsBySeasonId(seasonId) 
        }else if(typeSport === TypeSport.BaseBall){
            response = await BaseballService.getStandingsBySeasonId(seasonId)
        }else if(typeSport === TypeSport.BasketBall){
            response = await BasketBallService.getStandingsBySeasonId(seasonId);
        }
        res.json(response)
    }

    async getProbabilitiesBySeasonId(req: Request, res: Response) { 
        const seasonId = req.params.id
        let response = null;
        const typeSport = this.getBaseUrl(req);

        try {
            if(typeSport === TypeSport.Soccer){            
                response = await SoccerService.probabilities(seasonId,0, false)
            }else if(typeSport === TypeSport.Tennis){            
                response = await TennisService.probabilities(seasonId,0, false)
            }else if(typeSport === TypeSport.BasketBall){            
                response = await BasketBallService.probabilities(seasonId,0, false)
            }else if(typeSport === TypeSport.BaseBall){
                response = await BaseballService.probabilities(seasonId, 0, false)
            }
            
            if(Object.prototype.hasOwnProperty.call(response, 'sport_event_probabilities')){
                res.json((response as any).sport_event_probabilities)
            }else{
                
                const fulldata = (response as any).fullData
                if(Object.prototype.hasOwnProperty.call(fulldata, 'message') ){
                    res.json({"messsage": fulldata.message})
                } else if (fulldata === undefined || fulldata === null) {
                    res.json({"message": "Not Found "})
                }
                else{
                    res.json(fulldata.sport_event_probabilities)
                }
    
            }
        } catch (error) {
            res.json({ error: 'Internal Server Error' });
        }
    }
    // async getCompetitorsByTeam(req: Request, res: Response) {
    //     try {
    //         const idTeam = (req.query.idTeam as string);
    //         const data = await SportRadarService.getCompetitorsByTeam(idTeam);
    //         res.status(200).json(data);
    //     } catch (error) {
    //         res.status(500).json({ error: 'Internal Server Error' });
    //     }
    // }
}

export default new SportRadarController();