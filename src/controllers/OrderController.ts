import { Request, Response } from 'express';
import OrderService from '../service/orderService';

class OrderController { 

    async creteOrder(req: Request, res: Response) { 
        
        const order = req.body;
        const user = req.user;
        const id = (user as any)._id;
        const response = await OrderService.createOrder(id, order);

        res.json(response)
        
    }
}

export default new OrderController();