import { Request, Response } from "express";
import paymentService from "../service/paymentService";

class PaymentController { 

    async createPayment(req: Request, res: Response) { 
        try {
            const paymentData = req.body;
            const paymentSession = await paymentService.createPaymentSession(paymentData);
            res.status(200).json(paymentSession);
        } catch (error) {
            res.status(500).json(error);
        }
    }

}

export default new PaymentController();