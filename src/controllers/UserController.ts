import { Request, Response } from 'express';
import UserService from '../service/usersService';
import IUser from '../interfaces/IUsers';
import usersService from '../service/usersService';

class UserController {
  async createUser(req: Request, res: Response) {
    try {
      const userData = req.body;
      const createdUser = await UserService.createUser(userData);
      res.status(201).json(createdUser);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error creating user' });
    }
  }

  async getUser(req: Request, res: Response) {
    try {
      const userId = req.params.id;
      const user = await UserService.getUser(userId);
      res.json(user);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error retrieving user' });
    }
  }

  async getAllusers(req: Request, res: Response) {
    try {
      const users = await UserService.getAllusers();
      res.json(users);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error retrieving users' });
    }
  }

  async updateUser(req: Request, res: Response) {
    try {
      const userId = req.params.id;
      const updatedUserData = req.body;
      const updatedUser = await UserService.updateUser(userId, updatedUserData);
      res.json(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error updating user' });
    }
  }

  async deleteUser(req: Request, res: Response) {
    try {
      const userId = req.params.id;
      await UserService.deleteUser(userId);
      res.json({ message: 'User deleted successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Error deleting user' });
    }
  }

  usersTest(req: Request, res: Response) {
    res.status(200).json({ status: 'OK' });

  }
  async forgotPassword(req: Request, res: Response) {
    const { email } = req.body
    const message = await usersService.forgetPasswordSendMail(email);
    res.json(message)
  }

  async resetPassword(req: Request, res: Response) {
    const { email, _id, resetPasswordToken, password, newPassword } = req.body
    const message = await UserService.resetPassword(email, _id, resetPasswordToken, password, newPassword);
    res.json(message)
  }
  async activateUser(req: Request, res: Response) {
    const { _id, activationToken } = req.params;
    const query = req.query;
    if (!query.redirectUrl) {
      // esto hay que ponerlo en config.data yua que es una url por default
      query.redirectUrl = 'https://www.airplay.bet'
    }
    const activation = await UserService.activateAccount(_id, activationToken);

    res.redirect(query.redirectUrl as string);
    // res.json(activation)
  }

  async setPicture(req: Request, res: Response) {
    const user = (req.user as IUser);
    const imageFile = (req.files as any);

    if (imageFile.size > 10 * 1024 * 1024) {
      res.status(400).json({ message: 'Image size exceeds the limit of 10MB' });
    } else {
      const response = await UserService.setPictureProfile(user, imageFile);
      res.status(200).json(response);
    }    
  }

}

export default new UserController();
