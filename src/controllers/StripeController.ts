import { Request, Response } from 'express';
import stripeService from '../service/stripeService';
import StriperService from '../service/stripeService';

class StripeController {
    async invoice(req: Request, res: Response) {
        const response = await StriperService.invoice(req);
        if (!response) {
            res.sendStatus(400);
        }
        console.log('webhook completed')
        res.send();
    }
}

export default new StripeController();