import OpenAiDefault from "../enums/OpenAiDefault";

export default interface IOpenAi {
    rol: string | OpenAiDefault.rolSystem | OpenAiDefault.rolUser | OpenAiDefault.rolAssistant;
    message: Array<any>;
    model: string | OpenAiDefault.model35turbo;
}



