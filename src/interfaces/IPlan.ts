export default interface IPlan {
    code?: string;
    status: boolean;
    priceByMonth: number;
    priceByYear: number;
    priceByMonthWithDiscount: number;
    priceByYearWithDiscount: number;
    discount: number;
    discountByYear: number;
    discountByMonth: number;
    name: string;
    // category?: Schema.Types.ObjectId;
    description?: string;
    accessToChat: boolean;
    accessToForo: boolean;
    accessToStadistics: boolean;
    acceessToPredictionsByWeek: boolean;
    amountOfPredictionsByWeek: number;
    amountOfPredictionsByMonth: number;
    startDate?: Date;
    endDate?: Date;
    image?: string;
    currency: string;
    trialDays?: number;
    avalibleForTrial: boolean;
}