import UserRole from '../enums/UsersRoles';

interface User {
  _id?: string;
  username?: string;
  password: string;
  name: string;
  lastName: string;
  country: string;
  state: string;
  birthdate: Date;
  email: string;
  role?: UserRole;
  activate?: boolean;
  activationToken?: string;
  resetPasswordToken?: string;
  createdAt?: Date;
  updatedAt?: Date;
  isDeleted?: boolean;
  msg?:string,
  picture?:string
}

export default User;