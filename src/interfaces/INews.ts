import NewsLanguage from '../enums/NewsLanguage';
import NewsQuestionSport from '../enums/NewsQuestionSport';

export default interface INews {
    question: string | NewsQuestionSport.default;
    from?: Date | null | string;
    to?: Date | null | string;
    language: string | NewsLanguage.default;  
    sortBy?: string;  
}