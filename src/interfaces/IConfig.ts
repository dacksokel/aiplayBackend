export default interface Config {
    env: string;
    port: number;
    db: {
        uri: string;
    };
    jwt: string;
    urlServer: string;
    smtp: {
        port: number,
        password: string,
        email: string,
        server: string,
        tls: boolean,
    };
    urlActivateAccount: string;
    urlResetPassword: string;
    company: {
        name: string,
    }
    openaiApiKey: string;
    sportRadar: {
        soccer: string,
        tennis: string,
        nfl: string,
        baseball: string,
        basketball: string
    },
    data: any;
    newsapi: {
        apiKey: string,
    },
    stripe: {
        secretKey:string,
        signingSecret:string
    }
}

