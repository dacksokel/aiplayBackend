export default interface IStandingSoccer {
    generated_at: string,
    season_form_standings: Array<object>
} 