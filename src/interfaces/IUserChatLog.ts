import { ObjectId } from 'mongodb';
import SourceChatLog from '../enums/SourceChat';

interface IUserChatLog {
    user: string | ObjectId,
    question: String,
    answer: String,
    sportType: string,
    source: string | SourceChatLog.Website | SourceChatLog.Telegram | SourceChatLog.Discord
}

export default IUserChatLog;