import { Schema, Types } from 'mongoose';
import Wallet from '../enums/wallet';

export default interface IWaller {
    _id?: string;
    userId: Schema.Types.ObjectId;
    balance: Wallet;
    transactions: Array<{ type: Wallet.transactionTypeDeposit | Wallet.transactionTypeWithdraw; amount: number; }>;
}