import PromptStatus from "../enums/promptStatus";

export default interface IPrompt {
    _id?:string;
    prompt: string;
    description: string;
    titulo: string;
    exmaple: string;
    active?: PromptStatus;
}

