import { ObjectId } from 'mongodb';

interface IToken {
    expiryTime: Date,
    resourceRef: string | ObjectId,
    tokenValue?: String,
    requestedBy?: String | ObjectId,
    roles: Array<string>,
    creationMethod: String,
    deleted?: boolean
}

export default IToken;