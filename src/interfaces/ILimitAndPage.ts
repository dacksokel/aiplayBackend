export default interface LimitAndPage{
    limit: number | 1;
    page: number | 1;
}