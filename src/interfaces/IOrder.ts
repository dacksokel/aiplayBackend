import IPlan from './IPlan';

export default interface IOrder { 
    orderCode: string;
    userId: string;
    plan: {
        planId: string;
        planName: string;
        planPrice: number;
        planQuantity: number;
        extraData?: object;
    };
    paymentMethod?: {
        transactionId: string;
        amount: number;
    }
    status: string;
}