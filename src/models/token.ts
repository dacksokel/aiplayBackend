import { Schema, Document, model } from 'mongoose';
import  IToken from '../interfaces/IToken';


const TokenSchema = new Schema<IToken & Document>({
    expiryTime: { type: Date },
    resourceRef: { type: Schema.Types.ObjectId },
    tokenValue: { type: String },
    requestedBy: { type: Schema.Types.ObjectId },
    roles: { type: [String] },
    creationMethod: { type: String },
    deleted: { type: Boolean }
}, { timestamps: true });


const TokenModel = model<IToken & Document>('Token', TokenSchema);

export { TokenModel, IToken };
