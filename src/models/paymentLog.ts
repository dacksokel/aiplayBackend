import { Schema, Document, model } from 'mongoose';

const paymentLog = new Schema({
    paymentId: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    amount_total: {
        type: Number,
        require: true
    },
    fullData: {
        type: Object,
        require: true
      }
},{timestamps: true});

export default model('PaymentLog', paymentLog);
