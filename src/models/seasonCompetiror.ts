import { Schema, model } from 'mongoose';

const seasonCompetitorsSchema = new Schema({
    data: {
        type: Object,
        required: false
    },
    fullData: {
        type: Object,
        required: false
    },
    seasonId:{
        type: String,
        require: true
    },
    competitionType: {
        type: String,
        require: true
    },
    source: {
        type: String,
        require: true
    }
},{timestamps:true});

export default model('seasonCompetitors', seasonCompetitorsSchema);
