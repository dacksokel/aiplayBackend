import { Schema, model } from 'mongoose';

const planSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true,
        index: true,
        uppercase: true
    },
    status: {
        type: Boolean,
        required: true,
    },
    price: {
        type: Number,
        required: true
    },
    priceByMonth: {
        type: Number,
        required: true
    },
    priceByYear: {
        type: Number,
        required: true
    },
    priceByMonthWithDiscount: {
        type: Number,
        required: true
    },
    priceByYearWithDiscount: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    discountByYear: {
        type: Number,
        required: true
    },
    discountByMonth: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    // category: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'CategoryPlan',
    //     index: true
    // },
    description: {
        type: String,
        required: false
    },
    accessToChat: {
        type: Boolean,
        required: true
    },
    accessToForo: {
        type: Boolean,
        required: true
    },
    accessToStadistics: {
        type: Boolean,
        required: true
    },
    acceessToPredictionsByDay: {
        type: Boolean,
        required: true,
        default: false
    },
    acceessToPredictionsByWeek: {
        type: Boolean,
        required: true,
        default: false
    },
    amountOfPredictionsByDay:{
        type: Number,
        require: true,
        default: 1
    },
    amountOfPredictionsByWeek: {
        type: Number,
        required: true,
        default: 1
    },
    amountOfPredictionsByMonth: {
        type: Number,
        required: true,
        default: 1
    },
    startDate: {
        type: Date,
        index: true
    },
    endDate: {
        type: Date,
        index: true
    },
    image: {
        type: String,
        required: false
    },
    currency: {
        type: String,
        required: true
    },
    avalibleForTrial: {
        type: Boolean,
        required: true
    },
    trialDays: {
        type: Number,
        required: false
    },

}, { timestamps: true });

export default model('plans', planSchema);
