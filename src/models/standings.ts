import { Schema, model } from 'mongoose';

const standingsSchema = new Schema({
  data: {
    type: String,
    required: true
  },
  fullData: {
    type: Object,
    required: false
  },
  seasonId: {
    type: String,
    require: true
  },
  competitionType:{
    type: String,
    require: true
  },
  sourceData: {
      type: String,
      require: true
  }
},{timestamps: true});

export default model('Standings', standingsSchema);
