import { Schema, model } from 'mongoose';

const competitionsSchema = new Schema({
    data: {
        type: String,
        required: false
    },
    fullData: {
        type: Object,
        required: false
    },
    competitionType:{
        type: String,
        require:true
    },
    source: {
        type: String,
        require: true
    }
},{timestamps: true});

export default model('Competitions', competitionsSchema);
