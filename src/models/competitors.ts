import { Schema, model } from 'mongoose';

const competitorsSchema = new Schema({
    data: {
        type: Object,
        required: false
    },
    fullData: {
        type: Object,
        required: false
    },
},{timestamps: true});

export default model('Competitors', competitorsSchema);
