import { Schema, Document, model } from 'mongoose';
import bcrypt from 'bcrypt';
import User from '../interfaces/IUsers';
import UserRole from '../enums/UsersRoles';
import { v4 as uuidv4 } from 'uuid'; // Import uuidv4 from the uuid package

const userSchema = new Schema<User & Document>({
    username: { type: String },
    password: { type: String, required: true },
    name: { type: String, required: true },
    lastName: {type: String, required: true },
    email: { type: String, required: true },
    country: { type: String, required: true},
    state: { type: String },
    birthdate: { type: Date, required: true},
    role: { type: String, enum: Object.values(UserRole), default: UserRole.Freemium },
    activate: { type: Boolean, default: false },
    activationToken: { type: String, default: uuidv4() }, // Add a default value using uuidv4()
    resetPasswordToken: { type: String },
    isDeleted: {
        type: Boolean,
        default: false
    },
    picture:{
        type: String,
        default: '',
        required: false
    },
    plan: {
        type: Schema.Types.ObjectId,
        default: 'no plan',
        required: false
    }
}, { timestamps: true });

userSchema.pre<User & Document>('save', async function (next) {
    if (this.isModified('password')) {
        const salt = await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password, salt);
    }
    next();
});

userSchema.methods.comparePasswords = async function (candidatePassword: string): Promise<boolean> {
    // if no password is set, then any password is invalid
    if (this.password) {
        return bcrypt.compare(candidatePassword, this.password);
    } else {
        return false;
    }

};

const UserModel = model<User & Document>('User', userSchema);

export { UserModel, User };
