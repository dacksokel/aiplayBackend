import { Schema, model } from 'mongoose';

const competitionsSeasonsSchema = new Schema({
    data: {
        type: Object,
        required: false
    },
    fullData: {
        type: Object,
        required: false
    },
    competitionId: {
        type: String,
        require: true
    },
    competitionType:{
        type: String,
        require: true
    },
    sourceData: {
        type: String,
        require: true
    }
},{timestamps: true});

export default model('CompetitionsSeasons', competitionsSeasonsSchema);
