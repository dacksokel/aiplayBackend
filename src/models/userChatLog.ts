import { Schema, Document, model } from 'mongoose';
import SourceChat from '../enums/SourceChat';

const userLogChat = new Schema({
    user: {
        type: Schema.Types.ObjectId ,
        required: true
    },
    question: {
        type: String,
        required: true
    },
    answer:{
        type: String,
        require:true
    },
    sportType: {
        type: String,
        require: true
    },
    source:{
        type: String,
        require: true,
        enum: Object.values(SourceChat),
        default: SourceChat.Website
    }
},{timestamps: true});

export default model('UserLogChat', userLogChat);
