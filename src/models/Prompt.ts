import mongoose, { Schema, Document } from 'mongoose';
import IPrompt from '../interfaces/IPrompt';
import PromptStatus from '../enums/promptStatus';

const PromptSchema: Schema = new Schema({
    prompt: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    titulo: {
        type: String,
        required: true,
    },
    example: {
        type: String,
        required: true,
    },
    active: {
        type: Boolean,
        default: PromptStatus.Enabled,
    },
}, { timestamps: true });

export const PromptModel = mongoose.model<IPrompt & Document>('Prompt', PromptSchema);

