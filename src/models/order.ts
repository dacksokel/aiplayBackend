import { Schema, model } from 'mongoose';


const orderShema = new Schema(
    {
        orderCode: {
            type: String,
            unique: true,
            require: true
        },
        userId: {
            type: Schema.Types.ObjectId,
            require: true,
            ref: 'user'
        },
        planCode: {
            type: String,
            require: true
        },
        paymentMethod: {
            transactionId: { type: String, required: true },
            amount: { type: Number, required: true }
        },
        status: {
            type: String,
            require: true
        }
    },
    { timestamps: true }
);
export default model('orders', orderShema);
