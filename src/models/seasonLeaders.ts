import { Schema, model } from 'mongoose';

const seasonLeadersSchema = new Schema({
  data: {
    type: String,
    required: true
  },
  fullData: {
    type: Object,
    required: false
  },
  seasonId: {
    type: String,
    require: true
  },
  competitionType: {
    type: String,
    require: true
  },
  source: {
      type: String,
      require: true
  }
},{timestamps: true});

export default model('SeasonLeaders', seasonLeadersSchema);
