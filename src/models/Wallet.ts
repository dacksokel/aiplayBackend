import { Schema, Document, model } from 'mongoose';
import IWaller from '../interfaces/IWallet';
import Wallet from '../enums/wallet';

const walletSchema = new Schema<IWaller & Document>({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    balance: {
        type: Number,
        required: true,
        default: Wallet.balanceDefault
    },
    transactions: {
        type: [{
            type: {
                type: String,
                enum: [Wallet.transactionTypeDeposit, Wallet.transactionTypeWithdraw],
                required: true
            },
            amount: {
                type: Number,
                required: true
            }
        }],
        required: true,
        default: []
    }
}, { timestamps: true })

const WalletModel = model<IWaller & Document>('wallet', walletSchema);

export { WalletModel, IWaller };