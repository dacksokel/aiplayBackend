enum UserRole {
    Freemium = 'freemium',
    Premium = 'premium',
    Admin = 'admin',
    User = 'user'
  }
  
  export default UserRole;