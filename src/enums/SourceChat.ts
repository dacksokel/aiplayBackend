enum SourceChat {
    Website = 'website',
    Telegram = 'telegram',
    Discord = 'discord',
}

export default SourceChat;
