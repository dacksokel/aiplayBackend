enum PromptStatus {
    Enabled = 1,
    Disabled = 0,
}

export default PromptStatus;
