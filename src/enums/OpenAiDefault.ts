enum OpenAiDefault {
    model35turbo = 'gpt-3.5-turbo',
    model35turbo16k = 'gpt-3.5-turbo-1106',
    // model35turbo16k = 'gpt-3.5-turbo-16k-0613',
    model4 = 'gpt-4',
    model432k0613 = 'gpt-4-32k-0613',
    model4320314 = 'gpt-4-32k-0314',
    model432k = 'gpt-4-32k',
    rolSystem =  'system',
    rolUser = 'user',
    rolAssistant = 'assistant'
}

export default OpenAiDefault;