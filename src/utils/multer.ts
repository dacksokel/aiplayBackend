import multer from "multer";
import fs from 'fs';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const folderPath = "./public/img/profile";
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath, { recursive: true });
    }
    cb(null, folderPath);
  },

  filename: function (req: any, file: any, cb: any) {
    const extension = file.originalname.split('.').pop();
    const newName = `${Date.now()}.${extension}`;
    cb(null, newName);
  },
});
const fileFilter = (req: any, file: any, cb: any) => {
  if (
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png"
  ) {
    cb(null, true);
  } else {
    cb(new Error("Image uploaded is not of type jpg/jpeg or png"), false);
  }
};

export const upload = multer({ storage: storage, fileFilter: fileFilter });
