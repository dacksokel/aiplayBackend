import jwt from 'jsonwebtoken';
import config from '../../config/config';
import IUser from '../interfaces/IUsers';

export const generateToken = (user: IUser) => {
    const expiresIn = 43200; // Tiempo de expiración en segundos (12 horas)
    const algorithm = 'HS256';

    try {
        const token = jwt.sign({ user }, config.jwt, { expiresIn, algorithm });
        return token;
    } catch (error) {
        // Manejo del error al generar el token
        console.error('Error al generar el token:', error);
        throw error;
    }
};

export const verifyToken = (token: string) => {
    try {
        const decoded = jwt.verify(token, config.jwt);
        return decoded;
    } catch (error) {
        // Manejo del error al verificar el token
        console.error('Error al verificar el token:', error);
        throw error;
    }
};
