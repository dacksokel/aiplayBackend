/***
 * pasos:
 * primero se debe preguntar por el competition id, luego de obtenerla entonces se debe enviar dicho competition id, junto con la pregunta a la prediccion3
 * en la prediccion 3 se guardara en un objeto de usuarios {'usuarioId':{competitionId,seasonID}} se debe buscar el season id y agregarlo en este objeto, 
 * de esta forma comenzaremos con la prediccion 3 preguntando soobre la tabla de pociciones de la competicion en cuestion, pasandole siempre la seasonId
 * y dejandola especificada en el log de la function para que la ia pueda saber lo que debe de hacer con dichos valores :D, las respuestas y preguntas realizadas por el usuario se deben agregar
 * en el objeto de ser posibles
 * 
 */
import config from '../../../../config/config';
import openAiService from '../../openAiService';
import SoccerService from './soccerService';

import TypeSport from '../../../enums/TypeSport';

class AiService {

    async questionsAi(question: string, user: any, soccerData: any) {
        const messages = [
            {
                role: 'system',
                content:
                    config.data.api.systemPromptSoccer,
            },
            {
                role: 'user',
                // content:
                //     `ruta de busqueda de datos: 
                //     primero standings, de alli puedes obtener ids de los equipos, luego con esos ids puedes buscar en overUnder y probabilities, getcompetitorlastgame es para obtener los datos de un equipo en su ultimo partido, tambien recuerda de responder como un profecionar analista deportivo, no como alguien casual, dando respuestas simples y claras, las probabilidades de goles recuerda no darlas con decimales sino con numeros enteros.
                // ${question}`
                content: `${question} ${JSON.stringify(soccerData)}`
            },
        ];

        //now functions
        const functions = [

            {
                name: 'standingFullTimeTotalCount',
                description: `Primeros se debe consultar esto Pregunta por el tamano total full time de standing de la season: ${soccerData.season.id}, para luego, lo cual dara un numero que expresara el numero de veces que tengra que preguntar a standingFullTimeTotal, el cual ira entregando respuesta en base al numero que le mandes, ejemplo:  standingsFullTimeTotalCount te dara 20, entonces deberas comenzar usando 1, 2, 3 ,4 ,.... hasta el 20 para que de esa forma te de varias informaciones`,
                parameters: {
                    type: 'object',
                    properties: {
                        seasonId: {
                            type: 'string',
                            descriptcion: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:xxxxx" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        }
                    },
                },
                require: ['seasonId'],

            },
            {
                name: 'standingFullTimeTotal',
                description: 'para poder usar esta funcion se debe obtener la cantidad de veces que se consultara de standingFullTimeTotalCount, entregara dato a dato segun el numero que le mandes, el cual dependera del total que previamente se le entregue, si al consultar anteriolmente le dieron 20 el debera consultar para ir aprendiendo sobre la informacion en base al numero total y terminara cuando el total sea igual al numero de veces que pregunto',
                parameters: {
                    type: 'object',
                    properties: {

                        contadorStadings: {
                            type: 'number',
                            description: 'es el numero total que le dara al standingsFullTimeTotal, de esta forma ir recibiendo data'
                        },
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:xxxxx" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        }

                    }
                },
                require: ['contadorStadings', 'seasonId'],
            },
            {
                name: 'probabilitiesCount',
                description: `
                    recuerda consultar standingFullTimeTotal, para poder saber el estatus de los equipos y sus posiciones
                        entrega el numero de probabilidades que se pueden consultar en la season ${soccerData.season.id}, para que el getProbabilitiesOneToOnelos consulte uno a uno 
                    `,
                parameters: {
                    type: 'object',
                    properties: {
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:xxxxx" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        }
                    }
                },
                require: ['seasonId'],
            },
            {
                name: 'getProbabilitiesOneToOne',
                description: `
                    Siempre que se vaya a usar esta funcion, se debe obtener la cantidad de veces que se consultara con la funcion probabilitiesCount, nunca asumir que ya tienes el numero de conteo de otra funcion que no sea la antes mensionada, entregara dato a dato segun el numero que le mandes, el cual dependera del total que previamente se le entregue, si al consultar anteriolmente le dieron 20 el debera consultar para ir aprendiendo sobre la informacion en base al numero total y terminara cuando el total sea igual al numero de veces que pregunto`,
                parameters: {
                    type: 'object',
                    properties: {

                        contadorProbabilities: {
                            type: 'number',
                            description: 'es el numero total que le dara al getProbabilitiesOneToOne, de esta forma ir recibiendo data uno a uno hasta que termine el conteo'
                        },
                        seasonId: {
                            type: 'string',
                            descriptcion: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:xxxxx" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        },
                        total: {
                            type: 'number',
                            description: `es el total de probabilidades obtenidos desde probabilitiesCount y es necesario para comparar todas las probabilidades posibles de la season que tiene el id ${soccerData.season.id}`
                        }

                    }
                },
                require: ['contadorProbabilities', 'seasonId', 'total'],
            },
            {
                name: 'getProbabilitiesByCompetitorId',
                description: 'con esta function se puede buscar directamente las probabilities de un competidor, usando el id del mismo, la cual puede ser obtenida en las standings',
                parameters:{
                    type: 'object',
                    properties:{
                        competitorId:{
                            type:'string',
                            description: ` es el id del competidor que se obtiene desde los standigs, es lo que se usa para identificar cada equipo de forma que sea unico un ejemplo del id seria esto que te coloco entre comillas, pero el ejemplo que te doy no debes usarlo en caso reales, solo debes buscar algo asi parecedio en los standings "sr:competitor:numero"`
                        },
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:numero" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        },
                        competitorName: {
                            type: 'string',
                            description: `es el nombre del competidor que se encuentra en la pregunta hecha por el usuario en ${question}, ejemplo de los nombres: que estarann el las comillas`
                        }
                    }
                },
                require: ['competitorId', 'seasonId', 'competitorName']
            },
            {
                name: "getLeadersPointsCount",
                description:`Obtiene un total de veces que deben consultar otra funcion llamada getleadersPoints, dicho valor que esta funcion te dara es un valor numerico, puede ser 30 1 o 2, una vez obtenido este valor debes consultar la cantidad de veces que sea necesaria la otra funcion ya mensionada, cave decir que esta funcion recibe un seasonId el cual es obtenido del ${soccerData.season.id} `,
                parameters: {
                    type: 'object',
                    properties:{
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:numero" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        },
                    }
                },
                require: ['seasonId']
            },
            {
                 name:"getLeadersPoints",
                 description: `con esta funcion vas obteniendo uno a uno los jugadores que estan mejor posicionados segun la cantidad de puntos que llevan en la tabla de leaders, se debe obterner primero el total de veces que se debe consultar esta funcion desde, getLeadersPointsCount`,
                 parameters: {
                    type: 'object',
                    properties:{
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:numero" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        },
                        contadorLeadersPoints: {
                            type: 'number',
                            description: `es un cotador que tiene un numero total de veces que se deberia de llamar a la funcion para obtener este valor se debe consultart la function getLeadersPointsCount`                        
                        }
                    }
                 },
                 require: ['seasonId', 'contadorLeadersPoints']
            },
            {
                name: 'getOverUnderByCompetitorId',
                description: 'con esta function se puede buscar directamente las estadisticas de overUnder de un competidor, usando el id del mismo, la cual puede ser obtenida en las standings, un dato importate de los over unders es que te entregan pronosticos de goles con decimales, debes redondearlos a enteros, por ejemplo 1.5 cantidad de goles seria 2 goles, 0.5 goles seria 1 gol y asi sucesivamente',
                parameters:{
                    type: 'object',
                    properties:{
                        competitorId:{
                            type:'string',
                            description: ` es el id del competidor que se obtiene desde los standigs, es lo que se usa para identificar cada equipo de forma que sea unico un ejemplo del id seria esto que te coloco entre comillas, pero el ejemplo que te doy no debes usarlo en caso reales, solo debes buscar algo asi parecedio en los standings "sr:competitor:numero"`
                        },
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:numero" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${soccerData.season.id}`,
                            emun: [soccerData.season.id]
                        },
                        competitorName: {
                            type: 'string',
                            description: `es el nombre del competidor que se encuentra en la pregunta hecha por el usuario en ${question}, ejemplo de los nombres: que estarann el las comillas "RealMadrid, Manchester city, etc"`
                        }
                    }
                },
                require: ['competitorId', 'seasonId', 'competitorName']
            },
            {
                name: 'howAnswerProbabilities',
                description: 'es una funcion que instruye como se debe responder ante las probabilidades de comparacion entre equipos',
                parameters: {
                    type: 'object',
                    properties:{}
                }
            }
        ];
        return await openAiService.conecctionWitFunctionsCall2(functions, messages, TypeSport.Soccer);

    }

    async callFunction(function_call: any) {
        const args = JSON.parse(function_call.arguments);
        switch (function_call.name) {
            case 'standingFullTimeTotalCount':
                return await this.standingFullTimeTotalCount(args['seasonId']);
            case 'standingFullTimeTotal':
                return await this.standingFullTimeTotal(args['contadorStadings'], args['seasonId']);
            case 'probabilitiesCount':
                return await this.probabilitiesCount(args['seasonId']);
            case 'getProbabilitiesOneToOne':
                return await this.getProbabilitiesOneToOne(args['contadorProbabilities'], args['total'], args['seasonId']);
            case 'getProbabilitiesByCompetitorId':
                return await this.getProbabilitiesByCompetitorId(args['seasonId'], args['competitorId'], args['competitorName']);
            case 'getLeadersPointsCount':
                return await this.getLeadersPointsCount(args['seasonId']);
            case 'getLeadersPoints':
                return await this.getLeadersPoints(args['contadorLeadersPoints'], args['seasonId']);
            case 'getOverUnderByCompetitorId':
                return await this.getOverUnderByCompetitorId(args['seasonId'], args['competitorId'], args['competitorName'])
            case 'howAnswerProbabilities':
                return this.howAnswerProbabilities()

            default:
                throw new Error('No function found');
        }
    }

    async standingFullTimeTotalCount(seasonId: string) {
        console.log('comenzado a buscar las cantidades')
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        if (seasonId) {
            const stadingForm = await SoccerService.getStandingsBySeasonId(seasonId)
            const fullTimeTotal = stadingForm?.standings[0].groups[0].form_standings
            console.log(fullTimeTotal.length)
            return `te recomiendo que una vez obtenido este numero: ${fullTimeTotal.length} consultes todo standingFullTimeTotal para que puedas tener toda tu data`

        }

        return "debe volver a consultar para obtener un numero"
    }

    async standingFullTimeTotal(numerito: number, seasonId: string) {
        if (numerito) {
            numerito = numerito - 1
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const stadingForm = await SoccerService.getStandingsBySeasonId(seasonId)
            const fullTimeTotal = stadingForm?.standings[0].groups[0].form_standings
            console.log(`este es el numerito: ${numerito}`)
            return fullTimeTotal[numerito]

        } else {
            return 'debes llamar primero a standingFullTimeTotalCount '
        }
    }

    async getStandings(seasonId: string) {
        console.log(`this its getStandings in prediccion3 id sseason: ${seasonId}`)
        const stadingForm = await SoccerService.getStandingsBySeasonId(seasonId)

        return JSON.stringify(stadingForm?.standings)
    }
//leaders data
    async getLeaders(seasonId: string) {
        const leaders = await SoccerService.getLeadersById(seasonId)
        return leaders
    }
    async LeadersPoints(seasonId:string){
        const leaders = await SoccerService.leaders(seasonId, 0, false)
        const leadersFullData = (leaders as any).fullData.lists;
        const leadersPoints = leadersFullData.find((list: any)=> list.type === 'points').leaders
        return leadersPoints;
    }
    async getLeadersPointsCount(seasonId: string){
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        
        const leadersPoints = await this.LeadersPoints(seasonId)
        const leadersPointsLen =(leadersPoints as any).length        

        return `este es el numero de veces que debes consultar el getLeadersPoints total: ${leadersPointsLen}`;
    }
    async getLeadersPoints(contadorLeadersPoints:number, seasonId:string){
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        // const leadersPoints = await this.LeadersPoints(seasonId)
        // const leadersPointsLen =(leadersPoints as any).length
        if(contadorLeadersPoints){
            contadorLeadersPoints = contadorLeadersPoints - 1;
            const leadersPoints = await this.LeadersPoints(seasonId)
            const leaderPointsPart = (leadersPoints as any)
            // console.log(leaderPointsPart[contadorLeadersPoints])
            return leaderPointsPart[contadorLeadersPoints]
     
        }
        return 'debes consultar primero getLeadersPointsCount para obterner el numero de veces que debes consultar esta funcion'
    }

//probabilities data
    async getProbabilities(seasonId: string, team: string) {
        const probabilities = await SoccerService.probabilities(seasonId, 0, true)

        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
                return rest;
            }
        }).filter((item: any) => item !== undefined);
        // console.log(response)
        console.log(team)
        return JSON.stringify(response);
    }
    async probabilitiesCount(seasonId: string) {
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        if (seasonId) {
            const probabilities = await SoccerService.probabilities(seasonId, 0, false)
            const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
                const { channels, ...rest } = item.sport_event;
                // console.log(rest)
                const providedDate = new Date(rest.start_time);

                // Get the current date
                const currentDate = new Date();

                if (providedDate > currentDate) {
                    return rest;
                }
            }).filter((item: any) => item !== undefined);

            return response.length
        }
        return 'si desea obtener las probabilidades de estadisticas de los equipos debes volver a llamar a esta funcion para obtener un numero'
    }

    async getProbabilitiesOneToOne(contador: number, totalProbabilities: number, seasonId: string) {
        console.log(`total probabilities ${totalProbabilities}`)
        const probabilities = await SoccerService.probabilities(seasonId, 0, false)
        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
                return rest;
            }
        }).filter((item: any) => item !== undefined);
        if (response.length !== totalProbabilities) {
            return 'debes consultar probabilitiesCount para obtener el numero de veces que se debe consultar las probabilidades, y debes consultar toda las probabilidades para que tengas una vision general de los equipos uno a uno en la function getProbabilitiesOneToOne'
        }
        if (contador) {
            contador = contador - 1
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const datosProbabilities = JSON.stringify(response[contador]);

            return `contador: ${contador}, total de probabilidades: ${totalProbabilities} y esto son los datos de la probabilidades: ${datosProbabilities}.
            si el contador no es igual al total enotces debes seguir consultando
            `

        } else {
            return 'debes llamar primero a probabilitiesCount'
        }
    }

    async getProbabilitiesByCompetitorId(seasonId: string, competitorId: string, competitorName: string) {
        let competitors = await SoccerService.seasonCompetitors(seasonId);
        competitors = (competitors as any).fullData.season_competitors.some((competitor: any) => {
            if(competitor.id === competitorId && competitor.name.includes(competitorName)){
                console.log(competitor)
                return true;
            }
        })
        if (competitorId && competitors) {
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const probabilities = await SoccerService.probabilities(seasonId, 0, false)
            const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
                const { channels, ...rest } = item.sport_event;
                // console.log(rest)
                const providedDate = new Date(rest.start_time);

                // Get the current date
                const currentDate = new Date();
                const competitor = rest.competitors.some((competitorSome: any) => competitorSome.id === competitorId)
                if (providedDate > currentDate && competitor) {
                    return rest;
                }
            }).filter((item: any) => item !== undefined);
            const responseString = JSON.stringify(response)
            return `recuerda que estos datos deben estar asociados a la pregunta del usuario y verifica que si estas tomando este dato para comparar es necesario que consultes al otro equipo tambien, ahora este es el dato de este equipo:  ${responseString}`
        }else{
            return 'posiblemente el competitorId no es el correcto debes consultar standingFullTimeTotalCount o en standingFullTimeTotal, para poder obterner las id de competitor, recuerda que el id del competidor debe ser el que el usuario require no el que tu elijas al asar o inventes un id'
        }
        return "debes obtener el competitor id, posiblemente lo puedes obtener en la lista de stadings"
    }

    async getOverUnder(seasonId: string) {
        const overUnder = await SoccerService.overUnder(seasonId, 0, true)

        return overUnder
    }

    async getOverUnderByCompetitorId(seasonId: string, competitorId: string, competitorName: string){
        let competitors = await SoccerService.seasonCompetitors(seasonId);
        competitors = (competitors as any).fullData.season_competitors.some((competitor: any) => {
            if(competitor.id === competitorId && competitor.name.includes(competitorName)){
                console.log(competitor)
                return true;
            }
        })
        if (competitorId && competitors) {
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const overUnder = await SoccerService.overUnder(seasonId, 0, true)
            const overUnderCompetitor = (overUnder as any).fullData.competitors.find((competitor: any)=>{
                if(competitor.id === competitorId){
                    return competitor
                }
            })

            console.log(overUnderCompetitor)
            return overUnderCompetitor


        }
    }

    howAnswerProbabilities(){
        const howToAnwser = `
            las probabilidades de que el equipo """ A """ ganen contra el equipo
            """B""" son de """numero_de_porcentaje%+""", te recomiendo que apuestes al equipo A
        `
        return howToAnwser;
    }
    
}

export default new AiService();