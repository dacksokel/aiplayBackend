import CompetitionsModel from '../../../models/competitions';
import CompetitorModel from '../../../models/competitors';
import CompetitionSeasonsModel from '../../../models/competitionsSeasons';
import SeasonCompetitorModel from '../../../models/seasonCompetiror';
import SeasonStandingModel from '../../../models/standings';
import SeasonProbabilitiesModel from '../../../models/probabilities';
import SeasonLeaderModel from '../../../models/seasonLeaders';
import SeasonOverUnderModel from '../../../models/overUnder';

import config from '../../../../config/config';
import ILimitAndPage from '../../../interfaces/ILimitAndPage';
import competitionsSeasons from '../../../models/competitionsSeasons';
import TypeSport from "../../../enums/TypeSport";
import IStandingSoccer from '../../../interfaces/IStandingsSoccer';


class SoccerService {
    urlSource: any;
    constructor() {
        this.urlSource = config.data.api.sportRadar.soccer;
    }
    //step 1 get competitions
    async fetchCompetitions() {
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitionsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchCompetitionsData = await fetch(url);
            const competitionsData = await fetchCompetitionsData.json();
            const competitinosData2Save = {
                data: JSON.stringify(competitionsData),
                fullData: competitionsData,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            }
            const competitions = await CompetitionsModel.create(competitinosData2Save);
            await competitions.save();

            return competitionsData.competitions;
        } catch (error) {
            console.error('Error fetching competitions:', error);
            // throw error;
        }
    }
    async fetchUpdateCompetitions(competitionsId: string) {
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitionsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchCompetitionsData = await fetch(url);
            const competitionsData = await fetchCompetitionsData.json();
            const competitinosData2Save = {
                data: JSON.stringify(competitionsData),
                fullData: competitionsData,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            }
            const response = await CompetitionsModel.findOneAndUpdate({ _id: competitionsId }, { $set: competitinosData2Save });

            console.log('CompetitionsUpdate')
            return response;
        } catch (error) {
            console.error('Error fetching competitions:', error);
            // throw error;
        }
    }
    async getCompetitions() {
        try {
            // const skip = (query.page - 1)* query.limit;
            const competitions = await CompetitionsModel.findOne({ competitionType: 'soccer' }).sort({ _id: -1 }).lean();
            if (competitions) {
                const dateCompetitions = competitions.fullData.generated_at;

                // if(this.validatedate(dateCompetitions)){
                return competitions.fullData.competitions;
                // }else{                  
                //     //here its update no fetch
                //     return await this.fetchUpdateCompetitions(competitions._id.toString())
                //     // return await this.fetchCompetitions()
                // }
            } else {
                return await this.fetchCompetitions()
            }
        } catch (error) {
            console.error('Error getting competitions:', error);
            // throw error;
        }
    }


    validatedate(date: string) {
        const date2Compare = new Date(date)
        const today = new Date();

        // Extract the year, month, and day from both dates
        const yearCompetition = date2Compare.getFullYear();
        const monthCompetition = date2Compare.getMonth();
        const dayCompetition = date2Compare.getDate();

        const yearToday = today.getFullYear();
        const monthToday = today.getMonth();
        const dayToday = today.getDate();

        if (yearCompetition === yearToday && monthCompetition === monthToday && dayCompetition === dayToday) {
            // console.log('This data is up to date for today.');
            return true;
        } else {
            // console.log('This data is not up to date for today.');
            return false;
        }
    }

    //step 2 get all competitons by sesion 
    async fetchCompetitionSeason(competitionId: string) {
        console.log('start step 2 fetchCompetitionSeason ', competitionId)
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitions}/${competitionId}/${this.urlSource.seasonsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchCompetitionSeasons = await fetch(url);
            const competitionSeasonsData = await fetchCompetitionSeasons.json();
            if (Object.prototype.hasOwnProperty.call(competitionSeasonsData, 'seasons')) {
                const data2save = {
                    data: JSON.stringify(competitionSeasonsData),
                    fullData: competitionSeasonsData,
                    competitionId: competitionId,
                    competitionType: TypeSport.Soccer,
                    sourceData: 'sportRadar'
                }
                const competitionSeasonData = new CompetitionSeasonsModel(data2save)
                competitionSeasonData.save();
                console.log('fetchCompetitionSeason was save  in to db')
                const seasonId = competitionSeasonsData.seasons[competitionSeasonsData.seasons.length - 1].id
                return seasonId;
            }
        } catch (error) {
            console.error('Error fetching competition seasons:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);

            return this.promise2resolverfetchCompetitionSeason(time, competitionId)
            // throw error;
        }
    }
    async getSesonIds() {
        const competitions = await CompetitionSeasonsModel.find();
        const sesasonIds = competitions.map(competition => {

        })
        console.log(competitions)
    }
    getCompetitionsDefault() {
        return config.data.api.defatulCompetitionsSportRadar.soccerCompetitions;
    }
    promise2resolverfetchCompetitionSeason(t: number, competitionId: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchCompetitionSeason(competitionId))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 2000 * t)
        })
    }
    async getCompetitionSeasonDefault(competitionId: string, index: number) {
        console.log('start find competition ', competitionId)
        console.log('start find competition index: ', index)
        let season;
        const competitionsSeason = await CompetitionSeasonsModel.findOne({ competitionId: competitionId });
        if (competitionsSeason) {
            season = competitionsSeason.fullData.seasons;
            let season_id = season[season.length - 1].id

            return season_id;
        }
        return this.promise2resolverfetchCompetitionSeason(index, competitionId);

    }


    async seasonCompetitors(id: string, index: number = 0, update: boolean = false) {
        const findSeasonCompetitor = await SeasonCompetitorModel.findOne({ seasonId: id, competitionType: TypeSport.Soccer }).sort({ _id: -1 }).lean();
        if (findSeasonCompetitor) {
            if (!this.validatedate(findSeasonCompetitor.fullData.generated_at) && update) {
                console.log('update seasonCompetitors ', id, index)
                return this.promise2resolverfetchUpdateSeasonCompetitors(index, id)
            }
            console.log('not need update')
            return findSeasonCompetitor;
        }
        return this.promise2resolverfetchSeasonCompetitors(index, id)
    }
    promise2resolverfetchSeasonCompetitors(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonCompetitors(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateSeasonCompetitors(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateCompetitors(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }
    //now neeed catch the teams or competitiors! this its the step 3
    // Season Competitors soccer/trial/v4/:language_code/seasons/:season_id/competitors:format
    async fetchSeasonCompetitors(season_id: string) {
        console.log('start fetchSeasonCompetitors ', season_id)
        if(season_id === undefined || season_id === null){
            return 'Season ID not Found'
        }
        try {


            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.competitorsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonCompetitors = await fetchSeason.json();
            const data2saveSeasonCompetitors = {
                data: JSON.stringify(seasonCompetitors),
                fullData: seasonCompetitors,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            //save in db
            const seasonCompetirorSave = new SeasonCompetitorModel(data2saveSeasonCompetitors);
            seasonCompetirorSave.save();
            console.log('fetchSeasonCompetitors save in db first time')

            return seasonCompetitors;

        } catch (error) {
            console.error('Error fetching season competitors:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);

            return this.promise2resolverfetchSeasonCompetitors(time, season_id)

            // throw error;
        }
    }
    async fetchSeasonUpdateCompetitors(season_id: string) {
        console.log('start fetchSeasonCompetitors ', season_id)
        try {


            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.competitorsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonCompetitors = await fetchSeason.json();
            const data2saveSeasonCompetitors = {
                data: JSON.stringify(seasonCompetitors),
                fullData: seasonCompetitors,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            const response = await SeasonCompetitorModel.findOneAndUpdate({ seasonId: season_id }, { $set: data2saveSeasonCompetitors });
            console.log('fetchSeasonUpdateCompetitors ')

            return response;

        } catch (error) {
            console.error('Error fetching season competitors:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchUpdateSeasonCompetitors(time, season_id)

            // throw error;
        }
    }


    //step 
    //please change name
    async fetchCompetitorsByTeam(teamId: string) {
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitions}/${teamId}/${this.urlSource.summariesJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchCompetitors = await fetch(url);
            const competitorsData = await fetchCompetitors.json();
            // const data2Save = {
            //     data: JSON.stringify(competitorsData),
            //     fullData: competitorsData,
            //     seasonId: season_id
            // };
            const competitors = new CompetitorModel({ data: '', fullData: competitorsData });
            await competitors.save();

            return competitorsData;
        } catch (error) {
            console.error('Error fetching competitors by team:', error);
            // throw error;
        }
    }

    promise2resolverfetchFromStandings(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonFormStandings(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateFromStandings(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateFormStandings(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }

    async formStandings(season_id: string, index: number = 0, update: boolean = false) {
        const standing = await SeasonStandingModel.findOne({ seasonId: season_id, competitionType: TypeSport.Soccer }).sort({ _id: -1 }).lean();
        if (standing) {

            if (!this.validatedate(standing.fullData.generated_at) && update) {
                console.log('update standings')
                return this.promise2resolverfetchUpdateFromStandings(index, season_id)
            }
            return standing
        }
        return this.promise2resolverfetchFromStandings(index, season_id)
    }

    //step 4 season standing
    async fetchSeasonFormStandings(season_id: string) {
        console.log('start fetchSeasonFormStandings with the id: ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.formStandingsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonFormStadings = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonFormStadings),
                fullData: seasonFormStadings,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonFormStadings = new SeasonStandingModel(data2Save);
            saveSeasonFormStadings.save();

            return data2Save;
        } catch (error) {
            console.error('Error fetching season form standings:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);

            return this.promise2resolverfetchFromStandings(time, season_id)

            // throw error;
        }
    }
    async fetchSeasonUpdateFormStandings(season_id: string) {
        console.log('start fetchSeasonUpdateFormStandings with the id: ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.formStandingsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonFormStadings = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonFormStadings),
                fullData: seasonFormStadings,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SeasonStandingModel.findOneAndUpdate({ seasonId: season_id }, data2Save);
            // saveSeasonFormStadings.save();

            return response;
        } catch (error) {
            console.error('Error fetching season form standings:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchUpdateFromStandings(time, season_id)

            // throw error;
        }
    }

    //step 5 season probabilitie

    promise2resolverfetchProbabilities(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonProbabilities(season_id))
            }, 2500 * t)
        })
    }
    promise2resolverfetchUpdateProbabilities(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateProbabilities(season_id))
            }, 2500 * t)
        })
    }
    async probabilities(season_id: string, index: number, update: boolean = true) {
        const probabilities = await SeasonProbabilitiesModel.findOne({ seasonId: season_id }).sort({ _id: -1 }).lean();
        if (probabilities) {
            if (!this.validatedate(probabilities.fullData.generated_at) && update) {
                console.log('update probabilities')
                return this.promise2resolverfetchUpdateProbabilities(index, season_id)
            }
            return probabilities
        }
        console.log('New Probabilities')
        return this.promise2resolverfetchProbabilities(index, season_id)
    }

    async fetchSeasonProbabilities(season_id: string) {
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return data2Save;
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchProbabilities(time, season_id)
            // throw error;
        }
    }
    async fetchSeasonUpdateProbabilities(season_id: string) {
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SeasonProbabilitiesModel.findOneAndUpdate({ seasonId: season_id }, data2Save);
            // const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            // saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return response;
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchUpdateProbabilities(time, season_id)
            // throw error;
        }
    }
    //step 6 season players leaders 
    promise2resolverfetchLeaders(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonleaders(season_id))
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateLeaders(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateleaders(season_id))
            }, 5000 * t)
        })
    }

    async leaders(season_id: string, index: number = 0, update: boolean = true) {
        const leaders = await SeasonLeaderModel.findOne({ seasonId: season_id, competitionType: TypeSport.Soccer}).sort({ _id: -1 }).lean();
        if (leaders) {
            if (!this.validatedate(leaders.fullData.generated_at) && update) {
                console.log('update season leaders')
                return this.promise2resolverfetchUpdateLeaders(index, season_id)
            }
            
            return leaders

        }
        console.log('New seasonLeaders')
        return this.promise2resolverfetchLeaders(index, season_id)
    }
    async fetchSeasonleaders(season_id: string) {
        console.log('start to fetch leaders ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.leadersJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonLeader = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonLeader),
                fullData: seasonLeader,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            //save in db
            const saveSeasonLeader = new SeasonLeaderModel(data2Save);
            saveSeasonLeader.save();

            return seasonLeader;
        } catch (error) {
            console.error('Error fetching season leaders:', error);
            // throw error;
        }
    }
    async fetchSeasonUpdateleaders(season_id: string) {
        console.log('start to fetch leaders ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.leadersJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonLeader = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonLeader),
                fullData: seasonLeader,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            //save in db
            const response = await SeasonLeaderModel.findOneAndUpdate({ seasonId: season_id }, data2Save);
            // const saveSeasonLeader = new SeasonLeaderModel(data2Save);
            // saveSeasonLeader.save();
            console.log('update sseason leader')
            return response;
        } catch (error) {
            console.error('Error fetching season leaders:', error);
            // throw error;
        }
    }

    //step 7 over under\

    promise2resolverfetchOverUnder(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonOverUnder(season_id))
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateOverUnder(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateOverUnder(season_id))
            }, 5000 * t)
        })
    }

    async overUnder(season_id: string, index: number, update: boolean = false) {
        const overUnders = await SeasonOverUnderModel.findOne({ seasonId: season_id }).sort({ _id: -1 }).lean();
        if (overUnders) {
            if (!this.validatedate(overUnders.fullData.generated_at) && update) {
                console.log('update season overunder:  ', season_id)
                return this.promise2resolverfetchUpdateOverUnder(index, season_id)
            }
            return overUnders
        }
        console.log('New over  under ', season_id)
        return this.promise2resolverfetchOverUnder(index, season_id)
    }
    async fetchSeasonOverUnder(season_id: string) {
        try {
            console.log('start over under ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.overUnderStatisticsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonOverUnder = await fetchSeason.json();

            const data2Save = {
                data: JSON.stringify(seasonOverUnder),
                fullData: seasonOverUnder,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            //save in db
            const saveSeasonOverUnder = new SeasonOverUnderModel(data2Save);
            saveSeasonOverUnder.save();
            console.log('save into db overunder: ', season_id)
            return seasonOverUnder;
        } catch (error) {
            console.log(error)
            // throw error
        }
    }

    async fetchSeasonUpdateOverUnder(season_id: string) {
        try {
            console.log('start over under ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.overUnderStatisticsJson}${this.urlSource.urlApiKey}${config.sportRadar.soccer}`;
            const fetchSeason = await fetch(url);
            const seasonOverUnder = await fetchSeason.json();

            const data2Save = {
                data: JSON.stringify(seasonOverUnder),
                fullData: seasonOverUnder,
                seasonId: season_id,
                competitionType: TypeSport.Soccer,
                source: 'sportRadar'
            };
            //save in db
            const response = await SeasonOverUnderModel.findOneAndUpdate({ seasonId: season_id }, data2Save);
            // const saveSeasonOverUnder = new SeasonOverUnderModel(data2Save);
            // saveSeasonOverUnder.save();
            console.log('Update into db overunder: ', season_id)
            return response;
        } catch (error) {
            console.log(error)
            // throw error
        }
    }


    //step 8 

    //one for test
    test() {
        console.log('this its a test from soccer sport radar service')
    }

    async getCompetitionById(id: string) {
        console.log(id)
        const competition = await CompetitionsModel.findOne({competitionType: TypeSport.Soccer}).sort({ _id: -1 }).lean()
        if (competition) {
            return this.externalizeCompetitionById(competition, id);
        }

        return { msg: 'hello' }
    }
    async getCompetitionAndSeason(competitionId: string) {
        const competitionSeason = await CompetitionSeasonsModel.findOne({ competitionId: competitionId }).sort({ _id: -1 }).lean();
        if (competitionSeason) {
            console.log(competitionSeason)
            const season_id = competitionSeason.fullData.seasons[competitionSeason.fullData.seasons.length - 1].id
            const competitorsSeason: any = await this.seasonCompetitors(season_id, 0);

            return this.externalizeCompetitorSeason(competitorsSeason, season_id)
        }
        //if not exits so we need to add into the database
        const season_id = await this.fetchCompetitionSeason(competitionId);
        // console.log(season_id)

        //now find competitors with this  season data
        const competitorsSeason = await this.fetchSeasonCompetitors(season_id)
        console.log(competitorsSeason)
        return this.externalizeCompetitorSeason(competitorsSeason, season_id)
    }

    async getAllSeasonAvalible() {
        // const skip = (query.page - 1) * query.limit;
        const competitionsSeasons: any = await CompetitionSeasonsModel.find({ "competitionType": TypeSport.Soccer })
        // .skip(skip)
        // .limit(query.limit)

        const seasonsSaves: Array<string> = [];

        const seasons = competitionsSeasons
            .map((competiton: any, index: number) => {
                const season = competiton.fullData.seasons[competiton.fullData.seasons.length - 1]
                // console.log(index)
                if (!seasonsSaves.includes(season.id)) {
                    seasonsSaves.push(season.id)
                    return season
                }
            })
            .filter((data: any) => data !== undefined)
        return {
            seasons
        }
    }

    async getSeasonsById(id: string) {
        const seasons = await this.getAllSeasonAvalible();
        const season = seasons.seasons.filter((season: any) => season.id === id)
        const competitionId = season.competition_id;
        let competitor: any = await this.seasonCompetitors(id, 0, false)
        competitor = competitor.fullData;
        return {
            season: season[0],
            competitors: competitor.season_competitors

        }

    }

    async getAllLeaders(seasonId: string = '') {
    }
    async getLeadersById(seasonId: string) {
        const leaderSeason = await this.leaders(seasonId);

        if (leaderSeason) {
            return this.externalizeLeadesById(leaderSeason, seasonId);
        }
    }

   async getStandingsBySeasonId(seasonId: string) {
        try {
            let standings = await this.formStandings(seasonId)

            const generated_at = (standings as any).fullData.generated_at ? (standings as any).generated_at : (standings as any).fullData.generated_at ;
            const auxStandings = (standings as { fullData: { season_form_standings: any } }).fullData.season_form_standings || (standings as any).season_form_standings;

            // if (validateGenerated_at || Object.prototype.hasOwnProperty.call(standings, 'fullData')) {
                return {
                    seasonId,
                    lastUpdate: generated_at,
                    standings: auxStandings
                };
            // }
        } catch (error) {
            console.error('Error fetching standings:', error);
            // throw error;
        }
    }
 
    async externalizeLeadesById(leaders: any, season_id: string) {
        const seasons = await this.getAllSeasonAvalible();
        const season = seasons.seasons.filter((season: any) => season.id === season_id)
        const points = leaders.fullData.lists.filter((data: any) => data.type === 'points')
        const goals = leaders.fullData.lists.filter((data: any) => data.type === 'goals')
        const assists = leaders.fullData.lists.filter((data: any) => data.type === 'assists')
        const red_cards = leaders.fullData.lists.filter((data: any) => data.type === 'red_cards')
        const yellow_red_cards = leaders.fullData.lists.filter((data: any) => data.type === 'yellow_red_cards')
        const yellow_cards = leaders.fullData.lists.filter((data: any) => data.type === 'yellow_cards')
        const own_goals = leaders.fullData.lists.filter((data: any) => data.type === 'own_goals')
        return {
            season,
            lastUpdate: leaders.fullData.generated_at,
            points: points[0].leaders,
            goals: goals[0].leaders,
            assists: assists[0].leaders,
            redCards: red_cards[0].leaders,
            yellowRedCards: yellow_red_cards[0].leaders,
            yellowCards: yellow_cards[0].leaders,
            ownGoals: own_goals[0].leaders

        }
    }

    async externalizeCompetitionById(competitions: any, id: string) {
        const dataCompetition = competitions.fullData.competitions;
        const dataCompetitionRetrun = dataCompetition.filter((competition: { id: string }) => competition.id === id);
        const dataCompetitionsCompetitorsSeason = await this.getCompetitionAndSeason(id);

        return {
            ...dataCompetitionRetrun[0],
            ...dataCompetitionsCompetitorsSeason
        };
    }
    externalizeCompetitorSeason(comeptitors: any, seasonId: string) {
        return {
            season: {
                id: seasonId
            },
            competitors: comeptitors.season_competitors
        }
    }


}

export default new SoccerService();