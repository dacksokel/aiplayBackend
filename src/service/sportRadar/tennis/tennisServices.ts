import config from "../../../../config/config";
import CompetitionsModel from "../../../models/competitions";
import CompetitionSeasonsModel from "../../../models/competitionsSeasons";
import SeasonProbabilitiesModel from '../../../models/probabilities';
import SummariesModel from '../../../models/summaries';

import TypeSport from "../../../enums/TypeSport";
import { rejects } from "assert";

class TennisService {
    urlSource: any;
    constructor() {
        this.urlSource = config.data.api.sportRadar.tennis;
    }

    validatedate(date: string){
        const date2Compare = new Date(date) 
        const today = new Date();

        // Extract the year, month, and day from both dates
        const yearCompetition = date2Compare.getFullYear();
        const monthCompetition = date2Compare.getMonth();
        const dayCompetition = date2Compare.getDate();

        const yearToday = today.getFullYear();
        const monthToday = today.getMonth();
        const dayToday = today.getDate();

        if (yearCompetition === yearToday && monthCompetition === monthToday && dayCompetition === dayToday) {
            // console.log('This data is up to date for today.');
            return true;
        } else {
            // console.log('This data is not up to date for today.');
            return false;
        }
    }

    async fetchCompetitions() {
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitionsJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchCompetitionsData = await fetch(url);
            const competitionsData = await fetchCompetitionsData.json();
            const competitinosData2Save = {
                data: JSON.stringify(competitionsData),
                fullData: competitionsData,
                competitionType: TypeSport.Tennis,
                source: "sportRadar",
            };
            const competitions = await CompetitionsModel.create(
                competitinosData2Save
            );
            await competitions.save();
            console.log("Competitions Tennis Created and Save");
            return competitionsData.competitions;
        } catch (error) {
            console.log("Error Fetch Competitions Tennis");
            console.log(error);
            return error;
        }
    }
    async fetchUpdateCompetitions() {
        try {
            console.log("Competitions Tennis Update");
        } catch (error) {
            console.log("ERrorFetch update compettions Tennis");
            console.log(error);
            return error;
        }
    }

    async getCompetitions() {
        try {
            // const skip = (query.page - 1)* query.limit;
            const competitions = await CompetitionsModel.findOne({
                competitionType: TypeSport.Tennis,
            })
                .sort({ _id: -1 })
                .lean();
            if (competitions) {
                const dateCompetitions = competitions.fullData.generated_at;

                // if(this.validatedate(dateCompetitions)){
                return competitions.fullData.competitions;
                // }else{
                //     //here its update no fetch
                //     return await this.fetchUpdateCompetitions(competitions._id.toString())
                //     // return await this.fetchCompetitions()
                // }
            } else {
                return await this.fetchCompetitions();
            }
        } catch (error) {
            console.error("Error getting competitions:", error);
            // throw error;
        }
    }

   
    async getCompetitionById(id: string) {
        console.log(id);
        const competition = await CompetitionsModel.findOne({competitionType: TypeSport.Tennis})
            .sort({ _id: -1 })
            .lean();
        if (competition) {
            return this.externalizeCompetitionById(competition, id);
        }

        return { msg: "hello" };
    }

    async externalizeCompetitionById(competitions: any, id: string) {
        const dataCompetition = competitions.fullData.competitions;
        const dataCompetitionRetrun = dataCompetition.filter(
            (competition: { id: string }) => competition.id === id
        );
        // const dataCompetitionsCompetitorsSeason = await this.getCompetitionAndSeason(id);

        return {
            ...dataCompetitionRetrun[0],
            // ...dataCompetitionsCompetitorsSeason
        };
    }

    //step 2 get all competitons by sesion
    async fetchCompetitionSeason(competitionId: string) {
        console.log("start step 2 fetchCompetitionSeason ", competitionId);
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitions}/${competitionId}/${this.urlSource.seasonsJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchCompetitionSeasons = await fetch(url);
            const competitionSeasonsData = await fetchCompetitionSeasons.json();
            if (
                Object.prototype.hasOwnProperty.call(competitionSeasonsData, "seasons")
            ) {
                const data2save = {
                    data: JSON.stringify(competitionSeasonsData),
                    fullData: competitionSeasonsData,
                    competitionId: competitionId,
                    competitionType: TypeSport.Tennis,
                    sourceData: 'sportRadar'
                };
                const competitionSeasonData = new CompetitionSeasonsModel(data2save);
                competitionSeasonData.save();
                console.log("fetchCompetitionSeason was save  in to db");
                const seasonId =
                    competitionSeasonsData.seasons[
                        competitionSeasonsData.seasons.length - 1
                    ].id;
                return seasonId;
            }
        } catch (error) {
            console.error("Error fetching competition seasons:", error);
             const time = Math.floor(Math.random() * (5000 - 1050 + 1)) + 1050;

             return this.promise2resolverfetchCompetitionSeason(time, competitionId)
            // throw error;
        }
    }
    async getSesonIds() {
        const competitions = await CompetitionSeasonsModel.find({competitionType: TypeSport.Tennis});
        const sesasonIds = competitions.map((competition) => { });
        console.log(competitions);
    }
    getCompetitionsDefault() {
        return config.data.api.defatulCompetitionsSportRadar.tennis;
    }
    promise2resolverfetchCompetitionSeason(t: number, competitionId: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchCompetitionSeason(competitionId));
                // season = this.fetchCompetitionSeason(competitionId);
            }, t);
        });
    }
    async getCompetitionSeasonDefault(competitionId: string, index: number) {
        console.log("start find competition ", competitionId);
        console.log("start find competition index: ", index);
        let season;
        const competitionsSeason = await CompetitionSeasonsModel.findOne({
            competitionId: competitionId,
            competitionType: TypeSport.Tennis
        });
        if (competitionsSeason) {
            season = competitionsSeason.fullData.seasons;
            let season_id = season[season.length - 1].id;

            return season_id;
        }
        const random = Math.floor(Math.random() * (99999 - 15040 + 1)) + 15040;
        const ramdom2 = Math.floor(Math.random() * (1123 - 499 + 1)) + 499 * random;
        const randomNumber = Math.floor(Math.random() * (500 - 150 + 1)) + 150 *index;
        const time = Math.floor(ramdom2 / randomNumber);
        console.log(time + " y " + index )
        return this.promise2resolverfetchCompetitionSeason(time, competitionId);
    }


     //step 5 season probabilitie

     promise2resolverfetchProbabilities(t:number, season_id:string ){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
              resolve(this.fetchSeasonProbabilities(season_id))              
            }, 2500 * t)     
        })
    }
 promise2resolverfetchUpdateProbabilities(t:number, season_id:string ){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
              resolve(this.fetchSeasonUpdateProbabilities(season_id))              
            }, 2500 * t)     
        })
    }
    async probabilities(season_id: string, index: number, update: boolean = true){
        const probabilities = await SeasonProbabilitiesModel.findOne({seasonId: season_id, competitionType: TypeSport.Tennis}).sort({_id: -1}).lean();
        if(probabilities){
            if(!this.validatedate(probabilities.fullData.generated_at) && update){
                console.log('update probabilities')
                return this.promise2resolverfetchUpdateProbabilities(index, season_id)
            }        
            return probabilities
        }
        console.log('New Probabilities')
        return this.promise2resolverfetchProbabilities(index, season_id)
    }
    
    async fetchSeasonProbabilities(season_id: string){
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.Tennis,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return data2Save;  
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promise2resolverfetchProbabilities(time, season_id)
            // throw error;
        }
    }
    async fetchSeasonUpdateProbabilities(season_id: string){
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.Tennis,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SeasonProbabilitiesModel.findOneAndUpdate({seasonId:season_id, competitionType: TypeSport.Tennis}, data2Save);
            // const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            // saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return response;  
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promise2resolverfetchUpdateProbabilities(time, season_id)
            // throw error;
        }
    }

    /////////////////////////////
    async getAllSeasonAvalible(){
        // const skip = (query.page - 1) * query.limit;
        const competitionsSeasons:any = await CompetitionSeasonsModel.find({"competitionType":TypeSport.Tennis})
        // .skip(skip)
        // .limit(query.limit)

        const seasonsSaves: Array<string> = [];

        const seasons = competitionsSeasons
        .map( (competiton: any, index: number)=> {
            const season = competiton.fullData.seasons[competiton.fullData.seasons.length -1]
            // console.log(index)
            if(!seasonsSaves.includes(season.id)){
                seasonsSaves.push(season.id)
                return season
            }
        } )
        .filter((data:any) => data !== undefined)
        return {
            seasons        
        }
    }

    async getSeasonsById(id:string){
        const seasons = await this.getAllSeasonAvalible();
        const season = seasons.seasons.filter((season:any) => season.competition_id === id)
        const competitionId = season.competition_id;
        // let competitor:any = await this.seasonCompetitors(id, 0, false)
        // competitor = competitor.fullData;
        return {
            season: season[0],
            // competitors: competitor.season_competitors

        }

    }


    //sumamries

    async getSummaries(season_id: string, index: number = 0, update: boolean = true){
        const sumamries = await SummariesModel.findOne({seasonId: season_id, competitionType: TypeSport.Tennis}).sort({_id: -1}).lean();
        if(sumamries){
            if(!this.validatedate(sumamries.fullData.generated_at) && update){
                console.log('update Summaries')
                return this.promiseFetchSummariesUpdate(index, season_id)
            }        
            return sumamries
        }
        console.log('New Summaries')
        return this.promiseFetchSummaries(index, season_id)
    }
    async fetchSummaries(season_id: string){
        try {
            console.log('start to fetch Summaries: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.summariesJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchSeason = await fetch(url);
            const summaries = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(summaries),
                fullData: summaries,
                seasonId: season_id,
                competitionType: TypeSport.Tennis,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonProbabilities = new SummariesModel(data2Save);
            saveSeasonProbabilities.save();
            console.log('Summaries was  save: ', season_id)
            return data2Save;  
        } catch (error) {
            console.error('Error fetching season Summaries:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promiseFetchSummaries(time, season_id)
            // throw error;
        }
    }
    async fetchSummariesUpdate(season_id: string){
        try {
            console.log('start to Update fetch Summaries: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.summariesJson}${this.urlSource.urlApiKey}${config.sportRadar.tennis}`;
            const fetchSeason = await fetch(url);
            const summaries = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(summaries),
                fullData: summaries,
                seasonId: season_id,
                competitionType: TypeSport.Tennis,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SummariesModel.findOneAndUpdate({seasonId:season_id, competitionType: TypeSport.Tennis}, data2Save);
            // const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            // saveSeasonProbabilities.save();
            console.log('Summaries was  save: ', season_id)
            return response;  
        } catch (error) {
            console.error('Error fetching season Summaries:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promiseFetchSummariesUpdate(time, season_id)
            // throw error;
        }
    }
        promiseFetchSummaries(t:number, season_id:string ){
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                  resolve(this.fetchSummaries(season_id))              
                }, 2500 * t)     
            })
        }
        
        promiseFetchSummariesUpdate(t:number, season_id:string ){
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                  resolve(this.fetchSummariesUpdate(season_id))              
                }, 2500 * t)     
            })
        }
}

export default new TennisService();
