/***
 * pasos:
 * primero se debe preguntar por el competition id, luego de obtenerla entonces se debe enviar dicho competition id, junto con la pregunta a la prediccion3
 * en la prediccion 3 se guardara en un objeto de usuarios {'usuarioId':{competitionId,seasonID}} se debe buscar el season id y agregarlo en este objeto, 
 * de esta forma comenzaremos con la prediccion 3 preguntando soobre la tabla de pociciones de la competicion en cuestion, pasandole siempre la seasonId
 * y dejandola especificada en el log de la function para que la ia pueda saber lo que debe de hacer con dichos valores :D, las respuestas y preguntas realizadas por el usuario se deben agregar
 * en el objeto de ser posibles
 * 
 */
import config from '../../../../config/config';
import openAiService from '../../openAiService';
import TennisService from './tennisServices';
import TypeSport from '../../../enums/TypeSport';


class TennisAiService {

    async questionsAi(question: string, user: any, tennisData: any) {
        const messages = [
            {
                role: 'system',
                content:
                    config.data.api.systemPromptDefault2,
            },
            {
                role: 'user',
                // content:
                //     `ruta de busqueda de datos: 
                //     consulta el summaries y las probabilidades, que es la informacion que se tiene de momento de este deporte, asi como define correctamente seasonId como ${tennisData.season.id} si las probabilidades no funcionan no te rindas, busca el getSummariesCount y luego el getsummarie
                // ${question}`
                content: `${question}, ${JSON.stringify(tennisData)}`
            },
        ];

        //now functions
        const functions = [
            {
                name: 'getSummariesIa',
                description: `por medio de esta funcion puede recibir un resumen completo de lo que esta pasando en la seasoon ${tennisData.season.id}`,
                parameters:{
                    type: 'object',
                    properties:{
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${tennisData.season.id}`,
                            emun: [tennisData.season.id]
                        }
                    }
                },
                require: ['seasonId']
            },
            {
                name: 'getSummariesCount',
                description: `es una funcion que entrega el numero total de summaries que tiene  en la base de dato para que luego sea consultada la otra funcion que se llama getSummaries`,
                parameters:{
                    type: 'object',
                    properties:{
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${tennisData.season.id}`,
                            emun: [tennisData.season.id]
                        }
                    }
                },
                require: ['seasonId']
            },
            {
                name: 'probabilitiesCount',
                description: `
                    recuerda consultar standingFullTimeTotal, para poder saber el estatus de los equipos y sus posiciones
                        entrega el numero de probabilidades que se pueden consultar en la season ${tennisData.season.id}, para que el getProbabilitiesOneToOnelos consulte uno a uno 
                    `,
                parameters: {
                    type: 'object',
                    properties: {
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${tennisData.season.id}`,
                            emun: [tennisData.season.id]
                        },
                        contadorSummaries: {
                            type: 'number',
                            description: `es un numero obtenido desde getSummariesCount, el cual entrega un numero entero, para hacer peticiones a esta funcion`
                        },
                        totalSummaries: {
                            type: 'number',
                            description: `es el total obtenido al preguntar a la funcion getSummariesCount, si no es igual entonces se emitira un mensaje para que consulte dicha funcion, es una manera de validar que si se consulta el total`
                        }
                    }
                },
                require: ['seasonId', 'contadorSummaries', 'totalSummaries'],
            },
            {
                name: 'getProbabilitiesOneToOne',
                description: `
                    Siempre que se vaya a usar esta funcion, se debe obtener la cantidad de veces que se consultara con la funcion probabilitiesCount, nunca asumir que ya tienes el numero de conteo de otra funcion que no sea la antes mensionada, entregara dato a dato segun el numero que le mandes, el cual dependera del total que previamente se le entregue, si al consultar anteriolmente le dieron 20 el debera consultar para ir aprendiendo sobre la informacion en base al numero total y terminara cuando el total sea igual al numero de veces que pregunto`,
                parameters: {
                    type: 'object',
                    properties: {

                        contadorProbabilities: {
                            type: 'number',
                            description: 'es el numero total que le dara al getProbabilitiesOneToOne, de esta forma ir recibiendo data uno a uno hasta que termine el conteo'
                        },
                        seasonId: {
                            type: 'string',
                            descriptcion: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${tennisData.season.id}`,
                            emun: [tennisData.season.id]
                        },
                        total: {
                            type: 'number',
                            description: `es el total de probabilidades obtenidos desde probabilitiesCount y es necesario para comparar todas las probabilidades posibles de la season que tiene el id ${tennisData.season.id}`
                        }

                    }
                },
                require: ['contadorProbabilities', 'seasonId', 'total'],
            }
        ];

        return await openAiService.conecctionWitFunctionsCallTennis(functions, messages);

    }

    async callFunction(function_call: any) {
        const args = JSON.parse(function_call.arguments);
        switch (function_call.name) {
            case 'probabilitiesCount':
                return await this.probabilitiesCount(args['seasonId']);
            case 'getProbabilitiesOneToOne':
                return await this.getProbabilitiesOneToOne(args['contadorProbabilities'], args['total'], args['seasonId']);
            case 'getSummariesIa':
                return await this.getSummariesIa(args['seasonId'], args['contadorSummaries'], args['totalSummaries'])
            case 'getSummariesCount':
                return await this.getSummariesCount(args['seasonId'])

            default:
                throw new Error('No function found');
        }
    }

 

//probabilities data
    async getProbabilities(seasonId: string, team: string) {
        const probabilities = await TennisService.probabilities(seasonId, 0, true)

        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
                return rest;
            }
        }).filter((item: any) => item !== undefined);
        // console.log(response)
        console.log(team)
        return JSON.stringify(response);
    }
    async probabilitiesCount(seasonId: string) {
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        if (seasonId) {
            const probabilities = await TennisService.probabilities(seasonId, 0, false)
            const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
                const { channels, ...rest } = item.sport_event;
                // console.log(rest)
                const providedDate = new Date(rest.start_time);

                // Get the current date
                const currentDate = new Date();

                if (providedDate > currentDate) {
                }
                return rest;
            }).filter((item: any) => item !== undefined);

            return response.length
        }
        return 'si desea obtener las probabilidades de estadisticas de los equipos debes volver a llamar a esta funcion para obtener un numero'
    }

    async getProbabilitiesOneToOne(contador: number, totalProbabilities: number, seasonId: string) {
        console.log(`total probabilities ${totalProbabilities}`)
        const probabilities = await TennisService.probabilities(seasonId, 0, false)
        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
            }
            return rest;
        }).filter((item: any) => item !== undefined);
        if (response.length !== totalProbabilities) {
            return 'debes consultar probabilitiesCount para obtener el numero de veces que se debe consultar las probabilidades, y debes consultar toda las probabilidades para que tengas una vision general de los equipos uno a uno en la function getProbabilitiesOneToOne'
        }
        if (contador) {
            contador = contador - 1
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const datosProbabilities = JSON.stringify(response[contador]);

            return `contador: ${contador}, total de probabilidades: ${totalProbabilities} y esto son los datos de la probabilidades: ${datosProbabilities}.
            si el contador no es igual al total enotces debes seguir consultando
            `

        } else {
            return 'debes llamar primero a probabilitiesCount o tener un valor definido de contador pero para eso debes saber el total'
        }
    }
    async getSummariesCount(seasonId: string){
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        const response = await TennisService.getSummaries(seasonId, 0, false);
        const sumamries = (response as any).fullData.summaries;
       
        return sumamries.length
    }

    async getSummariesIa(seasonId: string, contadorSummaries: number,  totalSummaries: number){
        
        if(!contadorSummaries){
            return 'debes consultar getSummariesCount y luego que obtegas el total vuevel aqui'
        }

        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        const totalSummariesFromFunciton = await this.getSummariesCount(seasonId) 
        if(totalSummariesFromFunciton === totalSummaries){
            const response = await TennisService.getSummaries(seasonId, 0, false);
            const sumamries = (response as any).fullData.summaries;
           
            return sumamries[contadorSummaries]

        }else{
        
            return 'creo que no tienes el numero contadorSummaries, debes consultar el getSummariesCount antes'
        }
    }
 

}

export default new TennisAiService();