import CompetitionsModel from '../../../models/competitions';
import CompetitorModel from '../../../models/competitors';
import CompetitionSeasonsModel from '../../../models/competitionsSeasons';
import SeasonCompetitorModel from '../../../models/seasonCompetiror';
import SeasonStandingModel from '../../../models/standings';
import SeasonProbabilitiesModel from '../../../models/probabilities';
import SeasonLeaderModel from '../../../models/seasonLeaders';
import SeasonOverUnderModel from '../../../models/overUnder';

import config from "../../../../config/config"

import TypeSport from "../../../enums/TypeSport";


class BascketBallService{
    urlSource: any; 
    constructor(){
        this.urlSource = config.data.api.sportRadar.basketball;        
    }

    validatedate(date: string) {
        const date2Compare = new Date(date)
        const today = new Date();

        // Extract the year, month, and day from both dates
        const yearCompetition = date2Compare.getFullYear();
        const monthCompetition = date2Compare.getMonth();
        const dayCompetition = date2Compare.getDate();

        const yearToday = today.getFullYear();
        const monthToday = today.getMonth();
        const dayToday = today.getDate();

        if (yearCompetition === yearToday && monthCompetition === monthToday && dayCompetition === dayToday) {
            // console.log('This data is up to date for today.');
            return true;
        } else {
            // console.log('This data is not up to date for today.');
            return false;
        }
    }

    async fetchCompetitions() {
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitionsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchCompetitionsData = await fetch(url);
            const competitionsData = await fetchCompetitionsData.json();
            const competitinosData2Save = {
                data: JSON.stringify(competitionsData),
                fullData: competitionsData,
                competitionType: TypeSport.BasketBall,
                source: "sportRadar",
            };
            const competitions = await CompetitionsModel.create(
                competitinosData2Save
            );
            await competitions.save();
            console.log("Competitions BasketBall Created and Save");
            return competitionsData.competitions;
        } catch (error) {
            console.log("Error Fetch Competitions BasketBall");
            console.log(error);
            return error;
        }
    }
    async fetchUpdateCompetitions() {
        try {
            console.log("Competitions BasketBall Update");
        } catch (error) {
            console.log("ERrorFetch update compettions BasketBall");
            console.log(error);
            return error;
        }
    }

    async getCompetitions() {
        try {
            // const skip = (query.page - 1)* query.limit;
            const competitions = await CompetitionsModel.findOne({
                competitionType: TypeSport.BasketBall,
            })
                .sort({ _id: -1 })
                .lean();
            if (competitions) {
                const dateCompetitions = competitions.fullData.generated_at;

                // if(this.validatedate(dateCompetitions)){
                return competitions.fullData.competitions;
                // }else{
                //     //here its update no fetch
                //     return await this.fetchUpdateCompetitions(competitions._id.toString())
                //     // return await this.fetchCompetitions()
                // }
            } else {
                return await this.fetchCompetitions();
            }
        } catch (error) {
            console.error("Error getting competitions:", error);
            // throw error;
        }
    }
    async getCompetitionAndSeason(competitionId: string) {
        const competitionSeason = await CompetitionSeasonsModel.findOne({ competitionId: competitionId, competitionType: TypeSport.BasketBall }).sort({ _id: -1 }).lean();
        if (competitionSeason) {
            console.log(competitionSeason)
            const season_id = competitionSeason.fullData.seasons[competitionSeason.fullData.seasons.length - 1].id
            const competitorsSeason: any = await this.seasonCompetitors(season_id, 0);

            return this.externalizeCompetitorSeason(competitorsSeason, season_id)
        }
        //if not exits so we need to add into the database
        const season_id = await this.fetchCompetitionSeason(competitionId);
        // console.log(season_id)

        //now find competitors with this  season data
        const competitorsSeason = await this.fetchSeasonCompetitors(season_id)
        console.log(competitorsSeason)
        return this.externalizeCompetitorSeason(competitorsSeason, season_id)
    }

    externalizeCompetitorSeason(comeptitors: any, seasonId: string) {
        return {
            season: {
                id: seasonId
            },
            competitors: comeptitors.season_competitors
        }
    }
    async getCompetitionById(id: string) {
        console.log(id);
        try {
            const competition = await CompetitionsModel.findOne({ competitionType: TypeSport.BasketBall })
                .sort({ _id: -1 })
                .lean();
            if (competition) {
                return this.externalizeCompetitionById(competition, id);
            }

            return { msg: "hello" };
        } catch (error) {
            console.error("Error getting competition by ID:", error);
            return error;
        }
    }

    async externalizeCompetitionById(competitions: any, id: string) {
        const dataCompetition = competitions.fullData.competitions;
        const dataCompetitionRetrun = dataCompetition.filter(
            (competition: { id: string }) => competition.id === id
        );
        const dataCompetitionsCompetitorsSeason = await this.getCompetitionAndSeason(id);

        return {
            ...dataCompetitionRetrun[0],
            ...dataCompetitionsCompetitorsSeason
        };
    }

    //step 2 get all competitons by sesion
    async fetchCompetitionSeason(competitionId: string) {
        console.log("start step 2 fetchCompetitionSeason ", competitionId);
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitions}/${competitionId}/${this.urlSource.seasonsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchCompetitionSeasons = await fetch(url);
            const competitionSeasonsData = await fetchCompetitionSeasons.json();
            if (
                Object.prototype.hasOwnProperty.call(competitionSeasonsData, "seasons")
            ) {
                const data2save = {
                    data: JSON.stringify(competitionSeasonsData),
                    fullData: competitionSeasonsData,
                    competitionId: competitionId,
                    competitionType: TypeSport.BasketBall,
                    sourceData: 'sportRadar'
                };
                const competitionSeasonData = new CompetitionSeasonsModel(data2save);
                competitionSeasonData.save();
                console.log("fetchCompetitionSeason was save  in to db");
                const seasonId =
                    competitionSeasonsData.seasons[
                        competitionSeasonsData.seasons.length - 1
                    ].id;
                return seasonId;
            }
        } catch (error) {
            console.error("Error fetching competition seasons:", error);
            const time = Math.floor(Math.random() * (5000 - 1050 + 1)) + 1050;

            return this.promise2resolverfetchCompetitionSeason(time, competitionId)
            // throw error;
        }
    }
    async getSesonIds() {
        const competitions = await CompetitionSeasonsModel.find({ competitionType: TypeSport.BasketBall });
        const sesasonIds = competitions.map((competition) => { });
        console.log(competitions);
    }
    getCompetitionsDefault() {
        const competitionsDefault = config.data.api.defatulCompetitionsSportRadar.basketball;
        if (competitionsDefault) {
            return competitionsDefault
        }


    }
    promise2resolverfetchCompetitionSeason(t: number, competitionId: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchCompetitionSeason(competitionId));
                // season = this.fetchCompetitionSeason(competitionId);
            }, t);
        });
    }
    async getCompetitionSeasonDefault(competitionId: string, index: number) {
        console.log("start find competition ", competitionId);
        console.log("start find competition index: ", index);
        let season;
        const competitionsSeason = await CompetitionSeasonsModel.findOne({
            competitionId: competitionId,
            competitionType: TypeSport.BasketBall
        });
        if (competitionsSeason) {
            season = competitionsSeason.fullData.seasons;
            let season_id = season[season.length - 1].id;

            return season_id;
        }
        const random = Math.floor(Math.random() * (99999 - 15040 + 1)) + 15040;
        const ramdom2 = Math.floor(Math.random() * (1123 - 499 + 1)) + 499 * random;
        const randomNumber = Math.floor(Math.random() * (500 - 150 + 1)) + 150 * index;
        const time = Math.floor(ramdom2 / randomNumber);
        console.log(time + " y " + index)
        return this.promise2resolverfetchCompetitionSeason(time, competitionId);
    }


    //step 5 season probabilitie

    promise2resolverfetchProbabilities(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonProbabilities(season_id))
            }, 2500 * t)
        })
    }
    promise2resolverfetchUpdateProbabilities(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateProbabilities(season_id))
            }, 2500 * t)
        })
    }
    async probabilities(season_id: string, index: number, update: boolean = true) {
        const probabilities = await SeasonProbabilitiesModel.findOne({ seasonId: season_id, competitionType: TypeSport.BasketBall }).sort({ _id: -1 }).lean();
        if (probabilities) {
            if (!this.validatedate(probabilities.fullData.generated_at) && update) {
                console.log('update probabilities')
                return this.promise2resolverfetchUpdateProbabilities(index, season_id)
            }
            return probabilities
        }
        console.log('New Probabilities')
        return this.promise2resolverfetchProbabilities(index, season_id)
    }

    async fetchSeasonProbabilities(season_id: string) {
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return data2Save;
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promise2resolverfetchProbabilities(time, season_id)
            // throw error;
        }
    }
    async fetchSeasonUpdateProbabilities(season_id: string) {
        try {
            console.log('start to fetch probabilities: ', season_id)
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.probabilitiesJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonPrbabilities = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonPrbabilities),
                fullData: seasonPrbabilities,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SeasonProbabilitiesModel.findOneAndUpdate({ seasonId: season_id, competitionType: TypeSport.BasketBall }, data2Save);
            // const saveSeasonProbabilities = new SeasonProbabilitiesModel(data2Save);
            // saveSeasonProbabilities.save();
            console.log('probabilitie was  save: ', season_id)
            return response;
        } catch (error) {
            console.error('Error fetching season probabilities:', error);
            const time = Math.floor(Math.random() * (50 - 1 + 1)) + 1;
            return this.promise2resolverfetchUpdateProbabilities(time, season_id)
            // throw error;
        }
    }

    /////////////////////////////
    async getAllSeasonAvalible() {
        // const skip = (query.page - 1) * query.limit;
        const competitionsSeasons: any = await CompetitionSeasonsModel.find({ "competitionType": TypeSport.BasketBall })
        // .skip(skip)
        // .limit(query.limit)

        const seasonsSaves: Array<string> = [];

        const seasons = competitionsSeasons
            .map((competiton: any, index: number) => {
                const season = competiton.fullData.seasons[competiton.fullData.seasons.length - 1]
                // console.log(index)
                if (!seasonsSaves.includes(season.id)) {
                    seasonsSaves.push(season.id)
                    return season
                }
            })
            .filter((data: any) => data !== undefined)
        return {
            seasons
        }
    }

    async getSeasonsById(id: string) {
        const seasons = await this.getAllSeasonAvalible();
        const season = seasons.seasons.filter((season: any) => season.id === id)
        const competitionId = season.competition_id;
        let competitor: any = await this.seasonCompetitors(id, 0, false)
        competitor = competitor.fullData;
        return {
            season: season[0],
            competitors: competitor.season_competitors

        }

    }


    // standings
    async getStandingsBySeasonId(seasonId: string) {
        try {
            const standings = await this.getStandings(seasonId)

            if (standings || Object.prototype.hasOwnProperty.call(standings, 'fullData')) {
                return {
                    seasonId,
                    lastUpdate: (standings as { fullData: { generated_at: string } }).fullData.generated_at,
                    standings: (standings as { fullData: { standings: any } }).fullData.standings
                };
            }
        } catch (error) {
            console.error('Error fetching standings:', error);
            // throw error;
        }
    }

    promise2resolverfetchStandings(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonStandings(season_id))
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateStandings(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateStandings(season_id))
            }, 5000 * t)
        })
    }

    async getStandings(season_id: string, index: number = 0, update: boolean = true) {
        const standing = await SeasonStandingModel.findOne({ seasonId: season_id, competitionType: TypeSport.BasketBall }).sort({ _id: -1 }).lean();
        if (standing) {

            if (!this.validatedate(standing.fullData.generated_at) && update) {
                console.log('update standings')
                return this.promise2resolverfetchUpdateStandings(index, season_id)
            }
            return standing
        }
        return this.promise2resolverfetchStandings(index, season_id)
    }

    //step 4 season standing
    async fetchSeasonStandings(season_id: string) {
        console.log('start fetchSeasonStandings with the id: ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.standingsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonFormStadings = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonFormStadings),
                fullData: seasonFormStadings,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                sourceData: 'sportRadar'
            };
            //save in db
            const saveSeasonFormStadings = new SeasonStandingModel(data2Save);
            saveSeasonFormStadings.save();

            return data2Save;
        } catch (error) {
            console.error('Error fetching season form standings:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);

            return this.promise2resolverfetchStandings(time, season_id)

            // throw error;
        }
    }
    async fetchSeasonUpdateStandings(season_id: string) {
        console.log('start fetchSeasonUpdateFormStandings with the id: ', season_id)
        try {
            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.standingsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonFormStadings = await fetchSeason.json();
            const data2Save = {
                data: JSON.stringify(seasonFormStadings),
                fullData: seasonFormStadings,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                sourceData: 'sportRadar'
            };
            //save in db
            const response = await SeasonStandingModel.findOneAndUpdate({ seasonId: season_id, competitionType: TypeSport.BasketBall }, data2Save);
            // saveSeasonFormStadings.save();

            return response;
        } catch (error) {
            console.error('Error fetching season form standings:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchUpdateStandings(time, season_id)

            // throw error;
        }
    }

    //====================

    async seasonCompetitors(id: string, index: number = 0, update: boolean = false) {
        const findSeasonCompetitor = await SeasonCompetitorModel.findOne({ seasonId: id, competitionType: TypeSport.BasketBall }).sort({ _id: -1 }).lean();
        if (findSeasonCompetitor) {
            if (!this.validatedate(findSeasonCompetitor.fullData.generated_at) && update) {
                console.log('update seasonCompetitors ', id, index)
                return this.promise2resolverfetchUpdateSeasonCompetitors(index, id)
            }
            console.log('not need update')
            return findSeasonCompetitor;
        }
        return this.promise2resolverfetchSeasonCompetitors(index, id)
    }
    promise2resolverfetchSeasonCompetitors(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonCompetitors(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }
    promise2resolverfetchUpdateSeasonCompetitors(t: number, season_id: string) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this.fetchSeasonUpdateCompetitors(season_id))
                // season = this.fetchCompetitionSeason(competitionId);
            }, 5000 * t)
        })
    }
    //now neeed catch the teams or competitiors! this its the step 3

    async fetchSeasonCompetitors(season_id: string) {
        console.log('start fetchSeasonCompetitors ', season_id)
        if (season_id === undefined || season_id === null) {
            return 'Season ID not Found'
        }
        try {


            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.competitorsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonCompetitors = await fetchSeason.json();
            const data2saveSeasonCompetitors = {
                data: JSON.stringify(seasonCompetitors),
                fullData: seasonCompetitors,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                source: 'sportRadar'
            };
            //save in db
            const seasonCompetirorSave = new SeasonCompetitorModel(data2saveSeasonCompetitors);
            seasonCompetirorSave.save();
            console.log('fetchSeasonCompetitors save in db first time')

            return data2saveSeasonCompetitors;

        } catch (error) {
            console.error('Error fetching season competitors:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);

            return this.promise2resolverfetchSeasonCompetitors(time, season_id)

            // throw error;
        }
    }
    async fetchSeasonUpdateCompetitors(season_id: string) {
        console.log('start fetchSeasonCompetitors ', season_id)
        try {


            const url = `${this.urlSource.source}${this.urlSource.seasons}/${season_id}/${this.urlSource.competitorsJson}${this.urlSource.urlApiKey}${config.sportRadar.basketball}`;
            const fetchSeason = await fetch(url);
            const seasonCompetitors = await fetchSeason.json();
            const data2saveSeasonCompetitors = {
                data: JSON.stringify(seasonCompetitors),
                fullData: seasonCompetitors,
                seasonId: season_id,
                competitionType: TypeSport.BasketBall,
                source: 'sportRadar'
            };
            const response = await SeasonCompetitorModel.findOneAndUpdate({ seasonId: season_id }, { $set: data2saveSeasonCompetitors });
            console.log('fetchSeasonUpdateCompetitors ')

            return response;

        } catch (error) {
            console.error('Error fetching season competitors:', error);
            const time = Math.floor(Math.random() * (10000 - 1000 + 1) + 1000);
            return this.promise2resolverfetchUpdateSeasonCompetitors(time, season_id)

            // throw error;
        }
    }

}

export default new BascketBallService()