import config from "../../../../config/config";
import CompetitionsModel from '../../../models/competitions';
import TypeSport from "../../../enums/TypeSport";

class NflService{
    urlSource: any; 
    constructor(){
        this.urlSource = config.data.api.sportRadar.nfl;        
    }

    async fetchCompetitions(){
        try {
            const url = `${this.urlSource.source}${this.urlSource.competitionsJson}${this.urlSource.urlApiKey}${config.sportRadar.nfl}`;     
            const fetchCompetitionsData = await fetch(url);        
            const competitionsData = await fetchCompetitionsData.json();   
            const competitinosData2Save = {
                data: JSON.stringify(competitionsData),
                fullData: competitionsData,
                competitionType: TypeSport.NFL,
                source: 'sportRadar'
            }
            const competitions = await CompetitionsModel.create(competitinosData2Save);
            await competitions.save();
            console.log(`Competitions ${TypeSport.NFL} Created and Save`)
            return competitionsData.competitions;  
        } catch (error) {
            console.log('Error Fetch Competitions '+ TypeSport.NFL)
            console.log(error)
            return error
        }
    }
    async fetchUpdateCompetitions(){
        try {
            console.log(`Competitions ${TypeSport.NFL} Update`)
        } catch (error) {
            console.log('ErrorFetch update compettions '+ TypeSport.NFL)
            console.log(error)
            return error
        }
    
    }

    async getCompetitions(){
        try {            
            // const skip = (query.page - 1)* query.limit;
            const competitions = await CompetitionsModel.findOne({competitionType: TypeSport.NFL}).sort({ _id: -1 }).lean();
            if(competitions){
                const dateCompetitions = competitions.fullData.generated_at;
                
                // if(this.validatedate(dateCompetitions)){
                    return competitions.fullData.competitions;
                // }else{                  
                //     //here its update no fetch
                //     return await this.fetchUpdateCompetitions(competitions._id.toString())
                //     // return await this.fetchCompetitions()
                // }
            }else{                  
                return await this.fetchCompetitions()
            }
        } catch (error) {
            console.error('Error getting competitions:', error);
            // throw error;
        }
    }

    async getCompetitionById(id: string ){
        console.log(id)
        const competition = await CompetitionsModel.findOne().sort({_id: -1}).lean()
        if(competition){            
            return this.externalizeCompetitionById(competition, id);
        }

        return {msg: 'hello'} 
    }
    
    async externalizeCompetitionById(competitions: any, id:string){
        const dataCompetition = competitions.fullData.competitions;
        const dataCompetitionRetrun = dataCompetition.filter((competition: {id:string}) => competition.id === id);
        // const dataCompetitionsCompetitorsSeason = await this.getCompetitionAndSeason(id);

        return {
            ...dataCompetitionRetrun[0],
            // ...dataCompetitionsCompetitorsSeason
        }; 
    }   
}

export default new NflService();