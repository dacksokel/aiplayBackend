import config from '../../../../config/config';
import openAiService from '../../openAiService';

import BaseBallService from '../baseball/baseballService'

import TypeSport from '../../../enums/TypeSport';


class BaseBallAiService{

    async questionsAi(question: string, user: any, baseBallData: any) {
        const messages = [
            {
                role: 'system',
                content:
                    config.data.api.systemPromptBeisbol,
            },
            {
                role: 'user',
                content:
                    `ruta de busqueda de datos: 
                    primero standings, de alli puedes obtener ids de los equipos, luego con esos ids puedes buscar en overUnder y probabilities, getcompetitorlastgame es para obtener los datos de un equipo en su ultimo partido, tambien recuerda de responder como un profesional analista deportivo de ${TypeSport.BaseBall}, no como alguien casual, dando respuestas simples y claras, las probabilidades de carreras, home run y mas en caso de no encontrar datos da una probabilidad en base a lo aprendido, de los standings y de todas las probabilidades..
                ${question}  ${JSON.stringify(baseBallData)} es importante considerar otros factores, como el rendimiento reciente del equipo y el desempeño de los jugadores clave.`
                // content: `${question} ${JSON.stringify(baseBallData)}`
            },
        ];

        //now functions
        const functions = [
            {
                name: 'standingFullTimeTotalCount',
                description: `Primeros se debe consultar esto Pregunta por el tamano total full time de standing de la season: ${baseBallData.season.id}, para luego, lo cual dara un numero que expresara el numero de veces que tengra que preguntar a standingFullTimeTotal, el cual ira entregando respuesta en base al numero que le mandes, ejemplo:  standingsFullTimeTotalCount te dara 20, entonces deberas comenzar usando 1, 2, 3 ,4 ,.... hasta el 20 para que de esa forma te de varias informaciones`,
                parameters: {
                    type: 'object',
                    properties: {
                        seasonId: {
                            type: 'string',
                            descriptcion: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${baseBallData.season.id}`,
                            emun: [baseBallData.season.id]
                        }
                    },
                },
                require: ['seasonId'],

            },
            {
                name: 'standingFullTimeTotal',
                description: 'para poder usar esta funcion se debe obtener la cantidad de veces que se consultara de standingFullTimeTotalCount, entregara dato a dato segun el numero que le mandes, el cual dependera del total que previamente se le entregue, si al consultar anteriolmente le dieron 20 el debera consultar para ir aprendiendo sobre la informacion en base al numero total y terminara cuando el total sea igual al numero de veces que pregunto',
                parameters: {
                    type: 'object',
                    properties: {

                        contadorStadings: {
                            type: 'number',
                            description: 'es el numero total que le dara al standingsFullTimeTotal, de esta forma ir recibiendo data'
                        },
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${baseBallData.season.id}`,
                            emun: [baseBallData.season.id]
                        }

                    }
                },
                require: ['contadorStadings', 'seasonId'],
            },
            {
                name: 'probabilitiesCount',
                description: `
                    recuerda consultar standingFullTimeTotal, para poder saber el estatus de los equipos y sus posiciones
                        entrega el numero de probabilidades que se pueden consultar en la season ${baseBallData.season.id}, para que el getProbabilitiesOneToOnelos consulte uno a uno 
                    `,
                parameters: {
                    type: 'object',
                    properties: {
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${baseBallData.season.id}`,
                            emun: [baseBallData.season.id]
                        }
                    }
                },
                require: ['seasonId'],
            },
            {
                name: 'getProbabilitiesOneToOne',
                description: `
                    Siempre que se vaya a usar esta funcion, se debe obtener la cantidad de veces que se consultara con la funcion probabilitiesCount, nunca asumir que ya tienes el numero de conteo de otra funcion que no sea la antes mensionada, entregara dato a dato segun el numero que le mandes, el cual dependera del total que previamente se le entregue, si al consultar anteriolmente le dieron 20 el debera consultar para ir aprendiendo sobre la informacion en base al numero total y terminara cuando el total sea igual al numero de veces que pregunto`,
                parameters: {
                    type: 'object',
                    properties: {

                        contadorProbabilities: {
                            type: 'number',
                            description: 'es el numero total que le dara al getProbabilitiesOneToOne, de esta forma ir recibiendo data uno a uno hasta que termine el conteo'
                        },
                        seasonId: {
                            type: 'string',
                            descriptcion: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${baseBallData.season.id}`,
                            emun: [baseBallData.season.id]
                        },
                        total: {
                            type: 'number',
                            description: `es el total de probabilidades obtenidos desde probabilitiesCount y es necesario para comparar todas las probabilidades posibles de la season que tiene el id ${baseBallData.season.id}`
                        }

                    }
                },
                require: ['contadorProbabilities', 'seasonId', 'total'],
            },
            {
                name: 'getProbabilitiesByCompetitorId',
                description: 'con esta function se puede buscar directamente las probabilities de un competidor, usando el id del mismo, la cual puede ser obtenida en las standings',
                parameters:{
                    type: 'object',
                    properties:{
                        competitorId:{
                            type:'string',
                            description: ` es el id del competidor que se obtiene desde los standigs, es lo que se usa para identificar cada equipo de forma que sea unico un ejemplo del id seria esto que te coloco entre comillas, pero el ejemplo que te doy no debes usarlo en caso reales, solo debes buscar algo asi parecedio en los standings "sr:competitor:numero"`
                        },
                        seasonId: {
                            type: 'string',
                            description: `Ejemplo de id que debes de pasar asi con las palabras "sr:season:105353" es el id que se le asigna a una season, competition y es importante para buscar los equipos normalmente debes usar este ${baseBallData.season.id}`,
                            emun: [baseBallData.season.id]
                        },
                        competitorName: {
                            type: 'string',
                            description: `es el nombre del competidor que se encuentra en la pregunta hecha por el usuario en ${question}, ejemplo de los nombres: que estarann el las comillas "Liverpool, Manchester city, etc"`
                        }
                    }
                },
                require: ['competitorId', 'seasonId', 'competitorName']
            }
        ];

        return await openAiService.conecctionWitFunctionsCall2(functions, messages, TypeSport.BaseBall);

    }

    async callFunction(function_call: any) {
        const args = JSON.parse(function_call.arguments);
        switch (function_call.name) {
            case 'standingFullTimeTotalCount':
                return await this.standingFullTimeTotalCount(args['seasonId']);
            case 'standingFullTimeTotal':
                return await this.standingFullTimeTotal(args['contadorStadings'], args['seasonId']);
            case 'probabilitiesCount':
                return await this.probabilitiesCount(args['seasonId']);
            case 'getProbabilitiesOneToOne':
                return await this.getProbabilitiesOneToOne(args['contadorProbabilities'], args['total'], args['seasonId']);
            case 'getProbabilitiesByCompetitorId':
                return await this.getProbabilitiesByCompetitorId(args['seasonId'], args['competitorId'], args['competitorName']);
            
            default:
                throw new Error('No function found');
        }
    }

    async standingFullTimeTotalCount(seasonId: string) {
        console.log('comenzado a buscar las cantidades')
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        if (seasonId) {
            const stadingForm = await BaseBallService.getStandingsBySeasonId(seasonId)
            const fullTimeTotal = stadingForm?.standings[0].groups[0].standings
            console.log(fullTimeTotal.length)
            return `te recomiendo que una vez obtenido este numero: ${fullTimeTotal.length} consultes todo standingFullTimeTotal para que puedas tener toda tu data`

        }

        return "debe volver a consultar para obtener un numero"
    }

    async standingFullTimeTotal(numerito: number, seasonId: string) {
        if (numerito) {
            numerito = numerito - 1
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const stadingForm = await BaseBallService.getStandingsBySeasonId(seasonId)
            const fullTimeTotal = stadingForm?.standings[0].groups[0].standings
            console.log(`este es el numerito: ${numerito}`)
            return fullTimeTotal[numerito]

        } else {
            return 'debes llamar primero a standingFullTimeTotalCount '
        }
    }

    async getStandings(seasonId: string) {
        console.log(`this its getStandings in prediccion3 id sseason: ${seasonId}`)
        const stadingForm = await BaseBallService.getStandingsBySeasonId(seasonId)

        return JSON.stringify(stadingForm?.standings)
    }


//probabilities data
    async getProbabilities(seasonId: string, team: string) {
        const probabilities = await BaseBallService.probabilities(seasonId, 0, true)

        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
                return rest;
            }
        }).filter((item: any) => item !== undefined);
        // console.log(response)
        console.log(team)
        return JSON.stringify(response);
    }
    async probabilitiesCount(seasonId: string) {
        const isNumber = /^\d+$/.test(seasonId);
        if (isNumber) {
            seasonId = `sr:season:${seasonId}`;
            console.log('era son lo numeros')
        }
        if (seasonId) {
            const probabilities = await BaseBallService.probabilities(seasonId, 0, false)
            const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
                const { channels, ...rest } = item.sport_event;
                // console.log(rest)
                const providedDate = new Date(rest.start_time);

                // Get the current date
                const currentDate = new Date();

                if (providedDate > currentDate) {
                    return rest;
                }
            }).filter((item: any) => item !== undefined);

            return response.length
        }
        return 'si desea obtener las probabilidades de estadisticas de los equipos debes volver a llamar a esta funcion para obtener un numero'
    }

    async getProbabilitiesOneToOne(contador: number, totalProbabilities: number, seasonId: string) {
        console.log(`total probabilities ${totalProbabilities}`)
        const probabilities = await BaseBallService.probabilities(seasonId, 0, false)
        const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
            const { channels, ...rest } = item.sport_event;
            // console.log(rest)
            const providedDate = new Date(rest.start_time);

            // Get the current date
            const currentDate = new Date();

            if (providedDate > currentDate) {
                return rest;
            }
        }).filter((item: any) => item !== undefined);
        if (response.length !== totalProbabilities) {
            return 'debes consultar probabilitiesCount para obtener el numero de veces que se debe consultar las probabilidades, y debes consultar toda las probabilidades para que tengas una vision general de los equipos uno a uno en la function getProbabilitiesOneToOne'
        }
        if (contador) {
            contador = contador - 1
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const datosProbabilities = JSON.stringify(response[contador]);

            return `contador: ${contador}, total de probabilidades: ${totalProbabilities} y esto son los datos de la probabilidades: ${datosProbabilities}.
            si el contador no es igual al total enotces debes seguir consultando
            `

        } else {
            return 'debes llamar primero a probabilitiesCount'
        }
    }

    async getProbabilitiesByCompetitorId(seasonId: string, competitorId: string, competitorName: string) {
        let competitors = await BaseBallService.seasonCompetitors(seasonId);
        competitors = (competitors as any).fullData.season_competitors.some((competitor: any) => {
            if(competitor.id === competitorId && competitor.name.includes(competitorName)){
                console.log(competitor)
                return true;
            }
        })
        if (competitorId && competitors) {
            const isNumber = /^\d+$/.test(seasonId);
            if (isNumber) {
                seasonId = `sr:season:${seasonId}`;
                console.log('era son lo numeros')
            }
            const probabilities = await BaseBallService.probabilities(seasonId, 0, false)
            const response = (probabilities as any).fullData.sport_event_probabilities.map((item: any) => {
                const { channels, ...rest } = item.sport_event;
                // console.log(rest)
                const providedDate = new Date(rest.start_time);

                // Get the current date
                const currentDate = new Date();
                const competitor = rest.competitors.some((competitorSome: any) => competitorSome.id === competitorId)
                if (providedDate > currentDate && competitor) {
                    return rest;
                }
            }).filter((item: any) => item !== undefined);
            const responseString = JSON.stringify(response)
            return `recuerda que estos datos deben estar asociados a la pregunta der usuario ${responseString}`
        }else{
            return 'posiblemente el competitorId no es el correcto debes consultar standingFullTimeTotalCount o en standingFullTimeTotal, para poder obterner las id de competitor, recuerda que el id del competidor debe ser el que el usuario require no el que tu elijas al asar o inventes un id'
        }
        return "debes obtener el competitor id, posiblemente lo puedes obtener en la lista de stadings"
    }


}

export default new BaseBallAiService();