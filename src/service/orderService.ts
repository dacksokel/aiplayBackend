import OrderModel from '../models/order';
import IOrder from '../interfaces/IOrder';
import StripeService from './stripeService';
import UserService from './usersService';
import PaymentService from './paymentService';

import IUser from '../interfaces/IUsers';
class OrderService {

    async createOrder(userId: string , datos: any) {
        const orderCode = this.generateCodeOrder();
        const user = await UserService.getUserById(userId)
        const paymentData = {
            code: datos.code,
            email: user.email,
            cancel_url: datos.url.cancel,
            success_url: datos.url.success
        }
        const paymentSession = await PaymentService.createPaymentSession(paymentData)
        const paymentSessionId = (paymentSession as any).id;
        const paymentTotalAmount = ( paymentSession as any).amount_total;
        const paymentStatus = (paymentSession as any).status;


            const newOrder = new OrderModel({
                orderCode,
                userId: user._id,
                planCode: datos.code,
                paymentMethod: {
                    transactionId: paymentSessionId,
                    amount: paymentTotalAmount,
                },
                status: paymentStatus
            })

            const order = await OrderModel.create(newOrder);

            return {
                orderCode: order.orderCode,
                status: order.status,
                url_payment: (paymentSession as any).url
            }
    }

    generateCodeOrder() {
        const date = new Date();
        const year = date.getFullYear().toString().slice(-2);
        const month = (`0${date.getMonth() + 1}`).slice(-2);
        const day = (`0${date.getDate()}`).slice(-2);

        const randomNumber = Math.floor(Math.random() * 100000);

        const randomLongNumber = Math.floor(Math.random() * 100000000000);

        const orderCode = `${year}${month}${day}-${randomNumber}-${randomLongNumber}`;

        return orderCode;
    }

    async updateOrder(paymentId: string, paymentStatus: string) {
        await OrderModel.updateOne({ 'paymentMethod.transactionId': paymentId }, { "status": paymentStatus })
    }

    async getOrdersService(filter: any, options: any) {
        const skip = (options.page - 1) * options.limit;

        const total = await OrderModel.estimatedDocumentCount();

        const orders = await OrderModel
            .find(filter || {})
            .sort({ createdAt: -1 })
            .skip(skip)
            .limit(options.limit)
            .exec();


        return this.externalizeForGetAllOrdersByPageAndLimit(orders, total, options.limit, options.page);
    };

    externalizeForGetAllOrdersByPageAndLimit(orders: any, total: any, limit: any, page: any) {
        return {
            total,
            page,
            limit,
            orders
        }
    }

    externalize(order: IOrder, paymentSession: any) {
        return {
            orderCode: order.orderCode,
            status: order.status,
            url_payment: paymentSession.url2
        }
    };
}

export default new OrderService();