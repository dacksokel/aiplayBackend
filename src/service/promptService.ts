import IPrompt from '../interfaces/IPrompt';
import { PromptModel } from '../models/Prompt';

class PromptService {
  async getAllPrompts(): Promise<IPrompt[]> {
    try {
      const prompts = await PromptModel.find();
      return prompts;
    } catch (error) {
      throw new Error('Failed to get prompts');
    }
  }

  async getPromptById(id: string): Promise<IPrompt | null> {
    try {
      const prompt = await PromptModel.findById(id);
      return prompt;
    } catch (error) {
      throw new Error('Failed to get prompt');
    }
  }

  async createPrompt(prompt: IPrompt): Promise<IPrompt> {
    try {
      const newPrompt = await PromptModel.create(prompt);
      return newPrompt;
    } catch (error) {
      console.log(error);
      throw new Error('Failed to create prompt');
    }
  }

  async updatePrompt(id: string, prompt: IPrompt): Promise<IPrompt | null> {
    try {
      const updatedPrompt = await PromptModel.findByIdAndUpdate(id, prompt, { new: true });
      return updatedPrompt;
    } catch (error) {
      throw new Error('Failed to update prompt');
    }
  }

  async deletePrompt(id: string): Promise<IPrompt | null> {
    try {
      const deletedPrompt = await PromptModel.findByIdAndDelete(id);
      return deletedPrompt;
    } catch (error) {
      throw new Error('Failed to delete prompt');
    }
  }
}

export default PromptService;
