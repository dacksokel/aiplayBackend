import config from '../../config/config';
import Standings from '../models/standings';
import SeasonLeaders from '../models/seasonLeaders';
import OverUnder from '../models/overUnder';
import Probabilities from '../models/probabilities';
import fs from 'fs';
import path from 'path';
import OpenAiService from "./openAiService";
import openAiService from './openAiService';
import SoccerService from './sportRadar/soccer/soccerService';


// import { RecursiveCharacterTextSplitter } from "langchain/text_splitter";

class IaPremierLeagueService {


    async test(question: string) {
        try {
            const standings = await this.getStandings();
            const playerLeader = await this.getLeders();
            const overUnder = await this.getOverUnder();
            const porbabilities = await this.getProbabilitie();

            console.log(standings);

            // console.log(playerLeader.length);
            // console.log(overUnder.length);
            // console.log(porbabilities.length);

            const arrayTojson = [{ role: "system", content: "analista de datos deportivos" }];
            // arrayTojson.push({ role: "user", content: standings })
            // // const responseOpenAi1 = await this.conecctionWithOpenAi(arrayTojson);
            // // arrayTojson.push({ role: responseOpenAi1?.role ?? "" , content: responseOpenAi1?.content ?? "" })
            // // arrayTojson.push({ role: "user", content: playerLeader })
            // arrayTojson.push({ role: "user", content: 'haz un match de equipos y sus jugadores' })
            // arrayTojson.push({ role: "user", content: 'y analisa a cada equipo segun su jugador' })
            // // arrayTojson.push({ role: "user", content: overUnder })
            // arrayTojson.push({ role: "user", content: 'haz un match de equipos y sus under over' })
            // arrayTojson.push({ role: "user", content: 'analisas sus datos segun los match' })
            // // arrayTojson.push({ role: "user", content: porbabilities })
            // arrayTojson.push({ role: "user", content: 'haz un match de equipos y sus probabilidades de victoria' })
            arrayTojson.push({ role: "user", content: 'analisas sus datos segun los match y reflexiona sobre cada equipo' })
            arrayTojson.push({ role: "user", content: question })

            const responseOpenAi = await OpenAiService.conecctionWithOpenAi(arrayTojson);

            // console.log(responseOpenAi)
            return responseOpenAi;
        } catch (error) {
            // console.log('Error fetching standings:', error);
            throw new Error('Error fetching standings');
        }
    }

    async fetchStandings() {
        try {
            const api = config.sportRadar.soccer;
            const url = config.data.api.sportRadar.soccer.standings;
            const response = await fetch(url + api);
            const data = await response.json();
            const dataStandings = data.standings[0].groups[0].standings;
            // await this.convertToJSONLStanding(dataStandings);
            const datosToString = JSON.stringify(dataStandings);
            const standings = new Standings({ data: datosToString, fullData: data });
            await standings.save();

        } catch (error) {
            console.log('Error fetching season leaders:', error);
            throw new Error('Error fetching season leaders');
        }
    }
    async fetchSeasonLeaders() {
        try {
            const api = config.sportRadar.soccer;
            const url = config.data.api.sportRadar.soccer.SeasonLeaders;
            const response = await fetch(url + api);
            const data = await response.json();
            const dataReal = JSON.stringify(data.lists);
            // await this.convertToJSONLSeasonLeaders(dataReal);
            const seassonLeaders = new SeasonLeaders({ data: dataReal, fullData: data });
            await seassonLeaders.save();

        } catch (error) {
            console.log('Error fetching season leaders:', error);
            throw new Error('Error fetching season leaders');
        }
    }
    async fetchOverUnder() {
        try {
            const api = config.sportRadar.soccer;
            const url = config.data.api.sportRadar.soccer.overUnder;
            const response = await fetch(url + api);
            const data = await response.json();
            const overUnder = JSON.stringify(data.competitors);
            const overUnderSave = new OverUnder({ data: overUnder, fullData: data });
            await overUnderSave.save();
        } catch (error) {
            console.log('Error fetching over under:', error);
            throw new Error('Error fetching fetchOverUnder');
        }
    }

    async fetchProbabilities() {
        try {
            const api = config.sportRadar.soccer;
            const url = config.data.api.sportRadar.soccer.probabilities;
            // await this.getProbabilitiesByRangeDateAndTeam('Sheffield United');
            const response = await fetch(url + api);
            const data = await response.json();

            const dataToretor = await this.getProbabilitiesByRangeDate(data);
            // console.log(dataToretor)
            const probabilities = new Probabilities({ data: dataToretor, fullData: data });
            await probabilities.save();
        } catch (error) {
            console.log('Error fetching probabilities:', error);
            throw new Error('Error fetching probabilities');
        }
    }
    async getProbabilitiesByRangeDate(data: any) {
        let dataToretor = '';
        if (!data) {
            data = (await this.getProbabilitie()).fullData;
        }
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        const todayMs = today.getTime();
        for (const probabiliti of data.sport_event_probabilities) {

            const startTime = new Date(probabiliti.sport_event.start_time);
            startTime.setHours(0, 0, 0, 0);
            const startTimeMs = startTime.getTime();

            const diff = startTimeMs - todayMs;
            const tenDaysMs = 20 * 24 * 60 * 60 * 1000;

            if (diff >= 0 && diff < tenDaysMs) {
                dataToretor += JSON.stringify(probabiliti);
            }
        }
        return dataToretor;
    }

    async getProbabilitiesByRangeDateAndTeam(team: string) {
        const data = (await this.getProbabilitie()).fullData;

        const today = new Date();
        today.setHours(0, 0, 0, 0);
        const todayMs = today.getTime();
        let dataToretor: object = {};
        for (const probabiliti of data.sport_event_probabilities) {

            const startTime = new Date(probabiliti.sport_event.start_time);
            startTime.setHours(0, 0, 0, 0);
            const startTimeMs = startTime.getTime();

            const diff = startTimeMs - todayMs;
            const tenDaysMs = 20 * 24 * 60 * 60 * 1000;

            if (diff >= 0 && diff < tenDaysMs) {
                if (probabiliti.sport_event.competitors[0].name.includes(team) || probabiliti.sport_event.competitors[1].name.includes(team)) {
                    // console.log(probabiliti.sport_event)
                    dataToretor = probabiliti;
                }
                // dataToretor += JSON.stringify(probabiliti);
            }
        }
        // const responseOpenAi = await OpenAiService.conecctionWithOpenAiSimple(JSON.stringify(dataToretor));
        return dataToretor;
    }

    async fetchCompetor() {
        try {
            const api = config.sportRadar.soccer;
            const url = config.data.api.sportRadar.soccer.competitor;
            const response = await fetch(url + api);
            const data = await response.json();
            console.log(data);
            //             const competitors = new Competitors({ data });
            //             await competitors.save();
        } catch (error) {
            console.log('Error fetching competitors:', error);
            throw new Error('Error fetching competitors');
        }
    }


    async getStandings() {
        try {
            const lastStandings = await Standings.findOne({seasonId: 'sr:season:105353'}).sort({ _id: -1 }).lean();
            // console.log(lastStandings);
            if (lastStandings) {
                return lastStandings;
            } else {
                throw new Error('Error getting standings');
            }
        } catch (error) {
            throw new Error('Error getting standings');
        }
    }

    async getStandingsByMessage(question: string) {
        try {
            const lastStandings = await Standings.findOne({seasonId: 'sr:season:105353'}).sort({ _id: -1 }).lean();
            if (lastStandings) {
                const standings = (await this.getStandings()).fullData.standings[0].groups[0].standings
                const arrayTojson = [
                    { role: "system", content: "analisa la tabla de posiciones de la premier league y ofrece respuestas clara y cortas, casi resumidas" },
                    // { role: "user", content: question }
                    { role: "user", content: "aqui tienes la data de las stadings: "+JSON.stringify(standings) }
                ];
                if (!question || question === '' ) {
                    question = 'analiza este dato y hazme un resumen'
                 }
                arrayTojson.push({ role: "user", content: question })
                // const responseOpenAi = await OpenAiService.conecctionWithOpenAi(arrayTojson);
                // arrayTojson.push({ role: responseOpenAi?.role ?? "", content: responseOpenAi?.content ?? "" })            

                return await OpenAiService.conecctionWithOpenAi(arrayTojson);
            } else {
                throw new Error('Error getting standings');
            }
        } catch (error) {
            throw new Error('Error getting standings');
        }
    }

    async getLeders() {
        try {
            const lastStandings = await SeasonLeaders.findOne({seasonId: 'sr:season:105353'}).sort({ _id: -1 }).lean();
            // console.log(lastStandings);
            if (lastStandings) {
                return lastStandings;
            } else {
                throw new Error('Error getting standings');
            }
        } catch (error) {
            throw new Error('Error getting standings');
        }
    }

    async getLeadersByTeamAndPoints(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'points' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }

    async getLeadersPoints() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'points' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];
            }
        }
    }
    async getLeadersByTeamAndGoals(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'goals' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;

                    for (const players of playersLeaders) {
                        for (const player of players.players) {
                            if (player.competitors[0].name.includes(team)) {
                                playersReturn.push(player);
                            }
                        }
                    }
                
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }

    async getLeadersGoals() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'goals' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];
            }
        }
        // return playersReturn;
    }

    async getLeadersByTeamAndAssists(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'assists' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }

    async getLeadersAssists() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'assists' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];
            }
        }
    }
    async getLeadersByTeamAndRedCards(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'red_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }

    async getLeadersRedCards() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'red_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];;
            }
        }
    }

    async getLeadersByTeamAndYellowRedCards(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'yellow_red_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];;
    }
    async getLeadersYellowRedCards() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'yellow_red_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];
            }
        }
    }

    async getLeadersByTeamAndYellowCards(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'yellow_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }
    async getLeadersYellowCards() {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'yellow_cards' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                return playersLeaders ?? ['sin datos por ahora'];
            }
        }
    }

    async getLeadersByTeamAndOwnGoals(team: string) {
        const data = (await this.getLeders()).fullData;
        const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'own_goals' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    for (const player of players.players) {
                        if (player.competitors[0].name.includes(team)) {
                            playersReturn.push(player);
                        }
                    }
                }
            }
        }
        return playersReturn ?? ['sin datos por ahora'];
    }

    async getLeadersAndOwnGoals() {
        const data = (await this.getLeders()).fullData;
        // const playersReturn: any[] = [];
        for (const playerList of data.lists) {
            if (playerList.type === 'own_goals' && Object.hasOwnProperty.call(playerList, 'leaders')) {
                const playersLeaders = playerList.leaders;
                for (const players of playersLeaders) {
                    return players ?? ['sin datos por ahora'];


                }
            }
        }

    }
    async getOverUnder() {
        try {
            const lastStandings = await OverUnder.findOne({seasonId: 'sr:season:105353'}).sort({ _id: -1 }).lean();
            // console.log(lastStandings);
            if (lastStandings) {
                return lastStandings;
            } else {
                throw new Error('Error getting standings');
            }
        } catch (error) {
            throw new Error('Error getting standings');
        }
    }

    async getOverUnderByTeam(team: string) {
        const data = (await this.getOverUnder()).fullData;
        let overUnderReturn: any[] = ['sin datos por ahora'];
        if (data.competitors.length > 0) {
            for (const teamOverUnder of data.competitors) {
                if (teamOverUnder.name.includes(team)) {
                    overUnderReturn =  teamOverUnder ?? ['sin datos por ahora']; 
                }
            }
        }
        return overUnderReturn;
    }
    async getProbabilitie() {
        try {
            const lastProbabilitie = await Probabilities.findOne({seasonId: 'sr:season:105353'}).sort({ _id: -1 }).lean();
            // console.log(lastProbabilitie);
            if (lastProbabilitie) {
                return lastProbabilitie;
            } else {
                throw new Error('Error getting standings');
            }
        } catch (error) {
            throw new Error('Error getting standings');
        }
    }


    async convertToJSONLStanding(dataStandings: any) {
        const systemRole = { role: "system", content: "agregando data sobre la premier league 23/24 estos son los Standings" };
        const assistantRole = { role: "assistant", content: "estos son los Standings de cada equipo que seguire aprendiendo" };
        const filePath = path.resolve(__dirname, '../../config/jsonl/standings.jsonl');
        const dir = path.dirname(filePath);

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir); // crear directorio padre si no existe
        }

        // message.push({ "role": "user", "content": "en este momento quiero que entiendas que estamos en el ano 2023 y la nueva tempodara de la premier league comenzo\nacontinuacion te dare los datos de la standings mas actualizados que hay, deberias responder si, ya que quiero que actues como un analista deportivo" });
        // let responseOpenAi = await this.conecctionWithOpenAi(message);
        // message.push({ "role": "assistant", "content": responseOpenAi?.content ?? "" });

        for (const team of dataStandings) {
            const message = [];
            let contentData = '';
            for (const key in team) {
                if (team[key] !== null && typeof team[key] === 'object') {
                    for (const key2 in team[key]) {
                        contentData = contentData + key2 + ': ' + team[key][key2] + ', ';
                    }
                } else {
                    contentData = contentData + key + ': ' + team[key] + ', ';
                }
            }
            console.log({ "role": "user", "content": contentData })
            contentData = contentData + 'analisa estos datos bien y ordenalos segun los datos que te estoy dando, haz match con cada dato en referencia a cada equipo';
            let responseOpenAi = await OpenAiService.conecctionWithOpenAiSimple(contentData);
            console.log(responseOpenAi?.content)
            message.push(systemRole, { "role": "user", "content": contentData }, { "role": "assistant", "content": responseOpenAi?.content ?? "" });


            // this.saveJSONL(message, 'standings');

            // message.push(assistantRole);
            if (fs.existsSync(filePath)) {
                // El archivo existe, hacemos append
                fs.appendFileSync(filePath, JSON.stringify({ "messages": message }));

            } else {
                // El archivo no existe, lo creamos
                console.log({ "messages": message })
                fs.writeFileSync(filePath, JSON.stringify({ "messages": message }));
            }
        }
    }
    async convertToJSONLSeasonLeaders(dataSeasonLeaders: any) {
        const systemRole = { role: "system", content: "agregando data sobre la premier league season 23/24 estos son los Season Leaders" };
        const assistantRole = { role: "assistent", content: "estos son los Season Leaders" };
        for (const player of dataSeasonLeaders) {
            const message = [];
            let contentData = '';
            for (const key in player) {
                if (player[key] !== null && typeof player[key] === 'object') {
                    for (const key2 in player[key]) {
                        contentData = contentData + key2 + ': ' + player[key][key2] + ', ';
                    }
                } else {
                    contentData = contentData + key + ': ' + player[key] + ', ';
                }
            }
            // console.log({ "role": "user", "content": contentData })
            message.push(systemRole, { "role": "user", "content": contentData }, assistantRole);

            this.saveJSONL(message, 'seasonLeaders');
        }
    }
    saveJSONL(message: any, nameFile: string) {
        const filePath = path.resolve(__dirname, '../../config/jsonl/' + nameFile + '.jsonl');
        const dir = path.dirname(filePath);

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir); // crear directorio padre si no existe
        }

        if (fs.existsSync(filePath)) {
            // El archivo existe, hacemos append
            fs.appendFileSync(filePath, JSON.stringify({ "messages": message }) + '\n');

        } else {
            // El archivo no existe, lo creamos
            console.log({ "messages": message })
            fs.writeFileSync(filePath, JSON.stringify({ "messages": message }) + '\n');
        }
        console.log(`data saved in ${filePath} ${nameFile}`)
    }

    async prediccion(team1: string, team2: string, question: string, systemPrompt: string) {
        try {
            console.log(team1);
            const standings = await this.getStandings();
            const overUnder = await this.getOverUnder();
            const porbabilities = await this.getProbabilitie();
            const standingsFullData = standings.fullData;

            let team1Stadistics: any, team2Stadistics: any;
            let team1Standis: any, team2Standis: any;
            let team1OverUnder: any, team2OverUnder: any;
            let team1Id: any, team2Id: any;
            let team1Probabilitie: any, team2Probabilitie: any;

            for (const team of standingsFullData.standings[0].groups[0].standings) {
                // console.log(team.competitor.name)
                if (team.competitor.name.includes(team1)) {
                    // console.log(team.competitor);
                    team1Id = team.competitor.id;
                    team1Standis = team;
                    const url = config.data.api.sportRadar.soccer.SeasonCompetitorStadits;
                    const response = await fetch(`${url}${team.competitor.id}/statistics.json?api_key=${config.sportRadar.soccer}`);
                    team1Stadistics = await response.json();
                }

                if (team.competitor.name.includes(team2)) {
                    // console.log(team.competitor);
                    team2Standis = team;
                    team2Id = team.competitor.id;
                    const url = config.data.api.sportRadar.soccer.SeasonCompetitorStadits;
                    const response = await fetch(`${url}${team.competitor.id}/statistics.json?api_key=${config.sportRadar.soccer}`);
                    team2Stadistics = await response.json();

                }
            }
            console.log(`finalizado standings`)

            for (const team of overUnder.fullData.competitors) {
                if (team.id === team1Id) {
                    team1OverUnder = team;
                    // console.log(team);
                }
                if (team.id === team2Id) {
                    team2OverUnder = team;
                }
            }
            console.log(`finalizado overUnder`)
            for (const team of porbabilities.fullData.sport_event_probabilities) {
                const id1 = team.sport_event.competitors[0].id;
                const id2 = team.sport_event.competitors[1].id;
                if (id1 == team1Id && id2 == team2Id || id1 == team2Id && id2 == team1Id) {
                    team1Probabilitie = team;
                    // console.log(team)
                }
                // if (team.sport_event.competitors[0].id === team2Id || team.sport_event.competitors[1].id === team2Id) {
                //     team2Probabilitie = team;
                //  }
            }
            console.log(`finalizado probabilities`)

            // const complitorVsCompetitorResponse = await fetch(`https://api.sportradar.us/soccer/trial/v4/en/competitors/${team1Id}/versus/${team2Id}/summaries.json?api_key=h6dcvj3g23rhm7q9uhznykrr`)
            // const competitorVsCompetitor = await complitorVsCompetitorResponse.json();

            const message = [];
            const systemContent = systemPrompt;

            message.push({ role: "system", content: systemContent });
            message.push({ role: "user", content: JSON.stringify(team1Standis) });
            message.push({ role: "user", content: JSON.stringify(team2Standis) });
            console.log('push standings`')
            message.push({ role: "user", content: JSON.stringify(team1Stadistics) });
            message.push({ role: "user", content: JSON.stringify(team2Stadistics) });
            console.log('push stadisticas`')

            // message.push({ role: "user", content: JSON.stringify(team1OverUnder) });
            // message.push({ role: "user", content: JSON.stringify(team2OverUnder) });
            console.log('push overUnder`')

            message.push({ role: "user", content: JSON.stringify(team1Probabilitie) });
            console.log('push prababilities`')

            // message.push({ role: "user", content: JSON.stringify(competitorVsCompetitor.last_meetings) });
            // console.log('push competitorVsCompetitor`')

            message.push({ role: "user", content: question })


            const responseOpenAi = await OpenAiService.conecctionWithOpenAi(message);

            // console.log(responseOpenAi)
            // return responseOpenAi;
            // return { message: 123, team1: team1, team2: team2, response: 123 }
            return { message: 11, response: responseOpenAi }
        } catch (error) {
            console.error(error);
            // handle error
        }
    }

    async getCompetitiorLastGame(id: string) {
        const url = config.data.api.sportRadar.soccer.SeasonCompetitorStadits;
        const response = await fetch(`${url}${id}/statistics.json?api_key=${config.sportRadar.soccer}`);
        const data = await response.json();
        // console.log(data)
        const arrayTojson = [{ role: 'system', content: 'haz un resumen de estos datos' }, {role:'user', content: JSON.stringify(data)}];
        const responseOpenAi = await OpenAiService.conecctionWithOpenAi(arrayTojson);

        return responseOpenAi;
    }


    async botTelegramPrediction1(teamId:string){
        const today = new Date().toISOString().split('T')[0];
        return today
    }

    async liveGame1(teamId: string) { 
        console.log(teamId)
        const dataFetch = await fetch('https://api.sportradar.us/soccer/trial/v4/en/schedules/live/summaries.json?api_key=h6dcvj3g23rhm7q9uhznykrr');
        let returnData: any = ['no data'];
        const data = await dataFetch.json();
        for (const sportData of data.summaries) {
            if (sportData.sport_event.competitors[0].id === teamId || sportData.sport_event.competitors[1].id === teamId) {
                returnData = sportData;
            }
            
        }
        return returnData;
    }

    async getAllCompetitions(competition: string){
        const allLeague = await SoccerService.getCompetitions();
        
        const allLeagueText = JSON.stringify(allLeague)
        return allLeagueText;
        // const splitter = new RecursiveCharacterTextSplitter({
        //     chunkSize: 10,
        //     chunkOverlap: 1,
        //   });
          
        //   const output = await splitter.createDocuments([allLeagueText]);
        //   console.log(output)
    }

    async prediccion2(team1: string, team2: string, question: string, systemPrompt: string  = '') {
        const messages = [
            {
                role: 'system',
                content:
                    systemPrompt,
            },
            {
                role: 'user',
                content:
                    `ruta de busqueda de datos: 
                    primero standings, de alli puedes obtener ids de los equipos, luego con esos ids puedes buscar en overUnder y probabilities, getcompetitorlastgame es para obtener los datos de un equipo en su ultimo partido
                ${question}
`
            },
        ];
        const functions = [
//             {
//                 name: 'ruta',
//                 description: `
//                 - ruta: Realiza una ruta específica.
//                 - overUnder: Obtiene estadísticas y probabilidades de goles, incluyendo en qué periodo de tiempo se anotan los goles y las probabilidades de empate.
//                 - standings: Obtiene los datos de los equipos en la tabla de la Premier League, como puntos, goles, victorias y derrotas.
//                 - probabilities: Obtiene las probabilidades de los equipos en un partido, en relación con los datos de overUnder.
//                 - bestPlayersPointsByTeam: Obtiene los mejores jugadores de un equipo según los puntos acumulados.
//                 - bestPlayersGoalsByTeam: Obtiene los mejores jugadores de un equipo según los goles acumulados.
//                 - bestPlayersAssistsByTeam: Obtiene los mejores jugadores de un equipo según las asistencias acumuladas.
//                 - playersRedCardsByTeam: Obtiene los jugadores de un equipo que tienen tarjetas rojas.
//                 - playersYellowRedCardsByTeam: Obtiene los jugadores de un equipo que tienen tarjetas rojas y amarillas.
//                 - playersYellowCardsByTeam: Obtiene los jugadores de un equipo que tienen tarjetas amarillas.
//                 - playersOwsGoalsyTeam: Obtiene los jugadores de un equipo que han hecho autogoles.
//                 - playersOwnGoals: Obtiene los jugadores que han hecho autogoles.
//                 - playersLeadersByPoints: Obtiene los jugadores con más puntos.
// `,
//             },
            {
                name: 'overUnder',
                description: 'el overUnder es una lista de datos de probabilidades de goles, retorna si los goles son anotados en que periodo de tiempo si en el primer tiempo o en el segundo adicionalmente tambien da probabilidades si el equipo quedara en empate',
                parameters: {
                    type: 'object',
                    properties: {
                        team: {
                            type: 'string',
                            description: 'nombre del equipo que se quiere saber las estadisticas de goles, puede dar una probabilidad para hacer apuestas',
                        },
                    },
                },
            },
            {
                name: 'standings',
                description: 'standings toma de la base de dato los nombre de cada equipo de la premier league, y con esos nombres puede buscar en probabilities quienes son los proximos a enfrentarse? y retorna los datos de los mismos, como puntos, goles, victorias y derrotas',
                parameters: {
                    type: 'object',
                    properties: {
                        question: {
                            type: 'string',
                            description: 'pregunta que se le hace al sistema en base a la pregunta hecha por el usaurio con respecto a las posiciones de la tabla',
                        },
                    }
                },
                require: ['question'],
            },
            {
                name: 'liveGame1',
                description: 'entrega los datos del partido en vivo',
                parameters: {
                    type: 'object',
                    properties: {
                        teamId: {
                            type: 'string',
                            description: 'es el id del equipo se parece a este que te dare como ejemplo: sr:competitor:4722',
                        },
                    },
                },
            },
            // {
            //     name: 'standings',
            //     description: 'standings es la lista de pocicion en la premier league y retorna los datos de los mismos, como puntos, goles, victorias y derrotas',
            //     parameters: {
            //         type: 'object',
            //         properties: {
            //             question: { type: 'string' },
            //         }
            //     }
            // },
            {
                name: 'probababilities',
                description: 'lista de probabilidades que tienen los equipos en un vs,debe ser match con los over under retorna la lista de partidos agendadas en el calendario, el marker es quien tiene la probabilidad de victoria segun la planificacion del partido',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' },
                    }
                },
                require: ['team'],
            },
            {
                name: 'bestPlayersPointsByTeam',
                description: ' lista de los mejores jugadores de la premier league segun su equipo y puntos acumuldos y retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'bestPlayersGoalsByTeam',
                description: 'lista de los mejores jugadores de la premier league segun su equipo y los goles acumuldos y retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'bestPlayersAssistsByTeam',
                description: 'lista de los mejores jugadores de la premier league segun su equipo y las aisstencias acumuldos y retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersRedCardsByTeam',
                description: 'playersRedCardsByTeam es la lista de los jugadores de la premier league que tienen tarjetas rojas por equipos, retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersYellowRedCardsByTeam',
                description: 'playersYellowRedCardsByTeam es la lista de los jugadores de la premier league que tienen tarjetas rojas y amarillas por equipos, retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersYellowCardsByTeam',
                description: 'playersYellowCardsByTeam es la lista de los jugadores de la premier league que tienen tarjetas amarillas por equipos, retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersOwsGoalsyTeam',
                description: 'lista de los jugadores de la premier league que han hecho auto goles por equipos, retorna los datos de los mismos, como puntos, goles',
                parameters: {
                    type: 'object',
                    properties: {
                        team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersOwnGoals',
                description: 'lista de jugadores que han hecho auto goles, retorna un objeto de jugadores que han hecho auto gol',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            // {
            //     name: 'playersLeadersByPoints',
            //     description: 'lista de jugadores por puntos, retorna un objeto de jugadores que mas puntos han hecho',
            //     parameters: {
            //         type: 'object',
            //         properties: {
            //             // team: { type: 'string' }
            //         }
            //     }
            // },
            {
                name: 'playersLeadersByGoals',
                description: 'lista de jugadores por goles, retorna un objeto de jugadores que mas goles han hecho',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersLeadersByYellowCards',
                description: 'lista de jugadores que tienen tarjeta amarilla, retorna un objeto de jugadores tienen tarjeta amarilla',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersLeadersYellowRedCards',
                description: 'lista de jugadores que tienen tanto tarjeta amarilla como tarjeta rojo, retorna un objeto de jugadores que tienen tarjetas amarillas y rojas',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersLeadersByRed',
                description: 'lista de jugadores que tienen tarjeta roja y regresa el objeto de los jugadores que tienen dicho estatus',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            {
                name: 'playersLeadersByAssists',
                description: 'lista de jugadores por puntos de asistencia en hacer goles, retorna un objeto de jugadores que mas puntos han hecho asistencias',
                parameters: {
                    type: 'object',
                    properties: {
                        // team: { type: 'string' }
                    }
                }
            },
            {
                name: 'getCompetitior',
                description: 'esta funcion Obtiene todos los datos de un equipo/team por medio del id que puede ser obtenida en los standings, retorna un objeto con todos los datos del equipo los cuales pueden ser relevantes para determinar como esta jugando el equipo y que cosas puede hacer en futuros encuentros',
                parameters: {
                    type:'object',
                    properties: {
                        id: {
                            type: 'string',
                            description: 'este id debe ser obtenido usando probabilities y buscando el id del equipo que se quiere obtener los datos',
                            // enum: ['sr:competitor:17']
                        }
                    }
                },
                require: ['id']
            },
            {
                name: 'botTelegramPrediction1',
                description: `esta funcion obtiene datos de prediccion, standings y datos del equipos y genera una predicion bajo un formato muy especifico usando la siguiente nomesclatura para dar la respuesta:
                formato de respuesta:
                {
                    PREDICCION - {fecha_actual}
    
                    🏆 {equipo}
                    ⚽️ {posibles goles a anotar}
                    🕐 {posible tiempo en el que anotaran los goles}
                    🟡 {posible cantidad de tarjetas amarillas}
                    🔴 {Posible cantida de tarjetas rojas}
                    🚩 {posible cantidad de Corners}
                }

                debes mantener el formato de respuesta pero ser ingenioso en las respuestas, con respecto a los corners puedes inventarlos, pero manten estadisticas
                `,
                parameters: {
                    type: 'object',
                    properties: {
                        // teamId: {
                        //     type: 'string',
                        //     description: 'es el Id del equipo que se debe obtener ejemplo del ',
                        //     // enum: ['sr:competitor:17']
                        // }
                    },
                },
            },
            {
                name: 'getAllCompetitions',
                description: 'esta funcion entregra en formato String todas las competiciones de futbol, con sus ids',
                parameters: {
                    type:'object',
                    properties: {
                        competition: {
                            type: 'string',
                            description: 'es el nombre de una competicion, debes traer los datos completo id y mas datos para que luego los uses en la busqueda de la season',
                            // enum: ['sr:competitor:17']
                        }
                    }
                },                
            }
            

        ];
        // return await openAiService.conecctionWitFunctionsCall(functions, messages);
    }

    async callFunction(function_call: any) {
        const args = JSON.parse(function_call.arguments);
        switch (function_call.name) {
            case 'overUnder':
                return await this.getOverUnderByTeam(args['team']);
            case 'liveGame1':
                return await this.liveGame1(args['teamId']);
            
            case 'probababilities':
                return await this.getProbabilitiesByRangeDateAndTeam(args['team']);

            case 'standings':
                // return await this.getStandingsByMessage(args['question']);
                return (await this.getStandings()).fullData.season_form_standings[0].groups[0].form_standings;

            case 'bestPlayersPointsByTeam':
                return await this.getLeadersByTeamAndPoints(args['team']);

            case 'bestPlayersGoalsByTeam':
                return await this.getLeadersByTeamAndGoals(args['team']);

            case 'bestPlayersAssistsByTeam':
                return await this.getLeadersByTeamAndAssists(args['team']);

            case 'playersRedCardsByTeam':
                return await this.getLeadersByTeamAndRedCards(args['team']);

            case 'playersYellowCardsByTeam':
                return await this.getLeadersByTeamAndYellowCards(args['team']);

            case 'playersYellowRedCardsByTeam':
                return await this.getLeadersByTeamAndYellowRedCards(args['team']);

            case 'playersOwsGoalsyTeam':
                return await this.getLeadersByTeamAndOwnGoals(args['team']);

            case 'playersOwnGoals':
                return await this.getLeadersAndOwnGoals();

            case 'playersLeadersByPoints':
                return await this.getLeadersPoints()
            
            case 'playersLeadersByGoals':
                return await this.getLeadersGoals()

            case 'playersLeadersByYellowCards':
                return await this.getLeadersYellowCards();
            
            case 'playersLeadersByRedCards':
                return await this.getLeadersRedCards();

            case 'playersLeadersByAssists':
                return this.getLeadersAssists();
            
            case 'getCompetitior':
                // const {id} = JSON.parse(args['id']);
                return await this.getCompetitiorLastGame(args['id']);
            case 'botTelegramPrediction1':
                return await this.botTelegramPrediction1(args['teamId']);
            case 'getAllLeague':
                return await this.getAllCompetitions(args['competition'])
            default:
                throw new Error('No function found');
        }
    }


}


export default new IaPremierLeagueService();





