import Stripe from 'stripe';
import config from "../../config/config";
import UserService from './usersService';
import PaymentService from './paymentService';
class StripeService {

    stripeInstance: Stripe;
    endpointSecret: string;

    constructor() {
        this.stripeInstance = new Stripe(config.stripe.secretKey, {
            apiVersion: config.data.api.stripe.version,
            // typescript: true,
        });
        this.endpointSecret = config.stripe.signingSecret;
    }

    async createCheckoutSession(lineItemsProducts: any, paymentMode: any, successUrl: string, cancelUrl: string, email: string) {
        try {
            const session = await this.stripeInstance.checkout.sessions.create({
                line_items: lineItemsProducts,
                mode: paymentMode ?? 'payment',
                success_url: successUrl,
                cancel_url: cancelUrl,
                customer_email: email
            });

            await PaymentService.savePayment(session);

            return { ...session };
        } catch (error: any) {
            return { message: error.message };
        }
    }

    async checkoutSessionCompleted(paymentCompleted: any) {
        const emailUserPayment = paymentCompleted.customer_details.email;
        const user = await UserService.getUserByEmail(emailUserPayment);

        if (!user) {
            console.log(`The User ${emailUserPayment} not Found`);
            return;
        }
        
        await PaymentService.updatePayment(paymentCompleted);
    }

    async invoice(request2: any) {
        let event = request2;

        if (this.endpointSecret) {
            const signature = request2.headers['stripe-signature'];
            const rawBody = request2.body;
            const payloadString = JSON.stringify(rawBody, null, 2);
            try {
                event = this.stripeInstance.webhooks.constructEvent(rawBody, signature, this.endpointSecret);
            } catch (err: any) {
                throw new Error(`⚠️  Webhook signature verification failed. ${err.message}`);
            }
        }

        switch (event.type) {
            case 'payment_intent.succeeded':
                const paymentIntent = event.data.object;
                console.log(`PaymentIntent for ${paymentIntent.amount} was successful!`);
                break;
            case 'checkout.session.completed':
                console.log('this is a checkout session completed')
                this.checkoutSessionCompleted(event.data.object);
                break;
            default:
                console.log(`Unhandled event type ${event.type}.`);
        }

        return true;
    };


}

export default new StripeService();