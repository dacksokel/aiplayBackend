import OpenAIApi from 'openai'
import OpenAiDefault from '../enums/OpenAiDefault';
import config from '../../config/config';
import IaPremierLeagueService from './iaPremierLeagueService';
import AiService from './sportRadar/soccer/aiService'
import TennisAiService from './sportRadar/tennis/TennisAiService';
import BaseBallAiService from './sportRadar/baseball/baseballAiService';
import BasketBallAiService from './sportRadar/basketball/basketBallAIService'
// import NflAiService from './sportRadar/nfl/nflAiService'

import TypeSport from '../enums/TypeSport';

class OpenAiService {
    private openAI: OpenAIApi;

    constructor() {
        this.openAI = new OpenAIApi({ apiKey: config.openaiApiKey });
    }

    async conecctionWithOpenAi(data: any) {
        try {
            const completion = await this.openAI.chat.completions.create({
                // model: OpenAiDefault.model4,
                model: OpenAiDefault.model35turbo16k,
                // model: "ft:gpt-3.5-turbo-0613:personal::7sQJEQa1",
                messages: data,
                temperature: 0.5,
                // max_tokens:5000
            });
            console.log(await completion.choices[0].message)
            return completion.choices[0].message;
        } catch (error) {
            console.log(error)
            throw new Error('Error connecting with OpenAI ');
        }
    }
    async conecctionWithOpenAiSimple(data: string) {
        try {
            const completion = await this.openAI.chat.completions.create({
                // model: OpenAiDefault.model4,
                model: OpenAiDefault.model35turbo16k,
                // model: "ft:gpt-3.5-turbo-0613:personal::7sQJEQa1",
                messages: [
                    { "role": "system", "content": "analista deportivo experto" },
                    { "role": "user", "content": data }]
            });
            return completion.choices[0].message;
        } catch (error) {
            throw new Error('Error connecting with OpenAI');

        }
    }

    // async conecctionWitFunctionsCall(functions: any, messages: any) {
    //     try {

    //     let newMessage = null;
            
    //     while (true) {
    //         const completion = await this.openAI.chat.completions.create({
    //             // model: 'gpt-3.5-turbo',
    //             model: OpenAiDefault.model35turbo16k,

    //             messages,
    //             functions: functions,
    //             temperature: 0.5,

    //         });

    //         const message = completion.choices[0].message;
    //         messages.push(message);
    //         console.log(message);

    //         // If there is no function call, we're done and can exit this loop
    //         if (!message.function_call) {
    //             // return;
    //             return message
    //         }
            
    //         // If there is a function call, we generate a new message with the role 'function'.
    //         // const result = await AiService.callFunction(message.function_call);
    //         const result = await IaPremierLeagueService.callFunction(message.function_call);
    //         newMessage = {
    //             role: 'function',
    //             name: message.function_call.name,
    //             content: JSON.stringify(result),
    //         };
    //         messages.push(newMessage);
            
    //         console.log(newMessage);
    //         console.log('-----------------------------------------------------------------');
    //         // if (newMessage.role === 'assistant' && newMessage.content != null) { 
    //         //     return newMessage
    //         // }
    //     }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    async conecctionWitFunctionsCall2(functions: any, messages: any, typeSport: string) {
        try {

        let newMessage = null;
        // let oneTimeFunction = false;
            
        while (true) {
            const completion = await this.openAI.chat.completions.create({
                // model: 'gpt-3.5-turbo',
                model: OpenAiDefault.model35turbo16k,

                messages,
                functions: functions,
                temperature: 0.5,

            });

            const message = completion.choices[0].message;
            messages.push(message);
            console.log(`tamano de messages array de datos: ${messages.length}`)
            console.log(message);
            // if(oneTimeFunction){
            //     // If there is no function call, we're done and can exit this loop
            // }
            if (!message.function_call) {
                // return;
                return message;
            }

            
            // If there is a function call, we generate a new message with the role 'function'.
            let result = null;
            if(typeSport === TypeSport.Soccer){
                result = await AiService.callFunction(message.function_call);
            }else if(typeSport === TypeSport.BaseBall){
                result = await BaseBallAiService.callFunction(message.function_call);
            }else if(typeSport === TypeSport.Tennis){
                result = await TennisAiService.callFunction(message.function_call);
            }else if(typeSport === TypeSport.BasketBall){
                result = await BasketBallAiService.callFunction(message.function_call);
            }else if(typeSport === TypeSport.NFL){}

            // const result = await IaPremierLeagueService.callFunction(message.function_call);
            newMessage = {
                role: 'function',
                name: message.function_call?.name,
                content: JSON.stringify(result),
            };
            messages.push(newMessage);
            
            console.log(newMessage);
            console.log('-----------------------------------------------------------------');
            // if (newMessage.role === 'assistant' && newMessage.content != null) { 
            //     return newMessage
            // }
        }
        } catch (error) {
            console.log(error)
            return {errpr:'Not Answer'}
        }
    }
    async conecctionWitFunctionsCallTennis(functions: any, messages: any) {
        try {

        let newMessage = null;
            
        while (true) {
            const completion = await this.openAI.chat.completions.create({
                // model: 'gpt-3.5-turbo',
                model: OpenAiDefault.model35turbo16k,

                messages,
                functions: functions,
                temperature: 0.5,

            });

            const message = completion.choices[0].message;
            messages.push(message);
            console.log(message);

            // If there is no function call, we're done and can exit this loop
            if (!message.function_call) {
                // return;
                return message
            }
            
            // If there is a function call, we generate a new message with the role 'function'.
            const result = await TennisAiService.callFunction(message.function_call);
            // const result = await IaPremierLeagueService.callFunction(message.function_call);
            newMessage = {
                role: 'function',
                name: message.function_call.name,
                content: JSON.stringify(result),
            };
            messages.push(newMessage);
            
            console.log(newMessage);
            console.log('-----------------------------------------------------------------');
            // if (newMessage.role === 'assistant' && newMessage.content != null) { 
            //     return newMessage
            // }
        }
        } catch (error) {
            console.log(error)
        }
    }
}


export default new OpenAiService();