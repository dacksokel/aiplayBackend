import nodemailer from 'nodemailer';
import config from '../../config/config';
import typeMail from '../enums/typeMail';
import ITypeMail from '../interfaces/ITypeMail';
import IUser from '../interfaces/IUsers';
import ejs from 'ejs';
import fs from 'fs';
import path from 'path';


class MailService {
    transporter;

    constructor() {
        this.transporter = nodemailer.createTransport({
            host: config.smtp.server,
            port: config.smtp.port,
            secure: !config.smtp.tls,
            auth: {
                user: config.smtp.email,
                pass: config.smtp.password,
            },
            tls: {
                rejectUnauthorized: !config.smtp.tls,
            },
        });
    }

    async getTemplate(typeMessage: string, user: IUser) {
        let template;
        switch (typeMessage) {
            case typeMail.resetPassword:
                if (user._id && user.email && user.resetPasswordToken) { 
                    template = this.templateResetPassword(user._id, user.email, user.resetPasswordToken);
                }
                break;
            case typeMail.activeAcount:
                if (user._id && user.email && user.activationToken) {
                    template = await this.templateActivateAccount(user._id, user.email, user.activationToken);
                }
                break;            
            default:
                template = "<b>Hello world!</b>";
                break;
        }
        return template;
    }

    async templateResetPassword(id:string, email: string, token: string) {
        try {
            const templatePath = path.join(__dirname, '..', 'views', 'emailsTemplate', `${typeMail.resetPassword}.ejs`);
            console.log(templatePath)
            const templateContent = fs.readFileSync(templatePath, 'utf-8');
        
            const data = {
                email,
                token,
                resetLink: config.urlResetPassword,
                id,
                companyName: config.company.name

            }
            // Renderiza la plantilla con los datos proporcionados
            const renderedTemplate = await ejs.render(templateContent, data);
        
            return renderedTemplate;
          } catch (error) {
            console.error('Error rendering template:', error);
            throw new Error('Error rendering template');
          }      
    }

    async templateActivateAccount(id: string, email: string, token: string): Promise<string> {
        try {
            const defaultPath = './views/emailsTemplate';
            const templatePath = path.resolve(path.join(defaultPath, `${typeMail.activeAcount}.ejs`));
            // console.log(testFilePath)
            
            // const templatePath = path.join(__dirname, '..', 'views', 'emailsTemplate', `${typeMail.activeAcount}.ejs`);
            console.log(templatePath)
    
            const templateContent = fs.readFileSync(templatePath, 'utf-8');
        
            const data = {
                email,
                token,
                activationLink: config.urlActivateAccount,
                id,
                companyName: config.company.name
            }
            const renderedTemplate = await ejs.render(templateContent, data);
        
            return renderedTemplate;
          } catch (error) {
            console.error('Error rendering template:', error);
            throw new Error('Error rendering template');
          }        
    }

    getSubject(typeMessage: string) {
        let subject;
        switch (typeMessage) {
            case typeMail.resetPassword:
                subject = this.resetSubject();
                break;
            case typeMail.activeAcount:
                subject = this.activateAcountSubject();
            default:
                break;
        }

        return subject;
    }

    resetSubject() {
        return 'Reset you Password'
    }

    activateAcountSubject() {
        return 'activate Acount '
    }

    async sendEmail(typeMessage: typeMail, user: IUser) {
        try {
            const info = await this.transporter.sendMail({
                from: config.smtp.email, // sender address
                to: user.email, // list of receivers
                subject: this.getSubject(typeMessage), // Subject line
                html: await this.getTemplate(typeMessage, user), // html body
            });

            console.log("Message sent: %s", info.messageId);
        } catch (e) {
            console.log('Error to send email for ' + typeMessage, e)
        }
    }
}

export default new MailService();