import Wallet from "../enums/wallet";
import IWallet from "../interfaces/IWallet";
import { WalletModel } from "../models/Wallet";


class WalletService {
    async create(userId: string): Promise<IWallet> {
        try {
            const walletData = {
                userId: userId,
                balance: Wallet.balanceDefault,
                transactions: [{
                    type: Wallet.transactionTypeDeposit,
                    amount: Wallet.balanceDefault,
                }]
            };
            const createdWallet = await WalletModel.create(walletData);
            return createdWallet;
        } catch (error) {
            throw new Error("Failed to create wallet");
        }
    }
    async getWalletById(walletId: string): Promise<IWallet | null> {
        try {
            const wallet = await WalletModel.findById(walletId);
            return wallet;
        } catch (error) {
            throw new Error("Failed to read wallet");
        }
    }
    
    async update(walletId: string, walletData: IWallet) {
        try {
            const updatedWallet = await WalletModel.findByIdAndUpdate(walletId, walletData, { new: true });
            return updatedWallet;
        } catch (error) {
            throw new Error("Failed to update wallet");
        }
    }

    externalizador(walletData: IWallet) {
        return {
            balance: walletData.balance,
            _id: walletData._id
        };
    }

    async getWalletByUserId(userId: string): Promise<IWallet | null> {
        try {
            const wallet = await WalletModel.findOne({ userId: userId });
            return wallet;
        } catch (error) {
            throw new Error("Failed to get wallet by user ID");
        }
    }
}

export default new WalletService();

