interface WeatherInfo {
    location: string;
    temperature: string;
    unit: string;
    forecast: string[];
}

class TestServices {


    async test1() {
        console.log('you here in test 1')
    }

// Example dummy function hard coded to return the same weather
// In production, this could be your backend API or an external API
    async getCurrentWeather(location: string, unit: 'celsius' | 'fahrenheit' = 'fahrenheit'): Promise<WeatherInfo> {
        const weatherInfo: WeatherInfo = {
            location,
            temperature: '72',
            unit,
            forecast: ['sunny', 'windy']
        };

        return Promise.resolve(weatherInfo);
    }
}

export default new TestServices();