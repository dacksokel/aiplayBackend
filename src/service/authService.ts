import IUser from '../interfaces/IUsers';
import { UserModel } from '../models/User';
import usersService from './usersService';

class AuthService {
    async register(user: IUser) {
        try {
            const existingUser = await UserModel.findOne({ email: user.email });
            if (existingUser) {
                return 'User already exists';
            }             
            return await usersService.createUser(user);
        } catch (error) {
            throw new Error('Error registering user');
        }
    }
    async login(user: IUser) {
      try {
      const { email, password } = user;
      const validatePassword = await this.validatePassword(email, password);
        if (typeof validatePassword === 'string') {
           return validatePassword
        }
        if(!validatePassword){
            return 'Invalid password';
        }

        return await usersService.getUserByEmail(email);
        
      } catch (error) {
        throw new Error(`${error}`)
      }
    }   

    async validatePassword(email: string, password: string) {
        try {
          const user = await UserModel.findOne({ email: email });
          if (user) {
            const isValidPassword = await (user as any).comparePasswords(password);
            return isValidPassword;
          }
        } catch (error) {
          console.error('Error validating password:', error);
          throw new Error('Error validating password');
        }
      }

    
}

export default new AuthService;