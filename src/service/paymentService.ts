import PlanService from "./planService";
import StripeService from "./stripeService";
import PaymentLogModel from "../models/paymentLog";
import OrderService from "./orderService";

class PaymentService { 

    async createPaymentSession(paymentData: any) {
        try {
            const plan = await PlanService.getPlanByCode(paymentData.code);
            const lineItemsProducts = [
                {
                    price_data: {
                        product_data: {
                            name: plan?.name
                        },
                        currency: 'usd',
                        unit_amount: (plan?.priceByMonth ?? 0) * 100,
                    },
                    quantity: 1
                }
            ];
            const createSession = await StripeService.createCheckoutSession(lineItemsProducts, paymentData.payment_mode, paymentData.success_url, paymentData.cancel_url, paymentData.email);
            return createSession;
        } catch (error) {
            console.log(error)                                         
        }
        
    }
    
    async updatePayment (paymentData: any){
        try {
            await PaymentLogModel.updateOne({paymentId: paymentData.id},{
                $set:{
                    status: paymentData.status
                }
            }, {upsert: true,});

            await OrderService.updateOrder(paymentData.id, paymentData.status);

        } catch (error) {
            console.log(error)
        }
    }

    async savePayment(session: any){
        console.log(session)
        try {            
            const paymentLog = {
                paymentId: session.id,
                status: session.status,
                amount_total: session.amount_total,
                fullData: session
            }
            await PaymentLogModel.create(paymentLog);
        } catch (error) {
            console.log(error)
        }
    }

}

export default new PaymentService();