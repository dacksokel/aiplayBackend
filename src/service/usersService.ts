import { UserModel } from '../models/User';
import { v4 as uuidv4 } from 'uuid';
import IUser from '../interfaces/IUsers';
import { generateToken } from '../utils/jwt';
import mailService from './mailService';
import TypeMAil from '../enums/typeMail';
import walletService from './walletService';
import IWaller from '../interfaces/IWallet';
import IUserChatLog from '../interfaces/IUserChatLog';
import UserChatLogModel from '../models/userChatLog';
import SourceChat from '../enums/SourceChat';

class UserService {
  async createUser(userData: IUser) {
    try {
      const createdUser = await UserModel.create(userData);
      await mailService.sendEmail(TypeMAil.activeAcount, createdUser);
      const wallet = await walletService.create(createdUser._id);      
      return this.externalizeUser(createdUser, wallet);
    } catch (error) {
      console.error('Error creating user:', error);
      throw new Error('Error creating user');
    }
  }

  async getUser(userId: string) {
    try {
      const user = await UserModel.findById(userId);
      if (user) {
        const wallet = await walletService.getWalletByUserId(user._id);
        if (wallet) {
          return this.externalizeUser(user, wallet);
        }
      } else {
        throw new Error('User not found');
      }
    } catch (error) {

    }
  }

  async getAllusers() { 
    try {
      const users = await UserModel.find();
      if (users) {
        return users;
      } else {
        throw new Error('Users not found');
      }
    } catch (error) {
      console.error('Error retrieving users:', error);
      throw new Error('Error retrieving users');
    }
  }

  async getUserByEmail(email: string) {
    try {
      const user = await UserModel.findOne({ email: email });
      if (user) {
        const wallet = await walletService.getWalletByUserId(user._id);
        if (wallet) {
          return this.externalizeUser(user, wallet);
        } else {
          // throw new Error('User not found');
        return;

        }
      } else {
        // throw new Error('User not found');
        return;
      }
    } catch (error) {
      console.error('Error retrieving user:', error);
      throw new Error('Error retrieving user');
    }
  }

  async getUserById(id: string) {
    try {
      const user = await UserModel.findById(id);
      if (user) {
        const wallet = await walletService.getWalletByUserId(user._id);
        if (wallet) {
          return this.externalizeUser(user, wallet);
        } else {
          throw new Error('User not found');
        }
      } else {
        throw new Error('User not found');
      }
    } catch (error) {
      console.error('Error retrieving user:', error);
      throw new Error('Error retrieving user');
    }
   }

  async updateUser(userId: string, updatedUserData: IUser) {
    try {
      const updatedUser = await UserModel.findByIdAndUpdate(userId, updatedUserData, { new: true });
      if (updatedUser) {
        const wallet = await walletService.getWalletByUserId(userId);
        if (wallet) {
          return this.externalizeUser(updatedUser, wallet);
        } else {
          throw new Error('User not found');
        }
      } else {
        throw new Error('User not found');
      }
    } catch (error) {
      console.error('Error updating user:', error);
      throw new Error('Error updating user');
    }
  }

  async deleteUser(userId: string) {
    try {
      const deletedUser = await UserModel.findByIdAndDelete(userId);
      if (deletedUser) {
        return { message: 'User deleted successfully' };
      } else {
        throw new Error('User not found');
      }
    } catch (error) {
      console.error('Error deleting user:', error);
      throw new Error('Error deleting user');
    }
  }

  async activateAccount(userId: string, activationToken: string) {
    try {
      const user = await UserModel.findById(userId);

      if (user) {
        if (user.activationToken === activationToken) {
          await UserModel.updateOne(user._id, { activate: true });
          return { message: 'Account activated successfully' };
        } else {
          return { error: 'Token does not match' };
        }
      } else {
        return { error: 'User not found' };
      }
    } catch (error) {
      console.log(error)
      return { error: 'Error processing the request id no valid' };
    }
  }

  async forgetPasswordSendMail(email: string) {
    const user = await UserModel.findOne({ email: email });
    if (!user) {
      return { message: 'email wrong!!' }
    }

    user.resetPasswordToken = uuidv4();
    await user.save();
    await mailService.sendEmail(TypeMAil.resetPassword, user)
    return { message: 'email send successfully' }
  }

  async resetPassword(email: string, _id: string, resetPasswordToken: string, password: string, newPassword: string) {
      const existingUser = await UserModel.findById(_id);
      if (!existingUser) {
        return {message: 'User not Found'}      
      }
      if(!existingUser.activate){
        return {message: 'The user is not active'}
      }
      // Verify that the resetPasswordToken is the same as the one stored in the database
      if (resetPasswordToken !== existingUser.resetPasswordToken) {
        return { message: 'Invalid reset password token' };
      }
      // Verify that the current password is correct
      const isPasswordValid = await (existingUser as any).comparePasswords(password);
      if (!isPasswordValid) {
        return { message: 'Invalid current password' };
      }
      // Update the password in the database
      existingUser.password = newPassword;
      existingUser.resetPasswordToken = '';
      await existingUser.save();

      return { message: 'Password reset successfully' };
  }

  externalizeUser(user: IUser, wallet: IWaller) {
    let token = null;
    if(Object.prototype.hasOwnProperty.call(user, 'token')){
      token = (user as any).token
    }else{
      token = generateToken(user);
    }
    const walletToshow = {
      id: wallet._id,
      balance: wallet.balance
    }
    const { _id, username, email, role, activate, name, lastName, country, state, picture } = user;
    return { _id, username, email, role, activate, name, lastName, country, state, picture, wallet: walletToshow, token };
  }

  async setPictureProfile(user: IUser, image: any){
    const picture = (image as any)[0]
    const pictureName = picture.filename;
    const picturePatch = picture.path;
    const id = (user._id as string)
    const userUpdate = await UserModel.findOneAndUpdate({_id: id}, {picture: picturePatch})
    const wallet = await walletService.getWalletByUserId(id) || {} as IWaller;
    if (userUpdate) {
      return this.externalizeUser(userUpdate, wallet);
    } else {
      // throw new Error('User not found');
      return 'User not Found'
    }    
  }
  
  async saveChatLog(answer: string, question: string, user: IUser, typeSport: string, source: string){

      const toSave:IUserChatLog = {
        user: user?._id || '',
        question,
        answer,
        sportType: typeSport,
        source
      }
      console.log(toSave)
      await UserChatLogModel.create(toSave)
  }
  
  
}

export default new UserService();


