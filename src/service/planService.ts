import IPlan from "../interfaces/IPlan";
import PlanModel from "../models/plan";

class PlanService{ 

    async createPlan(plan: IPlan) {
        plan.code = this.generateCode();        
        const planResponse = await PlanModel.create(plan);
        const planResponseExternalize = this.externalizePlan(planResponse);
        return planResponseExternalize;
    }
    
    generateCode() { 
        const date = new Date();
        const year = date.getFullYear().toString().slice(-2);
        const month = (`0${date.getMonth() + 1}`).slice(-2);
        const day = (`0${date.getDate()}`).slice(-2);

        const randomNumber = Math.floor(Math.random() * 100000);

        const randomLongNumber = Math.floor(Math.random() * 100000000000);

        const planCpde = `${year}${month}${day}-${randomNumber}-${randomLongNumber}`;

        return planCpde;
    }

    async getPlans() { }

    async getPlanByCode(code: string) {
        const planResponse = await PlanModel.findOne({ code: code });
        return planResponse;
     }

    async getPlanById() { }

    async updatePlan() { }

    async deletePlan() { }

    externalizePlan(planResponse: IPlan) {
        return {            
            code: planResponse.code,
            name: planResponse.name,
            description: planResponse.description,
            status: planResponse.status,
            priceByMonth: planResponse.priceByMonth,
            priceByYear: planResponse.priceByYear,
            priceByMonthWithDiscount: planResponse.priceByMonthWithDiscount,
            priceByYearWithDiscount: planResponse.priceByYearWithDiscount,
            discount: planResponse.discount,
            discountByYear: planResponse.discountByYear,
            discountByMonth: planResponse.discountByMonth,
            accessToChat: planResponse.accessToChat,
            accessToForo: planResponse.accessToForo,
            accessToStadistics: planResponse.accessToStadistics,
            acceessToPredictionsByWeek: planResponse.acceessToPredictionsByWeek,
            amountOfPredictionsByWeek: planResponse.amountOfPredictionsByWeek,
            startDate: planResponse.startDate,
            endDate: planResponse.endDate,
            image: planResponse.image,
            currency: planResponse.currency,
            avalibleForTrial: planResponse.avalibleForTrial,
            trialDays: planResponse.trialDays

        }
     }
}

export default new PlanService();