// import { OpenAIEmbeddings } from "langchain/embeddings/openai";
// import { RecursiveCharacterTextSplitter } from "langchain/text_splitter";
// import { HNSWLib } from "langchain/vectorstores/hnswlib";

// import { OpenAI } from "langchain/llms/openai";
// import { RetrievalQAChain, loadQARefineChain } from "langchain/chains";
// import { JSONLoader } from "langchain/document_loaders/fs/json";

// import fs from 'fs';
// import path from 'path';

// import config from '../../config/config';
// import SoccerService from './sportRadar/soccer/soccerService';


// class LangChainService{
//     OPENAI_API_KEY:string = ''

//     constructor(){
//         console.log('langchainservice start')
//         this.OPENAI_API_KEY = config.openaiApiKey;
//         // this.generateAndStoreEmbeddings()
//     }
//     async generateAndStoreEmbeddings() {
    
//       // STEP 1: Load the data
//       const competitions = await SoccerService.getCompetitions();
//       const trainingText = JSON.stringify(competitions)

//         const filePath = path.resolve(__dirname, '../../config/json/test.json');
//         const dir = path.dirname(filePath);

//         if (!fs.existsSync(dir)) {
//             fs.mkdirSync(dir); // crear directorio padre si no existe
//         }

//         if (fs.existsSync(filePath)) {
//             fs.writeFileSync(filePath, JSON.stringify(trainingText));
//         //     // El archivo existe, hacemos append
//         //     fs.appendFileSync(filePath, JSON.stringify(trainingText);

//         // } else {
//         //     // El archivo no existe, lo creamos
//         //     // console.log({ "messages": message })
//         }
//         // .replace(/,/g, '\n');
//     //   const trainingText = fs.readFileSync("training-data.txt", "utf8");
   
//     const loader = new JSONLoader(filePath);

// const docs = await loader.load();
//       // STEP 2: Split the data into chunks
//     //   const textSplitter = new RecursiveCharacterTextSplitter({
//     //     separators: [","],
//     //     chunkSize: 1000,
//     //   });
    
//     //   // STEP 3: Create documents
//     //   const docs = await textSplitter.createDocuments([trainingText]);
    
//       // STEP 4: Generate embeddings from documents
//       const vectorStore = await HNSWLib.fromDocuments(
//         docs,
//         new OpenAIEmbeddings({ openAIApiKey: this.OPENAI_API_KEY }),
//       );
    
//       // STEP 5: Save the vector store
//       vectorStore.save("hnswlib");
//     }
    
//     async getAnswer(question: string) {
                
//         const model = new OpenAI({ openAIApiKey: this.OPENAI_API_KEY, temperature: 0.9 });
    
//       // STEP 1: Load the vector store
//       const vectorStore = await HNSWLib.load(
//         "hnswlib",
//         new OpenAIEmbeddings({ openAIApiKey: this.OPENAI_API_KEY }),
//       );
    
//       // STEP 2: Create the chain
//       const chain = new RetrievalQAChain({
//         combineDocumentsChain: loadQARefineChain(model),
//         retriever: vectorStore.asRetriever(),
//       });
    
//       // STEP 3: Get the answer
//       const result = await chain.call({
//         query: question,
//       });
    
//       return result.output_text;
//     }

// }

// export default new LangChainService();