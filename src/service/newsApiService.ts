import config from "../../config/config";
import INews from '../interfaces/INews';

class NewsApiService {
    async fetchNews(query: INews) {
        const urlSource = config.data.api.newsapi.souce;
        const url = `${urlSource}?q=${query.question}&from=${query.from}&to=${query.to}&language=${query.language}&softBy=${query.sortBy}`;
        console.log(url)
        const fetchNews = await fetch(url, {
            headers: {
                'Authorization': `Bearer ${config.newsapi.apiKey}`,
            },
        });
        const newsData = await fetchNews.json();

        return newsData;
    }
}

export default new NewsApiService();