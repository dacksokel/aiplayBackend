import express, { Application } from 'express';
import config from '../config/config';
import { connectDB } from '../db/';
import initRoutes from './routers';
import morgan from 'morgan';
import cors from 'cors';

// import * as cronjobs from '../cronjobs/index';


const app: Application = express();
const port: number = config.port;

connectDB();

app.use(morgan('dev'));

app.use(cors({
    origin: true
}));

app.use('/public', express.static('public'));
app.use('/', initRoutes);

app.listen(port, () => {
    // cronjobs.startCornJob()
    console.log(`Server listening at http://localhost:${port}`);
});