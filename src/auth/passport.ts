import passportLocal from "passport-local";
import passportJwt from "passport-jwt";
import { Request } from "express";
// import passportAccesstoken from 'passport-accesstoken';
// import passportAccesstoken from 'passport-access-token';
import config from "../../config/config";

import UsersService from "../service/usersService";
import AuthService from "../service/authService";

import IUser from "../interfaces/IUsers";
import authService from "../service/authService";

function setupPassport(passport: any) {
  console.log("start passport");
  const localStrategy = passportLocal.Strategy;
  const JWTstrategy = passportJwt.Strategy;
  const ExtractJWT = passportJwt.ExtractJwt;
  // const TokenStrategy = passportAccesstoken.Strategy;

  passport.use("/singup", new localStrategy((username, password, done) => {}));

  // passport.use(
  //   "/login",
  //   new localStrategy(async (username, password, done) => {
  //     try {
  //       const user = await UsersService.getUserByEmail(username);
  //       if (!user) {
  //         return done(null, false, { message: "User Not found" });
  //       }

  //       const validatePassword = await AuthService.validatePassword(
  //         username,
  //         password
  //       );

  //       if (!validatePassword) {
  //         return done(null, false, { message: "Wrong Password" });
  //       }

  //       return done(null, user, { message: "Login Success" });
  //     } catch (e: any) {
  //       done(e);
  //     }
  //   })
  // );
  //Create a passport middleware to handle User login
  passport.use(
    "login",
    new localStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
      },
      async (req: Request, email: string, password: string, done: Function) => {
        try {
          const user = await UsersService.getUserByEmail(email);
          if (!user) {
            return done(null, false, { message: "User Not found" });
          }

          const validatePassword = await AuthService.validatePassword(
            email,
            password
          );

          if (!validatePassword) {
            return done(null, false, { message: "Wrong Password" });
          }

          return done(null, user, { message: "Login Success" });
        } catch (e: any) {
          done(e);
        }
      }
    )
  );

  passport.use(
    new JWTstrategy(
      {
        //secret we used to sign our JWT
        secretOrKey: config.jwt,
        //we expect the user to send the token as a query parameter with the name 'secret_token'
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      },
      async (token, done) => {
        try {          
          let user;

          if (token.user) {
            return done(null, token.user);
          }
        } catch (error: any) {
          done(error);
        }
      }
    )
  );
}

export default setupPassport;
