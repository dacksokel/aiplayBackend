import cron from 'node-cron';
import BaseballService from '../../../src/service/sportRadar/baseball/baseballService'


const getCompetitionAndSeason = (competitions: any, competitionsDefaults: any) => {
    const promises = competitions.map((competition, index) => {
        // console.log(index)
        if (competitionsDefaults.includes(competition.id)) {
            // console.log(competition)
            return BaseballService.getCompetitionSeasonDefault(competition.id, index).then(respose => respose)
        }
    }).filter(competitions => competitions !== undefined)
    return Promise.all(promises)
}

const getProbabilitiesBySeasonId = (seasonsIds: string[]) => {
    console.log('get probabilitie baseball')
    let promises = seasonsIds.map((id, index) => BaseballService.probabilities(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    // console.log(promises)
    return Promise.all(promises)
}

const initStanding = (seasonsIds: string[]) => {
    console.log('get Standings baseball')
    let promises = seasonsIds.map((id, index) => BaseballService.getStandings(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    // console.log(promises)
    return Promise.all(promises)
}

const init = async ()=>{
    console.log('Start Baseball fetch');
    await BaseballService.getCompetitions();

    const competitions = await BaseballService.getCompetitions()
    // console.log(competitors)
    const competitionsDefaults = BaseballService.getCompetitionsDefault()
    // console.log(competitionsDefaults)

    const seasonsIds = await getCompetitionAndSeason(competitions, competitionsDefaults)
    // console.log(seasonsIds)
    const res = await getProbabilitiesBySeasonId(seasonsIds)
    // console.log(res)
    const standigsResponse = await initStanding(seasonsIds);
    // console.log(standigsResponse)

    console.log(`finish init baseball`)
}

export default {init}