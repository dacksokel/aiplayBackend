import cron from 'node-cron';
import BascketBallService from '../../../src/service/sportRadar/basketball/basketBallService';

const init = async ()=>{
    console.log('init cron basketball')
    const competitions = await BascketBallService.getCompetitions()
    const competitionsDefault:string[] =  BascketBallService.getCompetitionsDefault()

    //step 2 get all seasons from competitions Default
    const seasonsIds = await getCompetitionsAndSeasons(competitions, competitionsDefault)
    console.log(seasonsIds)
    //step 3 get probabilities
    const res = await getProbabilitiesBySeasonId(seasonsIds)
    console.log(res)
    const standigsResponse = await initStanding(seasonsIds);
    // console.log(standigsResponse)

    console.log(`finish init bascketBall`)
}

const getCompetitionsAndSeasons = (competitions: any, competitionsDefault:string[])=>{
    const promises = competitions.map(
        (competition, index)=>{
            if(competitionsDefault.includes(competition.id)){
                return BascketBallService.getCompetitionSeasonDefault(competition.id, index).then(response => response)
            }
        }
    ).filter(data => data !== undefined)
    return Promise.all(promises)

}

const getProbabilitiesBySeasonId = (seasonsIds: string[]) => {
    console.log('get probabilitie BascketBall')
    let promises = seasonsIds.map((id, index) => BascketBallService.probabilities(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    console.log(promises)
    return Promise.all(promises)
}

const initStanding = (seasonsIds: string[]) => {
    console.log('get Standings baseball')
    let promises = seasonsIds.map((id, index) => BascketBallService.getStandings(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    // console.log(promises)
    return Promise.all(promises)
}

export default {init}