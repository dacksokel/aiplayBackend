import cron from 'node-cron';
import TennisService from '../../../src/service/sportRadar/tennis/tennisServices'

// Define the cron schedule to run every 3 days
// const cronSchedule = '0 0 */3 * *';

// // Define the cron job function
// const cronJob = async () => {
//  // Add your code here to be executed by the cron job
// };

// // Schedule the cron job
// cron.schedule(cronSchedule, cronJob);


const getCompetitionAndSeason = (competitions: any, competitionsDefaults: any) => {
    const promises = competitions.map((competition, index) => {
        // console.log(index)
        if (competitionsDefaults.includes(competition.id)) {
            console.log(competition)
            return TennisService.getCompetitionSeasonDefault(competition.id, index).then(respose => respose)
        }
    }).filter(competitions => competitions !== undefined)
    return Promise.all(promises)
}

const getProbabilitiesBySeasonId = (seasonsIds: string[]) => {
    console.log('get probabilitie tennis')
    let promises = seasonsIds.map((id, index) => TennisService.probabilities(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    console.log(promises)
    return Promise.all(promises)
}

const getSummaries = (seasonsIds: string[])=>{
    console.log('start Summaries Tennis')
    let promises = seasonsIds.map((id, index) => TennisService.getSummaries(id, index).then(respose => respose));
    // promise = promise.filter(competitions => competitions !== undefined)
    // console.log(promises)
    return Promise.all(promises)

}

const tennisInit = async () => {
    console.log('Start Tennis Fetch Competitions')
    const competitions = await TennisService.getCompetitions()
    // console.log(competitors)
    const competitionsDefaults = TennisService.getCompetitionsDefault()
    // console.log(competitionsDefaults)

    const seasonsIds = await getCompetitionAndSeason(competitions, competitionsDefaults)
    console.log(seasonsIds)
    // const res = await getProbabilitiesBySeasonId(seasonsIds)
    // console.log(res)
    const summaries = await getSummaries(seasonsIds);
    console.log(summaries)

    console.log(`finish init tennis`)

}

export default { tennisInit }