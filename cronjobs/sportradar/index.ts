import Tennis from './tennis';
import Soccer from './soccer';
import BascketBall from './bascketball';
import Baseball from './baseball';
class SportRadarCronJobs {

    async init(){
        await Soccer.fetchingDataDefault()
        await Tennis.tennisInit()
        await BascketBall.init()
        await Baseball.init()
    }

}

export default new SportRadarCronJobs();
