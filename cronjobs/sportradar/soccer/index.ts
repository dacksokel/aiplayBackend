import SoccerService from '../../../src/service/sportRadar/soccer/soccerService';

console.log('start cron job for SportRadar soccer')

const initGetCompetitors = async (competitionsIds) => {
    const peticiones = competitionsIds.map((id, index)=> SoccerService.getCompetitionSeasonDefault(id, index).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const initGetAndUpdateStandings = async (seasonIds) => {
    const peticiones = seasonIds.map((id, index)=> SoccerService.formStandings(id, index).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const initGetAndUpdateProbabilities = async (seasonIds) => {
    const peticiones = seasonIds.map((id, index)=> SoccerService.probabilities(id, index).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const initGetAndUpdateLeaders = async (seasonIds) => {
    const peticiones = seasonIds.map((id, index)=> SoccerService.leaders(id, index).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const initGetAndUpdateOverUnder = async (seasonIds) => {
    const peticiones = seasonIds.map((id, index)=> SoccerService.overUnder(id, index).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const initGetAndUpdateCompetitors = async (seasonIds) => {
    const peticiones = seasonIds.map((id, index)=> SoccerService.seasonCompetitors(id, index, false).then(respose => respose))
    // const peticiones = competitionsIds.map(async (id, index)=> setTimeout(()=> SoccerService.getCompetitionSeasonDefault(id),5000 * index))
    return Promise.all(peticiones)
}

const fetchingDataDefault = async () => {
    
    //step 1
    const competitions = await SoccerService.getCompetitions();
    const competitionsDefaults = SoccerService.getCompetitionsDefault()

    const competitionDefaultDone: Array<string> = [];
    const competitionsIds = competitions.map( competition => {
        if(competitionsDefaults.includes(competition.name) && !competitionDefaultDone.includes(competition.name)){
            competitionDefaultDone.push(competition.name)
            return competition.id;
        }
    }).filter(id => id !== undefined);

    

    const seasonIds = await initGetCompetitors(competitionsIds);
    console.log('funcitron 1', seasonIds)
    
    const competitorsResult = await initGetAndUpdateCompetitors(seasonIds);
    console.log('initGetAndUpdateCompetitors 2', competitorsResult);

    const standingsResult = await initGetAndUpdateStandings(seasonIds);
    console.log('initGetAndUpdateStandings 3', standingsResult);
    
    const probabilitiesResult = await initGetAndUpdateProbabilities(seasonIds);
    console.log('initGetAndUpdateProbabilities 4', probabilitiesResult);
    
    const leadersResult = await initGetAndUpdateLeaders(seasonIds);
    console.log('initGetAndUpdateLeaders 5', leadersResult);
    
    const overUnderResult = await initGetAndUpdateOverUnder(seasonIds);
    console.log('initGetAndUpdateOverUnder 6', overUnderResult);

    console.log('update by default success')

}

export default {
    fetchingDataDefault
}