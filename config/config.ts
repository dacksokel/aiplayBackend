import dotenv from 'dotenv';
import IConfig from '../src/interfaces/IConfig';
import fs from 'fs';
import path from 'path';

const mode = process.env.NODE_ENV || 'default';
dotenv.config({ path: `./${mode}.env` });
console.log(`Mode: ${mode}`);

const defaultPath = './config/data';
const jsonFilePath = path.resolve(path.join(defaultPath, `${mode}.json`));

const jsonData = fs.readFileSync(jsonFilePath, 'utf-8');
const configData = JSON.parse(jsonData);

const config: IConfig = {
    env: process.env.NODE_ENV || 'development',
    port: parseInt(process.env.PORT || '4000'),
    db: {
        uri: process.env.DB_URI || 'mongodb://127.0.0.1:27017/aiplay',
    },
    jwt: process.env.JWT_SECRET_KEY || 'defaultSecretKey',
    urlServer: process.env.URL_SERVER || `http://localhost:4000`,
    smtp: {
        port: parseInt(process.env.SMTP_PORT || '587'),
        password: process.env.SMTP_PASSWORD || '1234567890',
        email: process.env.SMTP_USERNAME || 'test@tes.com',
        tls: !!process.env.SMTP_TLS || false,
        server: process.env.SMTP_HOST || 'localhost'
    },
    urlActivateAccount: process.env.ACTIVATE_ACCOUNT_URL || 'http://localhost:4000/activateaccount',
    urlResetPassword: process.env.RESET_PASSWORD_URL || 'http://localhost:4000/resetpassword',
    company: {
        name: process.env.COMPANY_NAMe || 'Company for Test',
    },
    openaiApiKey: process.env.OPENAI_API_KEY || 'fakeOpenAiKey',
    sportRadar: {
        soccer: process.env.SOCCER_API || 'fakeSoccerApi',
        tennis: process.env.TENNIS_API || 'fakeTennisApi',
        basketball: process.env.BASCKETBALL_API || 'FakebasketballApi',
        baseball: process.env.BASEBALL_API || 'fackeApi',
        nfl: process.env.NFL_API || 'fakeapi'
    },
    data: { ...configData },
    newsapi: {
        apiKey: process.env.NEWS_API_KEY || 'fakeNewsApiKey',
    },
    stripe: {
        secretKey: process.env.STRIPE_SECRET_KEY || 'fakeStripeSecretKey',
        signingSecret: process.env.STRIPE_SIGNNING_SECRET || 'fakeStripeSigningSecret'
    }
}
export default config;



