
# Instalación de un proyecto de Node.js con TypeScript

Este archivo readme.md es una guía básica para instalar un proyecto de Node.js con TypeScript.

## Requisitos previos

Antes de comenzar, asegúrate de tener instalado Node.js y npm en tu computadora. Si no los tienes, puedes descargarlos desde el sitio web oficial de Node.js.

## Paso 1: Crear un nuevo proyecto de Node.js

Para crear un nuevo proyecto de Node.js, abre una terminal y navega hasta la carpeta donde deseas crear el proyecto. Luego, ejecuta el siguiente comando:

```
npm init
```

Este comando iniciará un asistente que te guiará a través del proceso de creación de un nuevo proyecto de Node.js. Sigue las instrucciones en pantalla para completar el proceso.

## Paso 2: Instalar TypeScript

Para instalar TypeScript, ejecuta el siguiente comando en la terminal:

```
npm install typescript --save-dev
```

Este comando instalará TypeScript como una dependencia de desarrollo en tu proyecto.

## Paso 3: Configurar TypeScript

Crea un archivo llamado `tsconfig.json` en la raíz de tu proyecto y agrega la siguiente configuración:

```json
{
  "compilerOptions": {
    "target": "es6",
    "module": "commonjs",
    "outDir": "dist",
    "sourceMap": true
  },
  "include": [
    "src/**/*"
  ]
}
```

Esta configuración le dice a TypeScript que compile tu código en la carpeta `dist` y que genere mapas de origen para depuración.

## Paso 4: Escribir código TypeScript

Crea una carpeta llamada `src` en la raíz de tu proyecto y agrega un archivo llamado `index.ts`. Este será el archivo principal de tu proyecto.

Escribe el siguiente código en `index.ts`:

```typescript
console.log("Hola, mundo!");
```

## Paso 5: Compilar TypeScript

Para compilar tu código TypeScript, ejecuta el siguiente comando en la terminal:

```
npx tsc
```

Este comando compilará tu código TypeScript en la carpeta `dist`.

## Paso 6: Ejecutar tu proyecto

Para ejecutar tu proyecto, ejecuta el siguiente comando en la terminal:

```
node dist/index.js
```

Este comando ejecutará el archivo `index.js` en la carpeta `dist`.

¡Felicidades! Has instalado y configurado correctamente un proyecto de Node.js con TypeScript.


